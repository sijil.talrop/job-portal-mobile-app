import AsyncStorage from '@react-native-async-storage/async-storage';

const ChiefReducer = (state, action) => {
  switch (action.type) {
    case 'UPDATE_USER_DATA':
      const user_data = {...state.user_data, ...action.user_data};
      AsyncStorage.setItem('user_data', JSON.stringify(user_data));
      // console.warn(user_data);
      return {
        ...state,
        user_data: user_data,
      };

    default:
      return state;
  }
};

export default ChiefReducer;
