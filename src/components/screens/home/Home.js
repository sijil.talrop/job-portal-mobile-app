import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  ScrollView,
  RefreshControl,
} from 'react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import CommonSearchBar from './CommonSearchBar';
import Category from './Category';
import PopularJobs from './PopularJobs';
import RecentCompany from './RecentCompany';
import HomeHeader from '../header/HomeHeader';
import Package from './Package';
import {ImageBackground} from 'react-native';
import {Context} from '../../contexts/Store';
import axios from 'axios';
import * as base from '../../../../Settings';
import {TouchableOpacity} from 'react-native';
import {CommonActions, useNavigation} from '@react-navigation/native';

const {height, width, fontScale} = Dimensions.get('window');

export default function AccountTab(props) {
  const navigation = useNavigation();
  const [refresh, setRefresh] = useState(false);
  const {state, dispatch} = useContext(Context);
  console.warn(state.user_data.is_verified, 'sijil');

  useEffect(() => {
    _get_profile();
  }, []);

  const onRefresh = React.useCallback(() => {
    setRefresh(true);
    setTimeout(() => {
      setRefresh(false);
    }, 500);
  });
  let _get_profile = async () => {
    let get_url = base.BASE_URL + 'users/profile/';
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data, message} = response.data;
        if (StatusCode == 6000) {
          let profile_data = data.data;
          dispatch({
            type: 'UPDATE_USER_DATA',
            user_data: {
              ...state.user_data,
              profile_data: profile_data,
            },
          });
        } else {
        }
      })
      .catch((error) => {
        console.warn(error.response.status);
        if (error.response.status == 401) {
          dispatch({
            type: 'UPDATE_USER_DATA',
            user_data: {
              is_verified: false,
            },
          });
        }
      });
  };

  return (
    <>
      <View style={{minHeight: '100%', flex: 1, backgroundColor: '#fff'}}>
        <ImageBackground
          style={styles.backgroundImageBox}
          source={require('../../../assets/vector-images/Group20260.png')}
        />
        <View
          style={{
            paddingHorizontal: 15,
            paddingVertical: height * 0.04,
          }}>
          <HomeHeader refresh={refresh} />
        </View>
        <ScrollView
          refreshControl={
            <RefreshControl refreshing={refresh} onRefresh={onRefresh} />
          }
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            alignItems: 'center',
            width: width,
            minHeight: '100%',
            backgroundColor: '#fff',
          }}>
          <View style={{}}>
            <View style={styles.topView}>
              <View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <TouchableOpacity
                    onPress={() => {
                      navigation.navigate('Cv');
                    }}>
                    <Text style={styles.header}>Hello</Text>
                  </TouchableOpacity>

                  <View style={styles.nameBox}>
                    <Text style={[styles.headerLight, {marginLeft: 10}]}>
                      {state.user_data.profile_data &&
                        state.user_data.profile_data.first_name}
                    </Text>
                    <Text style={[styles.headerLight, {marginLeft: 5}]}>
                      {state.user_data.profile_data &&
                        state.user_data.profile_data.last_name}
                    </Text>
                  </View>
                </View>
                <Text adjustsFontSizeToFit style={styles.description}>
                  Find Your Perfect Job
                </Text>
              </View>
            </View>
            <View>
              <View style={{marginBottom: 20}}>
                <CommonSearchBar />
              </View>
              <Category />
            </View>
            <View style={{marginVertical: 20}}>
              <Package />
            </View>
            <View style={{marginTop: 20}}>
              <PopularJobs refresh={refresh} />
            </View>
            <View>
              <RecentCompany refresh={refresh} />
            </View>
          </View>
        </ScrollView>
      </View>
      {/* </ImageBackground> */}
    </>
  );
}

const styles = StyleSheet.create({
  header: {
    fontSize: RFPercentage(2),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  nameBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  headerLight: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.6),
  },
  backgroundImageBox: {
    resizeMode: 'contain',
    justifyContent: 'flex-end',
    width: width * 0.5,
    height: width * 0.3,
    position: 'absolute',
    right: 0,
  },
  description: {
    fontSize: RFValue(12, width),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  topView: {
    paddingHorizontal: 15,
    paddingBottom: 29,
  },
});
