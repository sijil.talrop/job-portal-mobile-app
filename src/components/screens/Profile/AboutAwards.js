import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import moment from 'moment';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';

import DateTimePicker from '@react-native-community/datetimepicker';
const {width, height} = Dimensions.get('window');
const card_width = width * 0.27;
const card_height = width * 0.3;

export default function AboutAwards() {
  const {state, dispatch} = useContext(Context);

  const [awarda, setAwarda] = useState();
  const [startDate, setStartDate] = useState();
  const [objective, setObjective] = useState();
  const [selectedBox, setSelectedBox] = useState();
  const [push, setPush] = useState([]);
  const [endDate, setEndDate] = useState();
  const [show, setShow] = useState(false);
  const [drag, setDrag] = useState(false);
  const [mode, setMode] = useState('date');
  const [date, setDate] = useState(new Date());
  const [add, setAdd] = useState(false);
  const [isFocused, setIsFocused] = useState({
    title_border: false,
    object_box: false,
  });

  // {textinput based Functions}

  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };

  const confirmHandler = (value) => {
    setSelectedBox(value);
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setStartDate(moment(currentDate).format('YYYY-MM-DD'));
    setShow(false);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setDrag(false);
    setMode(currentMode);
  };

  const onEndDate = (event, selectedValue) => {
    const liveDate = selectedValue;
    setEndDate(moment(liveDate).format('YYYY-MM-DD'));
    setDrag(false);
  };

  const pickeMode = (currentMode) => {
    setShow(false);
    setDrag(true);
    setMode(currentMode);
  };

  const registration = async () => {
    let access_token = state.user_data.access_token;
    let post_url = base.BASE_URL + 'users/update-award/';
    axios
      .post(post_url, push, {
        headers: {
          Authorization: 'Bearer ' + access_token,
          'Content-Type': 'application/json; ',
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          Toast.show(data.message, Toast.SHORT);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  const renderYearOptions = () => {
    if (awarda && startDate && endDate && objective) {
      setAdd(true);
      push.push({
        title: awarda,
        start_date: startDate,
        end_date: endDate,
        description: objective,
      });
      setAwarda();
      setStartDate();
      setEndDate();
      setObjective();
      Toast.show(' Added Successfully  ', Toast.SHORT);
    } else {
      Toast.show('Please enter all details', Toast.SHORT);
    }
  };

  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>5</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Tell us</Text> about your Awards
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Name of Award</Text>
            <TextInput
              value={awarda}
              style={[
                styles.input,
                {borderColor: isFocused.title_border ? '#c3c4c7' : '#ddd'},
              ]}
              placeholder="Name of award"
              placeholderTextColor={isFocused.title_border ? '#000' : '#aaa'}
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setAwarda(val)}
              onFocus={() => handleInputFocus('title_border')}
              onBlur={() => handleInputBlur('title_border')}
            />
          </View>

          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Start Date </Text>
            <View style={[styles.pickerBox]}>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onChange}
                />
              )}
              {startDate ? (
                <Text style={styles.dateStyle}>{startDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYY-MM-DD</Text>
              )}
              <TouchableOpacity
                onPress={() => {
                  showMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>End Date </Text>
            <View style={styles.pickerBox}>
              {drag && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onEndDate}
                />
              )}
              {endDate ? (
                <Text style={styles.dateStyle}>{endDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYYY-MM-DD</Text>
              )}
              <TouchableOpacity
                onPress={() => {
                  pickeMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={[styles.loginUser]}>
            <Text style={styles.subHeader}>Description</Text>
            <View
              style={[
                styles.objectiveBox,
                {borderColor: isFocused.object_box ? '#c3c4c7' : '#ddd'},
              ]}>
              <TextInput
                value={objective}
                style={styles.inputLast}
                placeholder="Explain of expereince"
                returnKeyType={'next'}
                onChangeText={(val) => setObjective(val)}
                multiline={true}
                placeholderTextColor={isFocused.object_box ? '#000' : '#aaa'}
                onFocus={() => handleInputFocus('object_box')}
                onBlur={() => handleInputBlur('object_box')}
              />
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            renderYearOptions();
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Awards</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (add) {
              registration();
            } else {
              Toast.show('Click Add Button', Toast.SHORT);
            }
          }}
          style={styles.button2}>
          <Text style={styles.uploadText2}>Save</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 12,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    backgroundColor: '#e0e6f7',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  pickerBox: {
    fontSize: 15,
    width: '100%',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  inputLast: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  objectiveBox: {
    height: height * 0.11,
    borderRadius: 20,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    paddingHorizontal: 10,
  },
  loginUser1: {
    backgroundColor: '#e0e6f7',
    borderRadius: 10,
    marginVertical: 10,
    paddingVertical: 5,
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  subHeader: {
    fontSize: RFPercentage(1.6),
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  button2: {
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 15,
    backgroundColor: '#ef803a',
  },
  uploadText2: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  dateStyle: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
  },
});
