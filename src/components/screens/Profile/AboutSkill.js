import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {Picker} from '@react-native-picker/picker';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';
const {width, height} = Dimensions.get('window');

export default function AboutSkill() {
  const {state, dispatch} = useContext(Context);
  const [skill_list, setSkill_list] = useState([]);
  const [skill, setSkill] = useState();
  const [level, setLevel] = useState();
  const [push, setPush] = useState([]);
  const [add, setAdd] = useState(false);
  const [isFocused, setIsFocused] = useState({
    Skill_Indicator: false,
  });

  useEffect(() => {
    get_job_type();
    renderYearOptions;
  }, []);

  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };

  const get_job_type = () => {
    let post_url = base.BASE_URL + 'users/skill-level/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setSkill_list(data.data);
        } else {
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const registration = async () => {
    let access_token = state.user_data.access_token;
    let post_url = base.BASE_URL + 'users/update-skill/';
    console.log(post_url);
    axios
      .post(post_url, push, {
        headers: {
          Authorization: 'Bearer ' + access_token,
          'Content-Type': 'application/json; ',
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          Toast.show(data.message, Toast.SHORT);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };
  const renderYearOptions = () => {
    if (skill && level) {
      push.push({
        title: skill,
        level: level,
      });
      setSkill();
      setLevel(0);
      Toast.show(' Added Successfully  ', Toast.SHORT);
      setAdd(true);
    } else {
      Toast.show('Please enter all details', Toast.SHORT);
    }
  };

  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>4</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Tell us</Text> about your Skills
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Type your Skill here</Text>
            <TextInput
              value={skill}
              style={[
                styles.input,
                {borderColor: isFocused.Skill_Indicator ? '#c3c4c7' : '#ddd'},
              ]}
              placeholderTextColor={isFocused.Skill_Indicator ? '#000' : '#aaa'}
              placeholder="Enter your Skill"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setSkill(val)}
              onFocus={() => handleInputFocus('Skill_Indicator')}
              onBlur={() => handleInputBlur('Skill_Indicator')}
            />
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>How much know </Text>
            <View style={styles.loginUser1}>
              <Picker
                selectedValue={level}
                style={styles.picker}
                onValueChange={(itemValue, itemIndex) => setLevel(itemValue)}>
                {skill_list &&
                  skill_list.length > 0 &&
                  skill_list.map((item, index) => (
                    <Picker.Item
                      label={item.label}
                      key={index}
                      value={item.value}
                    />
                  ))}
              </Picker>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            renderYearOptions();
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Skills</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (add) {
              registration();
            } else {
              Toast.show('Click Add Button', Toast.SHORT);
            }
          }}
          style={styles.button2}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText2}>Save</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 12,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  button2: {
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 15,
    backgroundColor: '#ef803a',
  },
  uploadText2: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    backgroundColor: '#e0e6f7',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  subHeader: {
    fontSize: RFPercentage(1.6),
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  picker: {
    color: '#aaa',
    fontWeight: 'normal',
    fontFamily: 'BalooPaaji2-Regular',
  },
});
