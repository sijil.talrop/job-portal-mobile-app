import SearchBar from 'react-native-dynamic-search-bar';
import React, {useState, useEffect, useRef} from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {TouchableOpacity} from 'react-native-gesture-handler';
const {width, height} = Dimensions.get('window');

export default function CommonSearchBar(props) {
  const navigation = useNavigation();
  const [spinnerVisibility, setSpinnerVisibility] = useState(false);
  const [search_query, setSearchQuery] = useState();

  const confirmHandler = (value) => {
    setSpinnerVisibility(true);
  };

  return (
    <View style={styles.middleContent}>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          paddingHorizontal: 20,
        }}>
        <View style={styles.searchBox}>
          <SearchBar
            onSearchPress={() => {
              console.log('Search Icon is pressed');
              navigation.navigate('SearchPage', {search_query: search_query});
            }}
            onClearPress={() => setSearchQuery('')}
            clearIconComponent={!search_query ? true : false}
            onRequestSearch={() => setSearchQuery()}
            placeholder="What are you looking for?"
            fontFamily="BalooPaaji2-SemiBold"
            shadowStyle={{}}
            onChangeText={(text) => {
              setSearchQuery(text);
            }}
            returnKeyType="search"
            onSubmitEditing={(text) => {
              setSearchQuery(text);
              navigation.navigate('SearchPage', {search_query: search_query});
            }}
            textInputStyle={{
              backgroundColor: '#f3f5f8',
              width: width * 0.55,
            }}
            style={{
              backgroundColor: '#f3f5f8',
              height: 55,
              width: width * 0.75,
              borderRadius: 20,
              borderWidth: 1,
              elevation: 1,
              borderColor: '#eee',
            }}
          />
        </View>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('FilterPage');
          }}
          style={styles.imageContainer}>
          <Image
            style={styles.image}
            source={require('../../../assets/vector-images/filter.png')}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  Scroll: {
    backgroundColor: 'red',
  },
  imageContainer: {
    width: 50,
    height: 50,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
  searchBox: {
    width: width * 0.78,
  },
});
