import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  ImageBackground,
  ScrollView,
} from 'react-native';
import CreateAccount from './CreateAccount';
const {height, width} = Dimensions.get('window');

export default function AccountTab({props, route}) {
  return (
    <ScrollView contentContainerStyle={styles.topView}>
      <ImageBackground
        source={require('../../../assets/vector-images/Group20260.png')}
        style={styles.backgroundImage}
      />
      <View>
        <View>
          <Text style={styles.header}>Create Account</Text>
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={styles.contentScroll}>
          <CreateAccount phone={route.params.phone} />
        </ScrollView>
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  topView: {
    minHeight: '100%',
    backgroundColor: '#fff',
    justifyContent: 'center',
    paddingHorizontal: 20,
    paddingVertical: 30,
    // flex: 1,
  },
  header: {
    color: '#000',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 10,
    elevation: 3,
    marginVertical: 20,
  },
  tabBox: {
    backgroundColor: 'red',
    width: width * 0.4,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 10,
  },
  iconBox: {
    paddingTop: 20,
    width: width - 30,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
  contentScroll: {},
  backgroundImage: {
    width: width * 0.6,
    height: width * 0.6,
    resizeMode: 'contain',
    position: 'absolute',
    top: 0,
    right: 0,
  },
});
