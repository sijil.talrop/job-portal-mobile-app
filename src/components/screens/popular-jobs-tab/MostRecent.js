import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {CommonActions, useNavigation} from '@react-navigation/native';
import axios from 'axios';
import * as base from '../../../../Settings';
import {InstagramLoader} from 'react-native-easy-content-loader';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.33;
const card_width = width * 0.6;
const box_width = width - 40;

export default function MostRecent() {
  const navigation = useNavigation();
  const [popular_job, setPopular_job] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const [selectedRating, setSelectedRating] = useState();
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  let _get_categories = () => {
    let get_url = base.BASE_URL + 'jobs/jobs/?popular=true&sort_by=newest';
    console.log(get_url);
    axios
      .get(get_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setPopular_job(data.data);
          setSelectedRating(data.data[0].pk);
          setTimeout(() => {
            setLoading(false);
          }, 200);
        } else {
          setPopular_job([]);
          setTimeout(() => {
            setLoading(false);
          }, 200);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  useEffect(() => {
    _get_categories();
  }, []);

  const Needs = ({item, index}) => {
    return (
      <InstagramLoader
        key={item.pk}
        active
        tHeight={5}
        tWidth="95%"
        sTWidth="95%"
        sTHeight={5}
        loading={isLoading}
        primaryColor="#d2d2d2"
        secondaryColor="#d2d2d2"
        imageStyles={{width: box_width - 40, height: 100, borderRadius: 20}}>
        <TouchableOpacity
          onPress={() => {
            TouchableHandler(item.pk);
            navigation.navigate('CompanyJobSinglePage', {
              item: item,
            });
          }}
          activeOpacity={0.9}
          style={[
            styles.mainContainer,
            {
              backgroundColor: item.pk == selectedRating ? '#2261a6' : '#fff',
            },
          ]}>
          <View style={styles.left}>
            <View
              style={[
                styles.lefContent,
                {
                  backgroundColor:
                    item.pk == selectedRating ? '#fff' : '#e2e2e2',
                },
              ]}>
              <View style={styles.imageContainer}>
                <Image source={{uri: item.logo}} style={styles.productImage} />
              </View>
            </View>
          </View>
          <View style={styles.contentContairener}>
            <Text
              style={[
                styles.introName,
                {color: item.pk == selectedRating ? '#fff' : '#adabbb'},
              ]}>
              {item.business.length > 17
                ? item.business.substring(0, 18) + '...'
                : item.business}
            </Text>
            <Text
              style={[
                styles.name,
                {color: item.pk == selectedRating ? '#fff' : '#130937'},
              ]}>
              {item.name.length > 17
                ? item.name.substring(0, 20) + '....'
                : item.name}
            </Text>
            <View style={styles.Location}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={[
                    styles.amount,
                    {
                      color: item.pk == selectedRating ? '#fff' : '#130937',
                    },
                  ]}>
                  ₹{item.salary_minimum}-{item.salary_maximum}/
                </Text>
                <Text
                  style={[
                    styles.amount1,
                    {
                      color: item.pk == selectedRating ? '#fff' : '#130937',
                    },
                  ]}>
                  m
                </Text>
              </View>
              <View style={styles.locationCharge}>
                <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
                <Text
                  style={[
                    styles.place,
                    {
                      color: item.pk == selectedRating ? '#fff' : '#130937',
                    },
                  ]}>
                  {item.location.length > 17
                    ? item.location.substring(0, 17) + '...'
                    : item.location}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.right}>
            <View style={styles.rateContainer}>
              <Icon
                name={'bookmark'}
                color={item.kind == 'Featured' ? '#ef801f' : '#eee'}
                size={45}
              />
            </View>
            <View style={styles.delivery}>
              <Text
                style={[
                  styles.bottomButtonText,
                  {
                    color: item.pk == selectedRating ? '#fff' : '#aaa',
                  },
                ]}>
                {item.date_added}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </InstagramLoader>
    );
  };

  return (
    <View style={{}}>
      <View style={{alignItems: 'center', height: '100%'}}>
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{minHeight: '100%'}}
          data={popular_job}
          renderItem={Needs}
          keyExtractor={(item) => item.pk}
        />
      </View>
      {!isNaN(popular_job) != '' && (
        <Text style={{textAlign: 'center'}}>Nothing Found</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#aaa',
    elevation: 0.08,
  },
  headText: {
    fontSize: 14,
    marginBottom: 20,
  },
  left: {
    alignItems: 'center',
    width: box_width * 0.16,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.52,
  },
  lefContent: {
    height: Reward_Box_Height * 0.4,
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 10,
    borderRadius: 30,
  },
  Location: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.6),
  },
  name: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 17,
    textTransform: 'capitalize',
    color: '#000',
    fontSize: RFValue(8, box_width), // second argument is standardScreenHeight(optional),
  },
  amount: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: RFPercentage(1.6),
    textTransform: 'capitalize',
    color: '#000',
  },
  amount1: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.4),
    textTransform: 'capitalize',
    color: '#393057',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.24,
    height: Reward_Box_Height,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: RFPercentage(1.5),
    fontFamily: 'BalooPaaji2-Regular',
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
    right: 7,
  },
});
