import React, {useEffect, useState} from 'react';
import {StyleSheet, View, Text, Dimensions, Image} from 'react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {Rating, AirbnbRating} from 'react-native-ratings';
import {TouchableOpacity} from 'react-native-gesture-handler';
const {width, height} = Dimensions.get('window');

export default function ReviewPage(props) {
  useEffect(() => {}, [rating]);

  const [rating, setRating] = useState(4.0);

  const ReviewRender = () => {
    return props.review.map((item) => (
      <View
        key={item.pk}
        style={{
          marginBottom: 20,
          borderWidth: 1,
          borderColor: '#aaa',
          borderStyle: 'dashed',
          borderRadius: 20,
          padding: 10,
        }}>
        <View style={styles.contentFlex}>
          <View style={styles.leftContainer}>
            <View style={styles.imageBox}>
              <Image style={styles.image} source={{uri: item.logo}} />
            </View>
            <View style={styles.contentBox}>
              <Text style={styles.name}>{item.name}</Text>
            </View>
          </View>
          <View>
            <Rating
              startingValue={item.rating}
              readonly={true}
              type="custom"
              tintColor="#fff"
              ratingColor="#ef801f"
              ratingBackgroundColor="#eee"
              ratingCount={5}
              imageSize={17}
              style={{paddingVertical: 10}}
              isDisabled={false}
            />
          </View>
        </View>
        <View style={{}}>
          <Text style={styles.content}>{item.comment}</Text>
        </View>
      </View>
    ));
  };
  return (
    <>
      <View
        style={{
          paddingVertical: 20,
          backgroundColor: '#fff',
        }}>
        <View style={styles.rateBox}>
          <Text style={styles.header}>Reviews</Text>
          <View style={{marginBottom: 10}}>
            <View style={[styles.rateContainer]}>
              <Rating
                startingValue={rating}
                readonly={true}
                type="custom"
                tintColor="#fff"
                ratingColor="#ef801f"
                ratingBackgroundColor="#eee"
                ratingCount={5}
                imageSize={25}
                style={{paddingVertical: 10}}
                isDisabled={false}
              />
              <Text style={styles.reviewCount}>
                {props.total_review.total_rating} out of 5
              </Text>
            </View>
            <Text style={styles.ratingText}>
              {props.total_review.total_rating_count} global Rating
            </Text>
          </View>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                props.toggleModal();
              }}
              style={styles.buttonBox}>
              <Text style={styles.buttonText}>Rate Your Recent Company</Text>
            </TouchableOpacity>
          </View>
        </View>
        {ReviewRender()}
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  rateContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFValue(10, width), // second argument is standardScreenHeight(optional),
  },
  contentBox: {
    marginLeft: 10,
  },
  name: {
    fontSize: RFPercentage(1.5),
    fontFamily: 'BalooPaaji2-Regular',
    color: '#9f9cb0',
  },
  subText: {
    fontSize: 18,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  reviewCount: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFPercentage(1.9),
  },
  buttonBox: {
    paddingVertical: 10,
    backgroundColor: '#ef801f',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 15,
  },
  buttonContainer: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.7),
  },
  rateBox: {
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderWidth: 0.5,
    borderColor: '#aaa',
    elevation: 0.1,
    borderRadius: 15,
    marginBottom: 25,
    backgroundColor: '#fff',
  },
  ratingText: {
    color: '#777778',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  inputHead: {
    color: '#000',
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
  },
  imageBox: {
    width: 80,
    height: 80,
    borderRadius: 30,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 100,
    overflow: 'hidden',
  },
  leftContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    width: width * 0.7,
  },
  contentFlex: {
    flexDirection: 'row',
    marginBottom: 5,
  },
  content: {
    fontSize: RFPercentage(1.6),
    fontFamily: 'BalooPaaji2-Regular',
    color: '#9491a7',
    textAlign: 'justify',
  },
  input: {
    width: '100%',
    paddingHorizontal: 10,
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 15,
    marginVertical: 10,
  },
});
