import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  Dimensions,
  View,
} from 'react-native';
var {height, width} = Dimensions.get('window');
import {useNavigation} from '@react-navigation/native';
import axios from 'axios';
import * as base from '../../../../Settings';

const Category = () => {
  const navigation = useNavigation();
  const [selectedRating, setSelectedRating] = useState('Product');
  const [popular_search, setPopular_search] = useState([]);

  const confirmHandler = (value) => {
    setSelectedRating(value);
  };

  let _get_popular_search = () => {
    let get_url = base.BASE_URL + 'jobs/popular-search/';
    axios
      .get(get_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setPopular_search(data.data);
        } else {
          setPopular_search([]);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  useEffect(() => {
    _get_popular_search();
  }, []);

  return (
    <>
      <View style={styles.flexBox}>
        {popular_search.map((item, id) => (
          <TouchableOpacity
            key={id}
            activeOpacity={0.8}
            onPress={() => {
              confirmHandler(item.keyword);
              navigation.navigate('SearchPage', {
                search_query: item.keyword,
              });
            }}
            style={[
              styles.mainContainer,
              {
                borderColor:
                  item.keyword === selectedRating ? '#2261a6' : '#aaa',
              },
            ]}>
            <Text
              style={[
                styles.itemFont,
                {color: item.keyword === selectedRating ? '#2261a6' : '#aaa'},
              ]}>
              {item.keyword}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
    width: width,
    paddingHorizontal: 20,
  },
  itemFont: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#9ba0ab',
    textAlign: 'center',
    fontSize: 15,
  },
  mainContainer: {
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderRadius: 10,
    margin: 5,
    width: width * 0.43,
    backgroundColor: '#fff',
    borderWidth: 1,
    elevation: 4,
    borderColor: '#eee',
  },
});

export default Category;
