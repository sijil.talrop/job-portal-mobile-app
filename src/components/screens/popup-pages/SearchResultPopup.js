import React, {useState, useEffect, useRef, useContext} from 'react';
import {StyleSheet, Text, View, Image, Dimensions} from 'react-native';
var {height, width} = Dimensions.get('window');

const SearchResultPopup = ({route}) => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.contentBox}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../../assets/vector-images/Group20334.png')}
              style={styles.image}
            />
          </View>
          <View style={styles.middle}>
            <Text style={styles.errorText}>No Result Found</Text>
            <Text style={styles.description}>
              Vivamus Eget Aliquam Dui. Integer Eu Arcu Vel Arcu Suscipit
              Ultrices Quis Non Mauris.Aenean
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    minHeight: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentBox: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    width: width * 0.7,
    height: width * 0.7,
  },
  image: {
    height: null,
    width: null,
    flex: 1,
    resizeMode: 'contain',
  },
  middle: {
    paddingHorizontal: 40,
  },
  errorText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 16,
    color: '#757575',
  },
  description: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    textAlign: 'center',
    color: '#757575',
  },
});

export default SearchResultPopup;
