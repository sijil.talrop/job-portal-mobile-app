import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
  Alert,
} from 'react-native';
import {CommonActions, useNavigation} from '@react-navigation/native';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {Context} from '../../contexts/Store';
import * as base from '../../../../Settings';

const {width, height} = Dimensions.get('window');
const card_width = width * 0.27;
const card_height = width * 0.3;

export default function AboutHeader({
  setFirst_name,
  setLastname,
  setPhone,
  setEmail,
  setdateOfbirth,
  setAddress,
  setGender,
  setAbout,
  setObjective,
  setSelectedBox,
  first_name,
  lastname,
  email,
  dateOfbirth,
  address,
  about,
  phone,
  gender,
  objective,
  selectedBox,
}) {
  const navigation = useNavigation();
  const {state, dispatch} = useContext(Context);

  const [basicData, setBasicData] = useState(
    state.user_data.profile_data.phone,
  );
  // const [first_name, setFirst_name] = useState();
  // const [lastname, setLastname] = useState();
  // const [job_position, setJob_position] = useState();
  // const [about, setAbout] = useState();
  // const [address, setAddress] = useState();
  // const [location, setLocation] = useState();
  // const [objective, setObjective] = useState();
  // const [email, setEmail] = useState();
  const [show, setShow] = useState(false);
  const [mode, setMode] = useState('date');
  const [date, setDate] = useState(new Date());
  // const [dateOfbirth, setdateOfbirth] = useState();
  // const [required, setRequired] = useState(false);
  // const [push, setPush] = useState([]);
  const [label, setLabel] = useState();

  const ref_input1 = useRef();
  const ref_input2 = useRef();
  const ref_input3 = useRef();

  const [isFocused, setIsFocused] = useState({
    name_box: false,
    secon_name: false,
    phone_box: false,
    address_box: false,
    about_box: false,
    objective_box: false,
    email: false,
    phones: false,
  });

  // console.warn(state.user_data);
  const [check, setCheck] = useState();
  const confirmHandler = (value) => {
    if (value == 'Male') {
      setSelectedBox(10);
      if (value) {
        setCheck(value);
      } else {
        setCheck(state.user_data.profile_data.gender);
      }
    } else if (value == 'Female') {
      setSelectedBox(20);
      if (value) {
        setCheck(value);
      } else {
        setCheck(state.user_data.profile_data.gender);
      }
    } else {
      setSelectedBox(30);
      if (value) {
        setCheck(value);
      } else {
        setCheck(state.user_data.profile_data.gender);
      }
    }
  };

  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  useEffect(() => {
    confirmHandler();
    _get_profile();
  }, []);

  useEffect(() => {
    setFirst_name(state.user_data.profile_data.first_name);
    setLastname(state.user_data.profile_data.last_name);
    setAbout(state.user_data.profile_data.about);
    setAddress(state.user_data.profile_data.address);
    setObjective(state.user_data.profile_data.objective);
    setCheck(state.user_data.profile_data.gender);
    setEmail(state.user_data.profile_data.email);
    // setSelectedBox(state.user_data.profile_data.gender);
    setdateOfbirth(state.user_data.profile_data.dob);
    setPhone(state.user_data.profile_data.phone);
    setGender(state.user_data.profile_data.gender);
  }, [state.user_data]);

  const Tab = [
    {
      Box: 'Male',
    },
    {
      Box: 'Female',
    },
    {
      Box: 'Other',
    },
  ];

  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setdateOfbirth(moment(currentDate).format('YYYY-MM-DD'));
    setShow(false);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setMode(currentMode);
  };

  let _get_profile = async () => {
    let get_url = base.BASE_URL + 'users/profile/';
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          let profile_data = data.data;
        } else {
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  const addData = () => {
    setFirst_name(first_name);
    setLastname(lastname);
    setAbout(about);
    setAddress(address);
    setObjective(objective);
    setCheck(check);
    setEmail(email);
    setSelectedBox(selectedBox);
    setdateOfbirth(dateOfbirth);
    setGender();
    setPhone(phone);
    // console.warn();
  };
  return (
    <View style={{marginBottom: height * 0.01}}>
      <View style={styles.basicDetailBox}>
        <View style={styles.dots}>
          <Text style={styles.dotText}>1</Text>
        </View>
        <Text style={styles.countText}>
          <Text style={{color: '#0462bf'}}>Basic</Text> Information
        </Text>
      </View>
      <View>
        <View style={styles.loginUser}>
          <Text style={styles.subHeader}>First Name</Text>
          <TextInput
            value={first_name}
            style={[
              styles.input,
              {borderColor: isFocused.name_box ? '#c3c4c7' : '#ddd'},
            ]}
            placeholder="Enter your First Name"
            autoFocus={false}
            returnKeyType={'next'}
            onChangeText={(val) => setFirst_name(val)}
            onSubmitEditing={() => ref_input1.current.focus()}
            placeholderTextColor={isFocused.name_box ? '#000' : '#aaa'}
            onFocus={() => handleInputFocus('name_box')}
            onBlur={() => handleInputBlur('name_box')}
            autoCompleteType="off"
          />
        </View>
        <View style={styles.loginUser}>
          <Text style={styles.subHeader}>Last Name</Text>
          <TextInput
            value={lastname}
            style={[
              styles.input,
              {borderColor: isFocused.secon_name ? '#c3c4c7' : '#ddd'},
            ]}
            placeholder="Enter your Last Name"
            autoFocus={false}
            returnKeyType={'next'}
            onChangeText={(val) => setLastname(val)}
            ref={ref_input1}
            onSubmitEditing={() => ref_input2.current.focus()}
            placeholderTextColor={isFocused.secon_name ? '#000' : '#aaa'}
            onFocus={() => handleInputFocus('secon_name')}
            onBlur={() => handleInputBlur('secon_name')}
            autoCompleteType="off"
          />
        </View>

        <View style={styles.loginUser}>
          <Text style={styles.subHeader}>Phone Number </Text>
          {/* <Text
            style={[
              styles.input,
              {borderColor: isFocused.phones ? '#c3c4c7' : '#ddd'},
            ]}> */}
          {/* {state.user_data.profile_data.phone} */}
          <TextInput
            value={phone}
            style={[
              styles.input,
              {borderColor: isFocused.phones ? '#c3c4c7' : '#ddd'},
            ]}
            placeholder="Enter your Phone Number"
            autoFocus={false}
            returnKeyType={'next'}
            onChangeText={(val) => setPhone(val)}
            ref={ref_input1}
            onSubmitEditing={() => ref_input2.current.focus()}
            placeholderTextColor={isFocused.email ? '#000' : '#aaa'}
            onFocus={() => handleInputFocus('phones')}
            onBlur={() => handleInputBlur('phones')}
            autoCompleteType="off"
          />
          {/* </Text> */}
        </View>

        <View style={styles.loginUser}>
          <Text style={styles.subHeader}>Email</Text>
          <TextInput
            value={email}
            style={[
              styles.input,
              {borderColor: isFocused.email ? '#c3c4c7' : '#ddd'},
            ]}
            placeholder="Enter your Email"
            autoFocus={false}
            returnKeyType={'next'}
            onChangeText={(val) => setEmail(val)}
            ref={ref_input1}
            onSubmitEditing={() => ref_input2.current.focus()}
            placeholderTextColor={isFocused.email ? '#000' : '#aaa'}
            onFocus={() => handleInputFocus('email')}
            onBlur={() => handleInputBlur('email')}
            autoCompleteType="off"
          />
        </View>

        <View style={styles.loginUser}>
          <Text style={styles.subHeader}>Date of Birth </Text>
          <View style={styles.pickerBox}>
            {show && (
              <DateTimePicker
                testID="dateTimePicker"
                value={date}
                mode={mode}
                is24Hour={true}
                display="default"
                onChange={onChange}
              />
            )}

            {dateOfbirth ? (
              <Text>{dateOfbirth}</Text>
            ) : (
              <Text>Select your DOB</Text>
            )}
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => {
                showMode();
              }}>
              <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
            </TouchableOpacity>
          </View>
        </View>

        <View style={styles.loginUser}>
          <Text style={styles.subHeader}> Gender</Text>
          <View style={styles.tabView}>
            {Tab.map((item, index) => (
              <TouchableOpacity
                key={index}
                onPress={() => {
                  confirmHandler(item.Box);
                }}
                style={[styles.tabBox]}>
                <Icon
                  name={'check'}
                  color={item.Box == check ? '#ef801f' : '#fff'}
                  size={20}
                />
                <Text
                  style={[
                    styles.buttonText,
                    {color: item.Box == check ? '#ef801f' : '#000'},
                  ]}>
                  {item.Box}
                </Text>
              </TouchableOpacity>
            ))}
          </View>
        </View>
        <View style={styles.loginUser}>
          <Text style={styles.subHeader}>Address</Text>
          <TextInput
            value={address}
            style={[
              styles.input,
              {borderColor: isFocused.address_box ? '#c3c4c7' : '#ddd'},
            ]}
            placeholder="Address"
            returnKeyType={'next'}
            onChangeText={(val) => setAddress(val)}
            multiline={true}
            onSubmitEditing={() => ref_input3.current.focus()}
            autoCompleteType="street-address"
            placeholderTextColor={isFocused.address_box ? '#000' : '#aaa'}
            onFocus={() => handleInputFocus('address_box')}
            onBlur={() => handleInputBlur('address_box')}
          />
        </View>
        <View style={styles.loginUser}>
          <Text style={styles.subHeader}>About</Text>
          <TextInput
            value={about}
            style={[
              styles.input,
              {borderColor: isFocused.about_box ? '#c3c4c7' : '#ddd'},
            ]}
            placeholder="About"
            ref={ref_input3}
            returnKeyType={'next'}
            onChangeText={(val) => setAbout(val)}
            multiline={true}
            autoCompleteType="street-address"
            placeholderTextColor={isFocused.about_box ? '#000' : '#aaa'}
            onFocus={() => handleInputFocus('about_box')}
            onBlur={() => handleInputBlur('about_box')}
          />
        </View>

        <View style={[styles.loginUser]}>
          <Text style={styles.subHeader}>Career Objective</Text>
          <View
            style={[
              {
                height: height * 0.12,
                backgroundColor: '#f5f6f9',
                borderWidth: 1,
                borderRadius: 20,
              },
              {borderColor: isFocused.objective_box ? '#c3c4c7' : '#ddd'},
            ]}>
            <TextInput
              value={objective}
              style={styles.inputLast}
              placeholder="Explain your self"
              ref={ref_input3}
              returnKeyType={'done'}
              onChangeText={(val) => setObjective(val)}
              multiline={true}
              onFocus={() => handleInputFocus('objective_box')}
              onBlur={() => handleInputBlur('objective_box')}
            />
          </View>
        </View>
      </View>
      <TouchableOpacity
        onPress={() => {
          addData();
        }}
        style={styles.button}>
        <Icon name={'plus'} color={'#ef803a'} size={20} />
        <Text style={styles.uploadText}>Save</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
    paddingVertical: '10%',
    backgroundColor: '#fff',
  },
  headerBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftIcon: {
    width: 30,
    height: 30,
    borderRadius: 10,
    backgroundColor: '#dbdae4',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 18,
  },
  imageContainer: {
    width: width * 0.3,
    height: width * 0.3,
    borderRadius: 100,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: '#fad4b2',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    borderRadius: 100,
    overflow: 'hidden',
  },
  basicDetailBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 14,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderWidth: 1,
  },
  locationInput: {
    fontSize: 15,
    color: '#aaa',
    fontFamily: 'BalooPaaji2-Regular',
  },
  pickerBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  inputLast: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    paddingHorizontal: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f5f6f9',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 20,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  tabBox: {
    width: width * 0.25,
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 5,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 15,
    paddingHorizontal: 15,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  subHeader: {
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
    fontSize: RFPercentage(1.6),
  },

  scrollStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageContent: {
    width: width * 0.4,
    height: width * 0.3,
    backgroundColor: '#fff',
  },
  companyImage: {
    width: width * 0.3,
    height: width * 0.25,
    borderWidth: 1,
    borderColor: '#aaa',
  },
  image1: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 10,
  },
  bottomButton: {
    backgroundColor: '#0462bf',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 100,
    marginTop: 20,
    width: width * 0.2,
    height: width * 0.1,
    borderWidth: 1,
    elevation: 0.1,
    borderColor: '#0462bf',
  },
  bottomText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#fff',
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  button: {
    backgroundColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  buttonBox: {
    padding: 5,
    backgroundColor: '#ef803a',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    marginTop: 10,
  },
  buttonBox2: {
    backgroundColor: '#ef803a',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: 100,
    marginTop: 10,
    paddingVertical: 5,
  },
  uploadButtonText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 12,
  },
  companyText: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
    marginTop: 5,
  },
  // picker: {
  //   fontSize: 9,
  //   color: 'red',
  // },
});
