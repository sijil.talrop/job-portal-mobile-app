import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import BasicHeading from '../header/BasicHeading';
import CommonSearchBar from '../home/CommonSearchBar';
import RecentTab from './RecentTab';
import RelevantTab from './RelevantTab';
const {height, width} = Dimensions.get('window');

export default function CompanyTab({route}) {
  const navigation = useNavigation();

  const Tab = [
    {
      Box: 'Most Relevant',
    },
    {
      Box: 'Most Recent',
    },
  ];

  const [selectedBox, setSelectedBox] = useState('Most Relevant');
  const confirmHandler = (value) => {
    setSelectedBox(value);
  };

  const renderTabList = () => {
    if (selectedBox == 'Most Relevant') {
      return <RelevantTab />;
    } else if (selectedBox == 'Most Recent') {
      return <RecentTab />;
    }
  };

  const renderTab = () => {
    return (
      <View style={{}}>
        <View style={styles.tabView}>
          {Tab.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                confirmHandler(item.Box);
              }}
              style={[
                styles.tabBox,
                {
                  backgroundColor:
                    item.Box === selectedBox ? '#2261a6' : '#f3f5f8',
                  borderColor: item.Box === selectedBox ? '#ddd' : '#b0c4e4',
                },
              ]}>
              <Text
                style={[
                  styles.buttonText,
                  {color: item.Box == selectedBox ? '#fff' : '#000'},
                ]}>
                {item.Box}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <View style={styles.contentScroll}>{renderTabList()}</View>
      </View>
    );
  };

  return (
    <View style={styles.backgroundImageBox}>
      <ImageBackground
        style={styles.background}
        source={require('../../../assets/vector-images/Group20260.png')}
      />
      <View style={{}}>
        <View style={{marginVertical: 20, paddingHorizontal: 15}}>
          <BasicHeading title="Company" />
        </View>
        <View style={{marginVertical: 20}}>
          <CommonSearchBar />
        </View>
        <View style={{paddingHorizontal: 20, marginTop: 20}}>
          <Text style={styles.filterHeader}>
            {route.params.total} Companies
          </Text>
        </View>
        <View style={{}}>{renderTab()}</View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImageBox: {
    width: width,
    minHeight: '100%',
    backgroundColor: '#fff',
  },
  background: {
    resizeMode: 'contain',
    width: width * 0.5,
    height: width * 0.3,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  contentScroll: {
    // minHeight: '100%',
  },
  filterHeader: {
    fontSize: 18,
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),

    fontFamily: 'BalooPaaji2-SemiBold',
  },
  tabView: {
    flexDirection: 'row',
    paddingBottom: 20,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginVertical: 20,
    backgroundColor: 'transparent',
  },
  tabBox: {
    width: width * 0.3,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 5,
    marginRight: 20,
    elevation: 0.7,
    borderWidth: 1,
  },
  iconBox: {
    paddingTop: 20,
    width: width - 30,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.6),
  },
});
