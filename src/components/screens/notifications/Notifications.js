import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Animated,
} from 'react-native';
import BasicHeading from '../header/BasicHeading';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.2;
const card_width = width * 0.6;
const box_width = width - 40;

export default function Notifications() {
  const [selectedRating, setSelectedRating] = useState();
  const [notification, setNotificaton] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  // useEffect(() => {
  //   _get_notifications();
  // }, []);

  const fadeAnim = useRef(new Animated.Value(0)).current;

  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: true,
    }).start();
  };

  const notification = [
    {
      description: 'r visuals are created to replace i',
      timestamp: 1200,
    },
    {
      description: 'r visuals are created to replace i',
      timestamp: 1200,
    },
    {
      description: 'r visuals are created to replace i',
      timestamp: 1200,
    },
  ];

  // let _get_notifications = async () => {
  //   setLoading(true);
  //   let get_url = base.BASE_URL + 'users/notifications/';
  //   let user = await AsyncStorage.getItem('user');
  //   let user_instance = JSON.parse(user);
  //   let access_token = state.user_data.access_token;
  //   axios
  //     .get(get_url, {
  //       headers: {
  //         Authorization: 'Bearer ' + access_token,
  //       },
  //     })
  //     .then((response) => {
  //       let {StatusCode, data} = response.data;
  //       if (StatusCode == 6000) {
  //         // setNotificaton(data.data);
  //         console.log(data.data, 'ooooooooooooooooooooooooooooooooooooooooo');
  //         setTimeout(() => {
  //           setLoading(false);
  //         }, 500);
  //       } else {
  //         // setVacancy_listPage([]);
  //         setTimeout(() => {
  //           setLoading(false);
  //         }, 500);
  //       }
  //     })
  //     .catch((error) => {
  //       console.log(error.message, '....................');
  //     });
  // };

  useEffect(() => {
    fadeIn();
  }, []);

  const Needs = () => {
    return notification.map((item, index) => (
      <TouchableOpacity
        key={item.pk}
        //   onPress={() => {
        //     TouchableHandler(item.pk);
        //     navigation.navigate('ApplicationListPage');
        //     console.log(item.pk);
        //   }}
        activeOpacity={0.9}
        style={styles.mainContainer}>
        <View style={styles.contentContairener}>
          <Text style={styles.notification}>{item.description}</Text>
        </View>
        <View style={styles.right}>
          <Text style={styles.bottomButtonText}>{item.timestamp}</Text>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <>
      <View style={{paddingVertical: 30, paddingHorizontal: 10}}>
        <BasicHeading title="Notifications" />
      </View>
      <ScrollView
        contentContainerStyle={{alignItems: 'center', paddingTop: 10}}>
        {/* {notification.length <= 0 ? ( */}
        <Animated.View
          style={[
            {
              opacity: fadeAnim,
            },
          ]}>
          {/* <Image
              style={styles.foundImage}
              source={require('../../../assets/vector-images/noresultfound.png')}
            /> */}
          <Text style={styles.messageText}>No Notifications</Text>
        </Animated.View>
        {/* ) : ( */}
        <View>{Needs()}</View>
        {/* )} */}
        <View style={{height: height * 0.2}} />
      </ScrollView>
    </>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 0.1,
  },

  contentContairener: {
    width: box_width * 0.55,
  },

  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  notification: {
    fontFamily: 'BalooPaaji2-Normal',
    fontSize: 14,
    // textTransform: 'capitalize',
    // marginBottom: 5,
    color: '#000',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.4,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  foundImage: {
    width: width * 0.5,
    height: width * 0.5,
    resizeMode: 'contain',
  },
  messageText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'BalooPaaji2-Regular',
  },
});
