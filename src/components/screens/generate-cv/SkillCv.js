import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Alert,
  TextInput,
} from 'react-native';
import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import {Picker} from '@react-native-picker/picker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as base from '../../../../Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import moment from 'moment';

const {width, height} = Dimensions.get('window');
const card_width = width * 0.27;
const card_height = width * 0.3;

export default function SkillCv({
  title,
  setTitle,
  level,
  setLevel,
  skill,
  setSkill,
}) {
  const ref_input2 = useRef();
  const ref_input3 = useRef();

  const [check, setCheck] = useState();

  const Tab = [
    {
      Box: 'Novice',
    },
    {
      Box: 'Intermediate',
    },
    {
      Box: 'Advace',
    },
    {
      Box: 'Expert',
    },
  ];

  const confirmHandler = (value) => {
    if (value == 'Novice') {
      setLevel(10);
      setCheck(value);
    } else if (value == 'Intermediate') {
      setLevel(20);
      setCheck(value);
    } else if (value == 'Advace') {
      setLevel(30);
      setCheck(value);
    } else if (value == 'Expert') {
      setLevel(40);
      setCheck(value);
    }
  };

  const [selectedBox, setSelectedBox] = useState();

  useEffect(() => {
    renderYearOptions;
  }, []);

  const renderYearOptions = () => {
    if (title && level) {
      skill.push({
        title: title,
        level: level,
      });
      setCheck('');
      setTitle('');
      setLevel('');
      Toast.show(' Added Successfully  ', Toast.SHORT);
    } else {
      Toast.show('Please enter all details', Toast.SHORT);
    }
  };

  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>4</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Tell us</Text> about your Skills
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Type your Skill here</Text>
            <TextInput
              value={title}
              style={styles.input}
              placeholder="Skill"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setTitle(val)}
              onSubmitEditing={() => ref_input2.current.focus()}
            />
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>How much know </Text>
            <View style={styles.loginUser1}>
              <View style={styles.tabView}>
                {Tab.map((item, index) => (
                  <TouchableOpacity
                    key={index}
                    onPress={() => {
                      confirmHandler(item.Box);
                    }}
                    style={[
                      styles.tabBox,
                      {
                        backgroundColor:
                          item.Box == check ? '#2261a6' : '#f5f6f9',
                      },
                    ]}>
                    <Text
                      style={[
                        styles.buttonText,
                        {color: item.Box == check ? '#fff' : '#000'},
                      ]}>
                      {item.Box}
                    </Text>
                  </TouchableOpacity>
                ))}
              </View>
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            setTimeout(() => {
              renderYearOptions();
            }, 500);
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Your Skills</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 10,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },

  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    backgroundColor: '#e0e6f7',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: 17,
  },
  subHeader: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    // backgroundColor: '#f5f6f9',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderColor: '#d8d6e2',
    flexWrap: 'wrap',
  },
  tabBox: {
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 5,
    borderWidth: 1,
    paddingHorizontal: width * 0.03,
    borderColor: '#d8d6e2',
  },
});
