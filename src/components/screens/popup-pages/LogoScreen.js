import React, {useState, useEffect, useRef, useContext} from 'react';
import {StyleSheet, View, Image, Dimensions} from 'react-native';
var {height, width} = Dimensions.get('window');

const LogoScreen = ({route}) => {
  return (
    <>
      <View style={styles.container}>
        <View style={styles.contentBox}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../../assets/vector-images/Group9570.png')}
              style={styles.image}
            />
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    minHeight: '100%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentBox: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    width: width * 0.7,
    height: width * 0.7,
  },
  image: {
    height: null,
    width: null,
    flex: 1,
    resizeMode: 'contain',
  },
});

export default LogoScreen;
