import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import moment from 'moment';
import {Context} from '../../contexts/Store';
import Modal from 'react-native-modal';
import DateTimePicker from '@react-native-community/datetimepicker';
import DegreeSearch from '../../screens/search/DegreeSearch';
import Toast from 'react-native-simple-toast';

const {width, height} = Dimensions.get('window');
const card_width = width * 0.27;
const card_height = width * 0.3;

export default function EductionCv({
  school,
  degree,
  startDate,
  place,
  endDate,
  description,
  education,
  setSchool,
  setDegree,
  setStartDate,
  setPlace,
  setEndDate,
  setDescription,
  setEducation,
  study,
  setStudy,
}) {
  const {state, dispatch} = useContext(Context);

  const [show, setShow] = useState(false);
  const [drag, setDrag] = useState(false);
  const [mode, setMode] = useState('date');
  const [date, setDate] = useState(new Date());
  const [label, setLabel] = useState();

  const ref_input3 = useRef();

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    // console.log(moment(currentDate).format('YYYY-MM-DD'));
    setStartDate(moment(currentDate).format('YYYY-MM-DD'));
    setShow(false);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setDrag(false);
    setMode(currentMode);
  };

  const onEndDate = (event, selectedValue) => {
    const liveDate = selectedValue;
    setEndDate(moment(liveDate).format('YYYY-MM-DD'));
    setDrag(false);
  };
  const pickeMode = (currentMode) => {
    setShow(false);
    setDrag(true);
    setMode(currentMode);
  };

  const [isModalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const renderYearOptions = () => {
    if (
      school &&
      degree &&
      study &&
      startDate &&
      endDate &&
      place &&
      description
    ) {
      education.push({
        school: school,
        degree: degree,
        study_field: study,
        start_date: startDate,
        end_date: endDate,
        place: place,
        description: description,
      });
      setSchool('');
      setDegree('');
      setStartDate();
      setEndDate();
      setPlace('');
      setStudy('');
      setDescription('');
      setLabel('');
      Toast.show(' Added Successfully  ', Toast.SHORT);
    } else {
      Toast.show('Please enter all details', Toast.SHORT);
    }
  };

  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>2</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Tell us</Text> about your Education
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>School</Text>
            <TextInput
              value={school}
              style={styles.input}
              placeholder="School"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setSchool(val)}
            />
          </View>
          <TouchableOpacity
            onPress={() => {
              toggleModal();
            }}
            style={styles.loginUser}>
            <Text style={styles.subHeader}>Degree</Text>
            <View style={styles.pickerBox}>
              {label ? (
                <Text style={styles.labelText}>
                  {label.length > 20 ? label.substring(0, 26) + '...' : label}
                </Text>
              ) : (
                <Text style={[styles.labelText, {color: '#aaa'}]}>
                  Select your qualification
                </Text>
              )}
              <Icon name="chevron-down" size={30} color="#000" />
            </View>
          </TouchableOpacity>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Study</Text>
            <TextInput
              value={study}
              style={styles.input}
              placeholder="Study"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setStudy(val)}
            />
          </View>

          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Start Date </Text>
            <View style={styles.pickerBox}>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onChange}
                />
              )}
              {startDate ? (
                <Text style={styles.dateStyle}>{startDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYYY-DD-MM</Text>
              )}
              <TouchableOpacity
                onPress={() => {
                  showMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>End Date </Text>
            <View style={styles.pickerBox}>
              {drag && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onEndDate}
                />
              )}
              {endDate ? (
                <Text style={styles.dateStyle}>{endDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYYY-DD-MM</Text>
              )}
              <TouchableOpacity
                onPress={() => {
                  pickeMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>City</Text>
            <TextInput
              value={place}
              style={styles.input}
              placeholder="Enter your city"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setPlace(val)}
              onSubmitEditing={() => ref_input3.current.focus()}
            />
          </View>
          <View style={[styles.loginUser]}>
            <Text style={styles.subHeader}>Description</Text>
            <View
              style={{
                height: height * 0.11,
                backgroundColor: '#f5f6f9',
                borderRadius: 20,
                borderWidth: 1,
                borderColor: '#d8d6e2',
              }}>
              <TextInput
                value={description}
                style={styles.inputLast}
                placeholder="Explain of education"
                ref={ref_input3}
                returnKeyType={'next'}
                onChangeText={(val) => setDescription(val)}
                multiline={true}
              />
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            renderYearOptions();
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Education</Text>
        </TouchableOpacity>
      </View>
      <Modal onBackButtonPress={toggleModal} isVisible={isModalVisible}>
        <View style={{flex: 1}}>
          <DegreeSearch
            setDegree={setDegree}
            setLabel={setLabel}
            toggleModal={toggleModal}
          />
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 10,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  pickerBox: {
    fontSize: 15,
    width: '100%',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputLast: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    paddingHorizontal: 20,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 5,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: 17,
  },
  subHeader: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  labelText: {
    fontFamily: 'BalooPaaji2-Regular',
    color: '#000',
    fontSize: 15,
  },
});
