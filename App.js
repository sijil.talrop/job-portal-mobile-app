import React, {useEffect, useContext} from 'react';
import {SafeAreaView, StyleSheet, StatusBar} from 'react-native';
import OfflinePopup from './src/components/screens/popup-pages/OfflinePopup';
import Base from './src/components/navigation/Base';
import Store from './src/components/contexts/Store';
import {NetworkProvider, NetworkConsumer} from 'react-native-offline';

const App = () => {
  return (
    // <View>
    //   <Note />
    // </View>
    <NetworkProvider>
      <NetworkConsumer>
        {({isConnected}) =>
          isConnected ? (
            <Store>
              <SafeAreaView style={[{flex: 1}]}>
                <StatusBar
                  barStyle="light-content"
                  backgroundColor="rgba(0, 0, 0, 0.2)"
                  hidden={true}
                  translucent={true}
                />
                <Base />
              </SafeAreaView>
            </Store>
          ) : (
            <OfflinePopup />
          )
        }
      </NetworkConsumer>
    </NetworkProvider>
  );
};

const styles = StyleSheet.create({
  scrollView: {},
});

export default App;
