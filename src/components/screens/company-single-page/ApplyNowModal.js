import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  TextInput,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import CheckBox from '@react-native-community/checkbox';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';
const {height, width} = Dimensions.get('window');
import DocumentPicker from 'react-native-document-picker';
import {Alert} from 'react-native';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';

export default function ApplyNowModal({route}) {
  const {state, dispatch} = useContext(Context);

  const navigation = useNavigation();
  const [selectedRating, setSelectedRating] = useState(false);
  const [toggleCheckBox, setToggleCheckBox] = useState(false);
  const [name, setName] = useState(
    state.user_data.profile_data.first_name +
      state.user_data.profile_data.last_name,
  );
  const [phone, setPhone] = useState(state.user_data.profile_data.phone);
  const [email, setEmail] = useState(state.user_data.profile_data.email);
  const [cover_letter, setCover_letter] = useState('');
  const [imageName, setImageName] = useState();

  let display_name =
    state.user_data.profile_data.first_name +
    state.user_data.profile_data.last_name;

  const [isFocused, setIsFocused] = useState({
    name: false,
    email: false,
    phone: false,
    optional: false,
  });
  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };

  const category = [
    {
      name: 'Choose File',
    },
    {
      name: 'No file Chosen',
    },
  ];
  const [singleFile, setSingleFile] = useState(null);

  const selectFile = async () => {
    try {
      const res = await DocumentPicker.pick({
        type: [DocumentPicker.types.allFiles],
      });
      console.log('res : ' + JSON.stringify(res));
      setSingleFile(res);
      setSelectedRating(true);
      setImageName(res.name);
    } catch (err) {
      setSingleFile(null);
      if (DocumentPicker.isCancel(err)) {
        alert('Canceled');
      } else {
        alert('Unknown Error: ' + JSON.stringify(err));
        throw err;
      }
    }
  };

  const renderJson = async () => {
    let access_token = state.user_data.access_token;
    const fileToUpload = singleFile;
    const data = new FormData();
    data.append('resume', fileToUpload);
    data.append('name', name);
    data.append('email', email);
    data.append('phone', phone);
    data.append('cover_letter', cover_letter);
    data.append('toggleCheckBox', toggleCheckBox);
    let post_url =
      base.BASE_URL + 'jobs/apply-job/' + route.params.item.pk + '/';
    if (singleFile) {
      let res = await fetch(post_url, {
        method: 'post',
        body: data,
        headers: {
          'Content-Type': 'multipart/form-data; ',
          Authorization: 'Bearer ' + access_token,
        },
      });
      let responseJson = await res.json();
      if (responseJson.StatusCode == 6000) {
        Toast.show(responseJson.data.message, Toast.SHORT);
        setTimeout(() => {
          navigation.navigate('SuccessPopup');
        }, 500);
      } else {
        Toast.show(responseJson.data.message, Toast.SHORT);
        setTimeout(() => {
          navigation.goBack();
        }, 500);
      }
    } else {
      Alert.alert(
        'Upload File',
        'Please upload file document of certificate ',
        [{text: 'Retry'}],
        {
          cancelable: false,
        },
      );
    }
  };

  const ChooseFile = () => {
    return (
      <>
        <View style={{marginBottom: 10}}>
          <Text style={styles.header}>Resume</Text>
        </View>
        <TouchableOpacity style={styles.flexBox}>
          <TouchableOpacity
            onPress={() => {
              selectFile();
            }}
            style={[
              styles.mainContainer,
              {borderColor: selectedRating ? '#ef801f' : '#9ba0ab'},
            ]}>
            <Text
              style={[
                styles.itemFont,
                {color: selectedRating ? '#ef801f' : '#9ba0ab'},
              ]}>
              Choose File
            </Text>
          </TouchableOpacity>
          {imageName != null && (
            <TouchableOpacity
              onPress={() => {
                selectFile();
              }}
              style={styles.mainContainer2}>
              <Text style={styles.itemFont2}>
                {imageName && imageName.length > 20
                  ? imageName.substring(0, 20) + '...'
                  : imageName}
              </Text>
            </TouchableOpacity>
          )}
        </TouchableOpacity>
        <View style={[styles.loginUser, {marginVertical: 10}]}>
          <Text style={[styles.header, {marginBottom: 15}]}>
            Cover letter (optional)
          </Text>
          <View
            style={[
              styles.letterContentBox,
              {borderColor: isFocused.optional ? '#ef8731' : '#aaa'},
            ]}>
            <TextInput
              value={cover_letter}
              style={[styles.optionalInput]}
              placeholderTextColor={isFocused.optional ? '#000' : '#aaa'}
              textContentType="name"
              keyboardType="default"
              autoCompleteType="off"
              multiline={true}
              onFocus={() => handleInputFocus('optional')}
              onBlur={() => handleInputBlur('optional')}
              onChangeText={(val) => setCover_letter(val)}
            />
          </View>
        </View>
        <View>
          <View style={styles.checkBoxContainer}>
            <CheckBox
              style={{
                color: 'red',
                backgroundColor: '#fff',
                marginRight: 10,
                transform: [{scaleX: 0.7}, {scaleY: 0.7}],
              }}
              disabled={false}
              value={toggleCheckBox}
              onValueChange={(newValue) => setToggleCheckBox(newValue)}
            />
            <View style={{width: width * 0.7}}>
              <Text style={styles.checkBoxText}>
                Notify me when similar jobs available{'\n'}
              </Text>
              <Text style={styles.checkBoxText}>
                By checking this box and clicking continue, you agree to our
                terms and you agree to receive similar jobs via email .You can
                change your consent settings at any time by{'\n'}
              </Text>
              <Text style={styles.checkBoxText}>
                By pressing continue,you will see questions from the employer
                that are part of this application
              </Text>
            </View>
          </View>
          <TouchableOpacity
            onPress={() => {
              renderJson();
            }}
            style={styles.button}>
            <Text style={styles.buttonText}>Apply Now</Text>
          </TouchableOpacity>
        </View>
      </>
    );
  };

  return (
    <ImageBackground
      source={require('../../../assets/vector-images/Group20260.png')}
      style={styles.backgroundImageBox}>
      <ScrollView contentContainerStyle={styles.ContentBox}>
        <View style={styles.TopContent}>
          <Text style={styles.companyName}>{route.params.item.name}</Text>
          <View style={styles.detailBox}>
            <Icon name={'location-pin'} color={'#4eeb12'} size={20} />
            <Text style={styles.location}>
              {route.params.item.location.length > 17
                ? route.params.item.location.substring(0, 40) + '...'
                : route.params.item.location}
            </Text>
          </View>
          <Text style={styles.amount}> ₹20-25/m</Text>
        </View>
        <View style={[styles.middleContent]}>
          <View style={styles.PickerContent}>
            <TouchableOpacity style={styles.loginUser}>
              <TextInput
                style={[
                  styles.input,
                  {borderColor: isFocused.name ? '#ef8731' : '#aaa'},
                ]}
                value={name}
                placeholder="Name"
                placeholderTextColor={isFocused.name ? '#000' : '#aaa'}
                textContentType="name"
                keyboardType="default"
                onFocus={() => handleInputFocus('name')}
                onBlur={() => handleInputBlur('name')}
                onChangeText={(val) => setName(val)}
              />
            </TouchableOpacity>
          </View>
          <View style={styles.PickerContent}>
            <View style={styles.loginUser}>
              <TextInput
                style={[
                  styles.input,
                  {borderColor: isFocused.email ? '#ef8731' : '#aaa'},
                ]}
                value={email}
                placeholder="Email"
                placeholderTextColor={isFocused.email ? '#000' : '#aaa'}
                textContentType="emailAddress"
                keyboardType="email-address"
                onFocus={() => handleInputFocus('email')}
                onBlur={() => handleInputBlur('email')}
                onChangeText={(val) => setEmail(val)}
              />
            </View>
          </View>
          <View style={styles.PickerContent}>
            <View style={styles.loginUser}>
              <TextInput
                style={[
                  styles.input,
                  {borderColor: isFocused.phone ? '#ef8731' : '#aaa'},
                ]}
                value={phone}
                placeholder="Phone"
                placeholderTextColor={isFocused.phone ? '#000' : '#aaa'}
                keyboardType="phone-pad"
                onFocus={() => handleInputFocus('phone')}
                onBlur={() => handleInputBlur('phone')}
                onChangeText={(val) => setPhone(val)}
                editable={false}
              />
            </View>
          </View>
          <View>{ChooseFile()}</View>
        </View>
      </ScrollView>
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  backgroundImageBox: {
    resizeMode: 'cover',
    width: width,
    minHeight: '100%',
  },
  middleContent: {
    paddingHorizontal: 30,
    borderWidth: 1,
    borderColor: '#eee',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingVertical: 30,
    backgroundColor: '#fff',
  },
  ContentBox: {
    paddingTop: '10%',
    backgroundColor: 'rgba(0,0,0,0.2)',
  },
  TopContent: {
    alignItems: 'center',
    marginBottom: 20,
  },
  loginUser: {
    borderColor: '#ef801f',
    marginBottom: 20,
  },
  input: {
    borderWidth: 1,
    borderRadius: 15,
    paddingHorizontal: 10,
  },
  optionalInput: {
    // width: width * 0.2,
    paddingHorizontal: 10,
  },
  container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
    width: width,
  },
  itemFont: {
    fontFamily: 'BalooPaaji2-SemiBold',
    textAlign: 'center',
    fontSize: 13,
  },
  itemFont2: {
    fontFamily: 'BalooPaaji2-Regular',
    textAlign: 'center',
    fontSize: RFPercentage(1.6),
  },
  mainContainer: {
    paddingHorizontal: 8,
    paddingVertical: 10,
    borderRadius: 12,
    margin: 5,
    width: width * 0.3,
    backgroundColor: '#fff',
    borderWidth: 1,
  },
  mainContainer2: {
    paddingHorizontal: 8,
    paddingVertical: 10,
    borderRadius: 12,
    margin: 5,
    width: width * 0.45,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ef801f',
  },
  header: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFPercentage(1.6),
    // second argument is standardScreenHeight(optional),
  },
  location: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.6),
  },
  amount: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
  companyName: {
    fontSize: RFValue(10, width),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  detailBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginVertical: 10,
  },
  description: {
    color: '#8e8e8e',
    fontSize: 16,
    fontFamily: 'BalooPaaji2-Regular',
  },
  attachmentComponent: {
    flexDirection: 'row',
    alignItems: 'baseline',
  },
  resumeText: {
    fontFamily: 'BalooPaaji2-Regular',
    color: '#7b7b7b',
  },
  create: {
    fontSize: 16,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#ef862d',
  },
  bottomContent: {
    marginTop: 15,
  },
  letterContentBox: {
    height: width * 0.2,
    borderRadius: 12,
    borderWidth: 0.5,
  },
  checkBoxText: {
    color: '#5d5d5e',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    lineHeight: 18,
  },
  checkBoxContainer: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  button: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 15,
    marginTop: 20,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFPercentage(1.6),
    color: '#fff',
  },
});
