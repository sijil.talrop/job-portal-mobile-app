import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Linking,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import AwardCv from './AwardCv';
import ExperienceCv from './ExperienceCv';
import SkillCv from './SkillCv';
import EducationCv from './EducationCv';
import SocialMediaCv from './SocialMediaCv';
import BasicCv from './BasicCv';
import BasicHeading from '../header/BasicHeading';
import * as base from '../../../../Settings';
import axios from 'axios';

import Toast from 'react-native-simple-toast';
import {Context} from '../../contexts/Store';
const {width, height} = Dimensions.get('window');
export default function CreateProfile({route}) {
  const {state, dispatch} = useContext(Context);
  const navigation = useNavigation();
  const [linkAddress, setLinkAddress] = useState();

  //   ++++basic++++
  const [scroll, setScroll] = useState(false);
  const [first_name, setFirst_name] = useState();
  const [lastname, setLastname] = useState();
  const [email, setEmail] = useState();
  const [dateOfbirth, setdateOfbirth] = useState();
  const [address, setAddress] = useState();
  const [about, setAbout] = useState();
  const [phone, setPhone] = useState();
  const [byke, setByke] = useState();
  const [gender, setGender] = useState();
  const [objective, setObjective] = useState();
  const [selectedBox, setSelectedBox] = useState();

  //   ++++education+++

  const [school, setSchool] = useState();
  const [degree, setDegree] = useState();
  const [study, setStudy] = useState();
  const [startDate, setStartDate] = useState();
  const [place, setPlace] = useState();
  const [endDate, setEndDate] = useState();
  const [description, setDescription] = useState();
  const [education, setEducation] = useState([]);

  //   ++++experience+++++

  const [jobTitle, setJobTitle] = useState();
  const [employer, setEmployer] = useState();
  const [fromDate, setFromDate] = useState();
  const [toDate, setToDate] = useState();
  const [city, setCity] = useState();
  const [coverLetter, setCoverLetter] = useState();
  const [experience, setExperience] = useState([]);

  //   +++skill+++
  const [title, setTitle] = useState();
  const [level, setLevel] = useState();
  const [skill, setSkill] = useState([]);
  //   console.warn(skill);

  // ++++award+++

  const [awardName, setAwardName] = useState();
  const [awardFrom, setAwardFrom] = useState();
  const [awardTo, setAwardTo] = useState();
  const [exPlain, setExplain] = useState();
  const [award, setAward] = useState([]);

  //   ++++Socialmedia+++
  const [media, setMedia] = useState();
  const [link, setLink] = useState();
  const [social_media, setSocial_media] = useState([]);

  console.warn(social_media, 'ucation');

  const [required, setRequired] = useState(false);

  const registration = async () => {
    let post_url = base.BASE_URL + 'resumes/generate-cv/';
    let access_token = state.user_data.access_token;
    axios
      .post(
        post_url,
        [
          {
            resume: {
              first_name: first_name,
              last_name: lastname,
              email: email,
              phone: phone,
              about: about,
              gender: selectedBox,
              dob: dateOfbirth,
              marital_status: 10,
              address: address,
              objective: objective,
            },
            experience: experience,
            skill: skill,
            education: education,
            award: award,
            social_media: social_media,
          },
        ],
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
            'Content-Type': 'application/json; ',
          },
        },
      )

      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          Toast.show(data.message, Toast.SHORT);
          //   console.warn(data.message, 'CORRECT');
          console.warn(data, 'CORRECT');
          setLinkAddress(data);
          setRequired(true);
        } else {
          Toast.show(data.message, Toast.SHORT);
          console.warn(data.message, '=INCORRECT');
          setSocial_media([]);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  return (
    <ScrollView style={{}} contentContainerStyle={styles.mainContainer}>
      <View>
        <View style={styles.headerBox}>
          <BasicHeading title="Generate Cv" />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <BasicCv
            setFirst_name={setFirst_name}
            setLastname={setLastname}
            setPhone={setPhone}
            setEmail={setEmail}
            setdateOfbirth={setdateOfbirth}
            setAddress={setAddress}
            setGender={setGender}
            setAbout={setAbout}
            setObjective={setObjective}
            setSelectedBox={setSelectedBox}
            first_name={first_name}
            lastname={lastname}
            email={email}
            dateOfbirth={dateOfbirth}
            address={address}
            about={about}
            phone={phone}
            gender={gender}
            objective={objective}
            selectedBox={selectedBox}
          />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <EducationCv
            school={school}
            degree={degree}
            startDate={startDate}
            place={place}
            endDate={endDate}
            description={description}
            education={education}
            study={study}
            setStudy={setStudy}
            setSchool={setSchool}
            setDegree={setDegree}
            setStartDate={setStartDate}
            setPlace={setPlace}
            setEndDate={setEndDate}
            setDescription={setDescription}
            setEducation={setEducation}
          />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <ExperienceCv
            jobTitle={jobTitle}
            setJobTitle={setJobTitle}
            employer={employer}
            setEmployer={setEmployer}
            fromDate={fromDate}
            setFromDate={setFromDate}
            toDate={toDate}
            setToDate={setToDate}
            city={city}
            setCity={setCity}
            coverLetter={coverLetter}
            setCoverLetter={setCoverLetter}
            experience={experience}
            setExperience={setExperience}
          />
        </View>

        <View style={{marginBottom: height * 0.06}}>
          <SkillCv
            title={title}
            setTitle={setTitle}
            level={level}
            setLevel={setLevel}
            skill={skill}
            setSkill={setSkill}
          />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <AwardCv
            awardName={awardName}
            setAwardName={setAwardName}
            awardFrom={awardFrom}
            setAwardFrom={setAwardFrom}
            awardTo={awardTo}
            setAwardTo={setAwardTo}
            exPlain={exPlain}
            setExplain={setExplain}
            award={award}
            setAward={setAward}
          />
        </View>

        <View style={{marginBottom: height * 0.06}}>
          <SocialMediaCv
            media={media}
            setMedia={setMedia}
            link={link}
            setLink={setLink}
            social_media={social_media}
            // setSocial_media={setSocial_media}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-around'}}>
          <TouchableOpacity
            onPress={() => {
              registration();
            }}
            style={[styles.button2, {backgroundColor: '#ef803a'}]}>
            <Icon name={'content-save'} color={'#fff'} size={20} />
            <Text style={styles.uploadText2}>Save</Text>
          </TouchableOpacity>
          {required ? (
            <TouchableOpacity
              onPress={() => {
                Linking.openURL(
                  'http://192.168.43.124:8000/resumes/download-cv/' +
                    linkAddress.pk +
                    '/',
                );
              }}
              style={[styles.button2, {backgroundColor: '#0462bf'}]}>
              <Icon name={'update'} color={'#fff'} size={20} />
              <Text style={styles.uploadText2}>Download-Cv</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              onPress={() => {
                Toast.show('Click save Button', Toast.SHORT);
              }}
              style={[styles.button2, {backgroundColor: '#0462bf'}]}>
              <Icon name={'update'} color={'#fff'} size={20} />
              <Text style={styles.uploadText2}>Download-Cv</Text>
            </TouchableOpacity>
          )}
        </View>
        <View style={{height: height * 0.05}} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
    paddingVertical: '10%',
    backgroundColor: '#fff',
  },
  headerBox: {
    marginBottom: '15%',
  },
  leftIcon: {
    width: 30,
    height: 30,
    borderRadius: 10,
    backgroundColor: '#dbdae4',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 18,
  },
  imageContainer: {
    width: width * 0.3,
    height: width * 0.3,
    borderRadius: 100,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: '#ddd',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    borderRadius: 100,
    overflow: 'hidden',
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 14,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderWidth: 1,
  },
  pickerBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  inputLast: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    paddingHorizontal: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f5f6f9',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginVertical: 20,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  tabBox: {
    width: width * 0.25,
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 5,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  subHeader: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  scrollStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageContent: {
    width: width * 0.4,
    height: width * 0.3,
    backgroundColor: '#fff',
  },
  companyImage: {
    width: width * 0.3,
    height: width * 0.25,
    borderWidth: 1,
    borderColor: '#aaa',
    borderRadius: 15,
  },
  image1: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 10,
  },
  bottomButton: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 100,
    marginTop: 20,
    width: width * 0.2,
    height: width * 0.1,
    borderWidth: 1,
    elevation: 0.1,
    borderColor: '#0462bf',
  },
  bottomText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#fff',
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  button: {
    backgroundColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  buttonBox: {
    padding: 5,
    backgroundColor: '#ef803a',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#2261a6',
    // marginTop: 10,
  },
  buttonBox2: {
    backgroundColor: '#ef803a',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: 100,
    marginTop: 10,
    paddingVertical: 5,
  },
  uploadButtonText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 12,
  },
  companyText: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
    marginTop: 5,
  },
  button2: {
    width: width * 0.4,
    backgroundColor: 'red',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    paddingVertical: 10,
    borderRadius: 15,
  },
  uploadText2: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
    marginLeft: 15,
  },
});
