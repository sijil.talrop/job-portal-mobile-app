import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as base from '../../../../Settings';
import Toast from 'react-native-simple-toast';
import axios from 'axios';

const {width, height} = Dimensions.get('window');
const card_width = width * 0.27;
const card_height = width * 0.3;

export default function SocialMediaCv({
  media,
  setMedia,
  link,
  setLink,
  social_media,
  // setSocial_media,
}) {
  const [titleList, setTitleList] = useState();
  const ref_input2 = useRef();

  useEffect(() => {
    get_title_list();
    // renderYearOptions();
  }, []);

  const confirmHandler = (value) => {
    setMedia(value);
  };

  // const renderYearOptions = () => {
  //   social_media.push({
  //     title: media,
  //     url: link,
  //   });
  //   setMedia();
  //   setLink('');
  // };

  const get_title_list = () => {
    let post_url = base.BASE_URL + 'users/social-media-title/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setTitleList(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>6</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Social</Text> Media
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Social Media</Text>
            <View style={styles.loginUser1}>
              <View style={styles.tabView}>
                {titleList &&
                  titleList.length > 0 &&
                  titleList.map((item, index) => (
                    <TouchableOpacity
                      key={index}
                      onPress={() => {
                        confirmHandler(item.value);
                      }}
                      style={[
                        styles.tabBox,
                        {
                          backgroundColor:
                            item.value == media ? '#2261a6' : '#f5f6f9',
                        },
                      ]}>
                      <Text
                        style={[
                          styles.buttonText,
                          {color: item.value == media ? '#fff' : '#000'},
                        ]}>
                        {item.label}
                      </Text>
                    </TouchableOpacity>
                  ))}
              </View>
            </View>
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Link</Text>
            <TextInput
              value={link}
              style={styles.input}
              placeholder="Link"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setLink(val)}
              onSubmitEditing={() => ref_input2.current.focus()}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            // renderYearOptions();
            if (media && link) {
              social_media.push({
                title: media,
                url: link,
              });
              setMedia(0);
              setLink('');
              Toast.show(' Added Successfully  ', Toast.SHORT);
            } else {
              Toast.show('Please enter all details', Toast.SHORT);
            }
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Social Media</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 10,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  input2: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    backgroundColor: '#f5f6f9',
    width: '70%',
  },
  loginUser1: {
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 5,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: 17,
  },
  subHeader: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  tabView: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderColor: '#d8d6e2',
    flexWrap: 'wrap',
  },
  tabBox: {
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 5,
    borderWidth: 1,
    paddingHorizontal: width * 0.03,
    borderColor: '#d8d6e2',
    margin: 5,
  },
});
