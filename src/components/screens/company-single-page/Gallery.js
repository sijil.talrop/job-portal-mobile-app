import React from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
const {width} = Dimensions.get('window');
const box_width = width * 0.4;
const box_height2 = width * 0.37;
const height = width * 0.6; //60%
const box_width2 = width * 0.32;

export default function Gallery(props) {
  const team = [
    {
      image:
        'https://cdn.pixabay.com/photo/2018/01/06/09/25/hijab-3064633_960_720.jpg',
      name: 'Jifna Raj',
    },
    {
      image:
        'https://cdn.pixabay.com/photo/2015/10/12/14/59/girl-984060_960_720.jpg',
      name: 'Ann Dennifer',
    },
    {
      image:
        'https://cdn.pixabay.com/photo/2016/09/24/23/34/woman-1692849_960_720.jpg',
      name: 'Romitto Joseph',
    },
    {
      image:
        'https://cdn.pixabay.com/photo/2020/10/26/18/20/man-5688155_960_720.jpg',
      name: 'Jhon Tennison',
    },
    {
      image:
        'https://cdn.pixabay.com/photo/2017/03/05/23/14/girl-2120196_960_720.jpg',
      name: 'Jhon Tennison',
    },
  ];

  const renderTeam = () => {
    return (
      <View>
        {props.team_member != '' && (
          <View>
            <Text style={styles.header}>Team Member</Text>
            <ScrollView
              pagingEnabled
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.scrollStyle}
              onScroll={''}>
              {props.team_member.map((item, index) => (
                <TouchableOpacity activeOpacity={0.8} style={styles.secondBox}>
                  <View style={styles.memberImage}>
                    <Image style={styles.imageBox} source={{uri: item.image}} />
                  </View>
                  <View style={styles.contentBox}>
                    <Text style={styles.name}>{item.name}</Text>
                    <Text style={styles.middleText}>{item.designation}</Text>
                    {/* <Text style={styles.bottomText}>Experience 7 Years</Text> */}
                  </View>
                </TouchableOpacity>
              ))}
            </ScrollView>
          </View>
        )}
      </View>
    );
  };

  const renderGallery = () => {
    return (
      <View
        style={{
          marginVertical: '5%',
          // alignItems: 'center',
        }}>
        {props.gallery != '' && (
          <>
            <Text style={styles.header}>Gallery</Text>
            <ScrollView
              pagingEnabled
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              contentContainerStyle={styles.scrollStyle}
              onScroll={''}>
              {props.gallery.map((item, index) => (
                <TouchableOpacity key={index}>
                  <ImageBackground
                    source={{uri: item.image}}
                    // source={item.image}
                    // height={100}
                    // width="100%"
                    overlayColor="#000"
                    overlayAlpha={0.5}
                    style={styles.bannerContainer}></ImageBackground>
                </TouchableOpacity>
              ))}
              <View style={{width: box_width * 0.51}} />
            </ScrollView>
          </>
        )}
        {/* {renderTeam()} */}
      </View>
    );
  };

  return (
    <View>
      {renderGallery()}
      {renderTeam()}
    </View>
  );
}

const styles = StyleSheet.create({
  header: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 20,
  },
  scrollStyle: {},
  bannerContainer: {
    width: box_width,
    height: box_width * 0.7,
    overflow: 'hidden',
    margin: 10,
    borderWidth: 0.5,
    borderColor: '#eee',
    borderRadius: 15,
  },
  banner: {
    overflow: 'hidden',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  title: {
    fontSize: 23,
    color: '#fff',
    textAlign: 'center',
    marginBottom: 10,
  },
  pagingText: {
    color: '#ccc',
    margin: 3,
  },
  activePagingText: {
    color: '#fff',
    margin: 3,
  },
  Pagination: {
    flexDirection: 'row',
    position: 'absolute',
    bottom: 7,
    alignSelf: 'center',
    flexDirection: 'row',
  },
  text: {
    color: '#fff',
    fontSize: 14,
    textAlign: 'center',
    marginBottom: 10,
    fontFamily: 'Poppins-LightItalic',
  },
  buttonBox: {
    width: 40,
    height: 40,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  //   secondBox alignments
  secondBox: {
    alignItems: 'center',
    width: box_width2,
    height: box_width + 20,
    margin: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    borderRadius: 15,
    backgroundColor: '#fff',
    elevation: 1,
  },
  memberImage: {
    width: box_width2,
    height: box_height2 * 0.55,
  },
  imageBox: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 15,
  },
  contentBox: {
    alignItems: 'center',
    paddingHorizontal: 10,
    // justifyContent: 'flex-start',
  },
  name: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    height: box_height2 * 0.2,
  },
  middleText: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  bottomText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 14,
    color: '#ef801f',
  },
});
