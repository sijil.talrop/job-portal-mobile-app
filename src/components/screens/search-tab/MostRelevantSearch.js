import React, {useState, useEffect, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BasicHeading from '../header/BasicHeading';
import CommonSearchBar from '../home/CommonSearchBar';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.32;
const card_width = width * 0.6;
const box_width = width - 40;

export default function MostRelevantSearch() {
  const [like, setLike] = useState(true);
  const [product, setproducts] = useState([]);
  const [selectedRating, setSelectedRating] = useState();
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  useEffect(() => {
    if (isNaN(earnings)) {
      setSelectedRating(earnings[0].product_name);
    }
  }, []);

  const earnings = [
    {
      id: 1,
      image: 'https://www.finscreener.org/system_images/logo300x300button.png',
      product_name: 'Fountain',
      Development: 'Product Designer',
      price: '30',
      icon: 'false',
    },
    {
      id: 2,
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRsDo1EBoDAvQA9v6ocBdyp_9vOzHb8mcA5Rg&usqp=CAU',
      product_name: 'Trade MArk',
      Development: 'Executive Manager',
      price: '45',
      icon: 'False',
    },
    {
      id: 3,
      image: 'https://dailybubble.io/wp-content/uploads/icon-G_S.1.png',
      product_name: 'Shop Clue',
      Development: 'Assistant Director',
      price: '20',
      icon: 'true',
    },
    {
      id: 4,
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTlhUPNAo2cBGxslM0yxZCMKBcPqI7OVgmxAA&usqp=CAU',
      product_name: 'Boats',
      price: '30',
      Development: 'Director of the Team ',
      icon: 'true',
    },
    {
      id: 5,
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQAJ0JloiNGO_-OSzBvELg-BaBA6OtobF09IQ&usqp=CAU',
      product_name: 'Intro Gamer',
      price: '30',
      Development: 'Theme Designer',
      icon: 'true',
    },
    {
      id: 6,
      image: 'https://fontmeme.com/images/garena-free-fire-font.jpg',
      product_name: 'Free Fire',
      price: '30',
      Development: 'Assistant Designer',
      icon: 'true',
    },
    {
      id: 7,
      image:
        'https://img.freepik.com/free-vector/gamer-logo-with-tagline_1043-109.jpg?size=338&ext=jpg',
      product_name: 'Wings',
      price: '30',
      Development: 'Cartoon Maker',
      icon: 'true',
    },
  ];

  const Needs = () => {
    return earnings.map((item, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          TouchableHandler(item.product_name);
        }}
        activeOpacity={0.9}
        style={[
          styles.mainContainer,
          {
            backgroundColor:
              item.product_name == selectedRating ? '#2261a6' : '#fff',
          },
        ]}>
        <View style={styles.left}>
          <View
            style={[
              styles.lefContent,
              {
                backgroundColor:
                  item.product_name == selectedRating ? '#fff' : '#e2e2e2',
              },
            ]}>
            <View style={styles.imageContainer}>
              <Image source={{uri: item.image}} style={styles.productImage} />
            </View>
          </View>
        </View>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.introName,
              {color: item.product_name == selectedRating ? '#fff' : '#adabbb'},
            ]}>
            {item.product_name}
          </Text>
          <Text
            style={[
              styles.name,
              {color: item.product_name == selectedRating ? '#fff' : '#130937'},
            ]}>
            {item.Development}
          </Text>
          <View style={styles.Location}>
            <Text
              style={[
                styles.amount,
                {
                  color:
                    item.product_name == selectedRating ? '#fff' : '#130937',
                },
              ]}>
              ₹20-25/m
            </Text>
            <View style={styles.locationCharge}>
              <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
              <Text
                style={[
                  styles.place,
                  {
                    color:
                      item.product_name == selectedRating ? '#fff' : '#130937',
                  },
                ]}>
                16 Ave New York
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.right}>
          {item.icon == 'true' ? (
            <View style={styles.rateContainer}>
              <Icon name={'bookmark'} color={'#eee'} size={55} />
            </View>
          ) : (
            <View style={styles.rateContainer}>
              <Icon name={'bookmark'} color={'#ef801f'} size={55} />
            </View>
          )}
          <View style={styles.delivery}>
            <Text
              style={[
                styles.bottomButtonText,
                {
                  color: item.product_name == selectedRating ? '#fff' : '#aaa',
                },
              ]}>
              5Hrs
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <View style={{}}>
      {Needs()}
      <View style={{height: height * 0.5}} />
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 1,
  },
  headText: {
    fontSize: 14,
    marginBottom: 20,
  },
  left: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: box_width * 0.21,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.55,
  },
  lefContent: {
    height: Reward_Box_Height * 0.4,
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 10,
    borderRadius: 30,
  },
  Location: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  amount: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#393057',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.16,
    height: Reward_Box_Height,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  cartLeft: {
    width: card_width,
    height: Reward_Box_Height,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: '#fff',
    elevation: 4,
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
    // elevation: 0.5,
  },
});
