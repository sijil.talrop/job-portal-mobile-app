import React, {useState, useEffect, useRef, useContext} from 'react';
import {StyleSheet, Text, View, Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
var {height, width} = Dimensions.get('window');
import {CommonActions, useNavigation} from '@react-navigation/native';

const SuccessPopup = ({route}) => {
  setTimeout(() => {
    navigation.navigate('Home');
  }, 1000);
  const navigation = useNavigation();
  return (
    <>
      <View style={styles.container}>
        <View style={styles.contentBox}>
          <View style={styles.imageContainer}>
            <Icon
              name="checkbox-marked-circle-outline"
              size={150}
              color="#fff"
            />
            <Text style={styles.boldText}>Success</Text>
            <Text style={styles.description}>
              Your Application Successfully Created
            </Text>
          </View>
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    minHeight: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2261a6',
  },
  contentBox: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    alignItems: 'center',
  },
  image: {
    height: null,
    width: null,
    flex: 1,
    resizeMode: 'contain',
  },
  boldText: {
    fontSize: 26,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
  },
  description: {
    fontSize: 20,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
  },
});

export default SuccessPopup;
