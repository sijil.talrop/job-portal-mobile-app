import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  TextInput,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Animated,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import * as base from '../../../../Settings';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';
import {useNavigation} from '@react-navigation/native';
import {Context} from '../../contexts/Store';

var {height, width} = Dimensions.get('window');

const SignInPage = (props) => {
  const navigation = useNavigation();

  // textinput Props and functions start
  const [phone, setPhone] = useState('');
  const [loading, setLoading] = useState(false);
  const [isFocused, setIsFocused] = useState({
    phone: false,
  });
  const {state, dispatch} = useContext(Context);

  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };
  const bottomValue = useRef(new Animated.Value(100)).current;
  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 9000,
    }).start();
  };
  useEffect(() => {
    jumpUp();
  });

  const registration = () => {
    let post_url = base.BASE_URL + 'users/sign-in/';
    console.log(post_url);
    axios
      .post(post_url, {
        phone: phone,
        current_role: 'candidate',
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          navigation.navigate('Otp', {
            phone: phone,
          });
          setLoading(false);
        } else {
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  return (
    <>
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.contentBox}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../../assets/vector-images/Group2039.png')}
              style={styles.image}
            />
          </View>
          <Animated.View
            style={[
              styles.box,
              {
                transform: [{translateY: bottomValue}],
              },
            ]}>
            <View style={styles.middle}>
              <Text style={styles.errorText}>OTP Verification</Text>
              <Text style={styles.content}>
                We will send an One Time Password {'\n'} on this mobile number
              </Text>
            </View>
            <View>
              <Text style={styles.number}>Enter your Number</Text>
              <View
                style={[
                  styles.inputBox,
                  {borderColor: isFocused.phone ? '#ef801f' : '#aaa'},
                ]}>
                <TextInput
                  style={styles.input}
                  underlineColorAndroid="transparent"
                  placeholder="Mobile Number"
                  placeholderTextColor={isFocused.name ? '#000' : '#aaa'}
                  autoCapitalize="none"
                  keyboardType="numeric"
                  onFocus={() => {
                    handleInputFocus('phone');
                  }}
                  onBlur={() => {
                    handleInputBlur('phone');
                  }}
                  onChangeText={(val) => {
                    setPhone(val);
                  }}
                />
                <Icon name={'phone-outline'} color={'#4eeb12'} size={20} />
              </View>
            </View>
            <TouchableOpacity
              onPress={() => {
                if (phone.length == 10) {
                  registration();
                } else {
                  Toast.show('Please enter valid mobile number', Toast.SHORT);
                }
              }}
              activeOpacity={0.8}
              style={styles.button}>
              <Text style={styles.buttonText}>Get OTP</Text>
            </TouchableOpacity>
          </Animated.View>
        </View>
      </TouchableWithoutFeedback>
    </>
  );
};

const styles = StyleSheet.create({
  contentBox: {
    justifyContent: 'flex-end',
    minHeight: '100%',
    alignItems: 'center',
    paddingBottom: 50,
    backgroundColor: '#fff',
  },
  imageContainer: {
    width: width * 0.9,
    height: width * 0.7,
  },
  image: {
    height: null,
    width: null,
    flex: 1,
    resizeMode: 'contain',
  },

  errorText: {
    fontSize: RFValue(11, width), // second argument is standardScreenHeight(optional),
    color: '#000',
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    textAlign: 'center',
  },
  content: {
    fontSize: RFPercentage(1.8),
    textAlign: 'center',
    color: '#757575',
    fontFamily: 'BalooPaaji2-Regular',
  },
  middle: {
    width: width,
    padding: 40,
  },
  box: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  button: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 15,
    marginTop: 20,
    width: width * 0.9,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 16,
    color: '#fff',
  },
  loginUser: {
    borderColor: '#ef801f',
    marginBottom: 20,
    width: width * 0.9,
  },
  input: {
    width: width * 0.6,
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderWidth: 1,
    width: width * 0.9,
    borderRadius: 15,
  },
  number: {
    fontSize: RFPercentage(1.9),
    textAlign: 'center',
    marginBottom: 10,
    color: '#757575',
    fontFamily: 'BalooPaaji2-Regular',
  },
});

export default SignInPage;
