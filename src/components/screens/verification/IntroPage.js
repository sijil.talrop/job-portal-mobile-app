import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  Animated,
  ImageBackground,
} from 'react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import {CommonActions, useNavigation} from '@react-navigation/native';

var {height, width} = Dimensions.get('window');

const IntroPage = (props) => {
  const navigation = useNavigation();

  const bottomValue = useRef(new Animated.Value(100)).current;
  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 9000,
    }).start();
  };
  useEffect(() => {
    jumpUp();
  });
  return (
    <>
      <ImageBackground
        source={require('../../../assets/vector-images/Group20260.png')}
        style={styles.container}>
        <View style={styles.contentBox}>
          <View style={styles.imageContainer}>
            <Image
              source={require('../../../assets/vector-images/Group20400-1.png')}
              style={styles.image}
            />
          </View>
          <Animated.View
            style={[
              styles.box,
              {
                transform: [{translateY: bottomValue}],
              },
            ]}>
            <View style={styles.middle}>
              <Text style={styles.errorText}>
                Find a perfect{'\n'} job match
              </Text>
              <Text style={styles.content}>
                Finding our dream job is now much{'\n'} easier and faster like
                never before
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('SignInPage');
              }}
              activeOpacity={0.8}
              style={styles.button}>
              <Text style={styles.buttonText}>Let's Get Started</Text>
            </TouchableOpacity>
          </Animated.View>
        </View>
      </ImageBackground>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: width,
    minHeight: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  contentBox: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingBottom: 50,
  },
  imageContainer: {
    width: width * 0.9,
    height: width * 0.9,
  },
  image: {
    height: null,
    width: null,
    flex: 1,
    resizeMode: 'contain',
  },
  middle: {
    paddingHorizontal: 40,
  },
  errorText: {
    textAlign: 'left',
    fontSize: RFValue(12, width),
    color: '#757575',
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    lineHeight: 34,
  },
  content: {
    fontSize: RFPercentage(1.9),
    fontFamily: 'BalooPaaji2-Regular',
  },
  middle: {
    width: width,
    padding: 40,
  },
  box: {
    alignItems: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  button: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 12,
    width: width * 0.9,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: RFPercentage(1.6),
    color: '#fff',
  },
});

export default IntroPage;
