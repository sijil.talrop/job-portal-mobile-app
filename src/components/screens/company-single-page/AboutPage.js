import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, Dimensions} from 'react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import ReviewPage from './RewiesPage';
import ReviewModal from '../modal/ReviewModal';
import Modal from 'react-native-modal';
const {height, width} = Dimensions.get('window');

export default function AboutPage(props) {
  const [isModalVisible, setModalVisible] = useState(false);

  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  return (
    <>
      <View>
        {props.about != '' ||
          (props.about == null && (
            <View style={{marginBottom: 15, paddingTop: 20}}>
              <Text style={styles.header}>About </Text>
              <Text style={styles.description}>{props.about}</Text>
            </View>
          ))}
        {/* <View>
            <Text style={styles.header}>Basic Steps</Text>
            {basic.map((item) => (
              <View style={styles.basicContent}>
                <Text style={styles.dot}> ⚫</Text>
                <Text style={styles.description}>{item.description}</Text>
              </View>
            ))}
          </View> */}

        <View>
          <ReviewPage
            pk={props.pk}
            review={props.review}
            toggleModal={toggleModal}
            total_review={props.total_review}
          />
        </View>
        <View>
          <Modal isVisible={isModalVisible} onBackButtonPress={toggleModal}>
            <ReviewModal
              pk={props.pk}
              logo={props.uri}
              toggleModal={toggleModal}
            />
          </Modal>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  scrollView: {},
  basicContent: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginTop: 10,
  },
  dot: {
    color: '#000',
    fontSize: 8,
    fontFamily: 'BalooPaaji2-Bold',
    marginRight: 10,
  },
  header: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFValue(10, width), // second argument is standardScreenHeight(optional),
  },
  description: {
    color: '#8e8e8e',
    fontSize: RFPercentage(1.7),
    fontFamily: 'BalooPaaji2-Regular',
  },
});
