import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  Platform,
  Dimensions,
} from 'react-native';
import * as base from '../../../../Settings';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import {TabRouter, useNavigation} from '@react-navigation/native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SearchBar from 'react-native-dynamic-search-bar';
import SearchResultPopup from '../popup-pages/SearchResultPopup';

const {width, height} = Dimensions.get('window');
const box_width = width - 40;
const Reward_Box_Height = width * 0.32;
const card_width = width * 0.9;

function SearchPage({route}) {
  const [selectedRating, setSelectedRating] = useState('Facebook');
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  const navigation = useNavigation();
  const [isOpen, setIsOpen] = useState(false);
  const [search, setSearch] = useState();
  const [lagsearch, setLagsearch] = useState('');

  const [products, setProducts] = useState([]);
  const ref_input2 = useRef();
  useEffect(() => {
    searchQuery();
  }, [search]);

  useEffect(() => {
    setSearch(route.params.search_query);
  }, []);

  let cancelToken;
  const searchQuery = async () => {
    console.log(search);
    console.log(cancelToken);
    //Check if there are any previous pending requests
    if (typeof cancelToken != typeof undefined) {
      cancelToken.cancel('Operation canceled due to new request.');
    }

    //Save the cancel token for the current request
    cancelToken = axios.CancelToken.source();

    if (search === '') {
      let get_url = base.BASE_URL + 'jobs/jobs/';
      try {
        const results = await axios.get(
          `${get_url}?job_title=`,
          {cancelToken: cancelToken.token}, //Pass the cancel token to the current request
        );
        console.log('Results for ' + search + ': ', results.data.data.data);
        if (results.data.StatusCode == 6000) {
          setProducts(results.data.data.data);
        } else {
          setProducts([]);
        }
      } catch (error) {
        console.log(error);
        setProducts([]);
      }
    } else {
      let get_url = base.BASE_URL + 'jobs/jobs/';
      try {
        const results = await axios.get(
          `${get_url}?job_title=${search}`,
          {cancelToken: cancelToken.token}, //Pass the cancel token to the current request
        );
        console.log('Results for ' + search + ': ', results.data.data.data);
        if (results.data.StatusCode == 6000) {
          setProducts(results.data.data.data);
        } else {
          setProducts([]);
        }
      } catch (error) {
        console.log(error.response);
        setProducts([]);
      }
    }

    // } else {
    //   setProducts([]);
    // }
  };

  const render_option = () => {
    return products.map((item, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          TouchableHandler(item.business);
          navigation.navigate('CompanyJobSinglePage', {
            // name: item.name,
            // job: item.job,
            // salary_minimum: item.salary_minimum,
            // salary_maximum: item.salary_maximum,
            // location: item.location,
            item: item,
          });
        }}
        activeOpacity={0.9}
        style={[
          styles.mainContainer,
          {
            backgroundColor:
              item.business == selectedRating ? '#2261a6' : '#fff',
          },
        ]}>
        <View style={styles.left}>
          <View
            style={[
              styles.lefContent,
              {
                backgroundColor:
                  item.business == selectedRating ? '#fff' : '#e2e2e2',
              },
            ]}>
            <View style={styles.imageContainer}>
              <Image source={{uri: item.logo}} style={styles.productImage} />
            </View>
          </View>
        </View>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.introName,
              {color: item.business == selectedRating ? '#fff' : '#adabbb'},
            ]}>
            {item.business.length > 17
              ? item.business.substring(0, 17) + '...'
              : item.business}{' '}
          </Text>
          <Text
            style={[
              styles.name,
              {color: item.business == selectedRating ? '#fff' : '#130937'},
            ]}>
            {item.name.length > 17
              ? item.name.substring(0, 20) + '....'
              : item.name}{' '}
          </Text>
          <View style={styles.Location}>
            <Text
              style={[
                styles.amount,
                {
                  color: item.business == selectedRating ? '#fff' : '#130937',
                },
              ]}>
              ₹{item.salary_minimum}-{item.salary_maximum}/m
            </Text>
            <View style={styles.locationCharge}>
              <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
              <Text
                style={[
                  styles.place,
                  {
                    color: item.business == selectedRating ? '#fff' : '#130937',
                  },
                ]}>
                {item.location.length > 17
                  ? item.location.substring(0, 20) + '...'
                  : item.location}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.right}>
          <View style={styles.rateContainer}>
            <Icon
              name={'bookmark'}
              color={item.kind == 'Featured' ? '#ef801f' : '#eee'}
              size={45}
            />
          </View>

          <View style={styles.delivery}>
            <Text
              style={[
                styles.bottomButtonText,
                {
                  color: item.business == selectedRating ? '#fff' : '#aaa',
                },
              ]}>
              {item.date_added}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <View
      style={{
        backgroundColor: '#fff',
        minHeight: '100%',
        paddingTop: height * 0.06,
      }}>
      <View style={{width: box_width}}>
        <View
          style={{width: width, alignItems: 'center', flexDirection: 'row'}}>
          <View style={styles.searchContainer}>
            <SearchBar
              autoFocus={true}
              onSearchPress={() => {
                searchQuery(search);
              }}
              clearIconComponent={!search ? true : false}
              openTheKeyboard={true}
              clearIcon={true}
              onChangeText={(text) => {
                setSearch(text);
              }}
              textInputStyle={{
                backgroundColor: '#f3f5f8',
                borderRadius: 10,
                width: width * 0.5,
                //   borderWidth: 1,
              }}
              onClearPress={() => setSearch('')}
              placeholder="Search..."
              searchIcon={true}
              value={search}
              returnKeyType="search"
              onSubmitEditing={() => {
                searchQuery(search);
              }}
              style={{
                backgroundColor: '#f3f5f8',
                height: 55,
                width: width * 0.9,
                borderRadius: 10,
                borderWidth: 1,
                borderColor: '#eee',
              }}
            />
          </View>
        </View>
        {!isNaN(products) && search != '' && (
          <View>
            {/* <Text
              style={{
                fontSize: 16,
                color: '#5F5F5F',
                marginBottom: 5,
                fontFamily: 'ProductSans-Regular',
                textAlign: 'center',
                marginTop: 60,
              }}>
              Nothing found
            </Text> */}
            <SearchResultPopup />
          </View>
        )}
        <View
          style={{
            paddingTop: 20,
            width: width,
          }}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            style={{paddingHorizontal: 20, height: '100%'}}>
            {render_option()}
            <View style={{height: height * 0.7}} />
          </ScrollView>
        </View>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  title: {
    color: '#000',
    fontFamily:
      Platform.OS === 'ios' ? 'ProductSans-Regular' : 'ProductSans-Bold',
    marginBottom: 5,
  },
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#aaa',
    elevation: 0.08,
  },
  headText: {
    fontSize: 14,
    marginBottom: 20,
  },
  left: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: box_width * 0.17,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.53,
  },
  lefContent: {
    height: Reward_Box_Height * 0.4,
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 10,
    borderRadius: 30,
  },
  Location: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  amount: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    textTransform: 'capitalize',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.24,
    height: Reward_Box_Height,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.7,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
    // elevation: 0.5,
  },
});
export default SearchPage;
