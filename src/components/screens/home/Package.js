import React, {useState, useEffect} from 'react';
import {
  ScrollView,
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  StyleSheet,
  Text,
  Dimensions,
  FlatList,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import axios from 'axios';
import * as base from '../../../../Settings';

const {width, height} = Dimensions.get('window');
const box_width = width - 40;
const box_height = box_width * 0.6;

const SPACING = 10;
const ITEM_SIZE = Platform.OS === 'ios' ? width * 0.72 : width * 0.74;
const EMPTY_ITEM_SIZE = (width - ITEM_SIZE) / 2;
const BACKDROP_HEIGHT = height * 0.65;

export default function Package() {
  const navigation = useNavigation();
  const [isLoading, setLoading] = useState(true);
  const [pakage, setPakage] = useState([]);

  useEffect(() => {
    _get_categories();
  }, []);

  let _get_categories = () => {
    let get_url = base.BASE_URL + 'payments/plans/';
    axios
      .get(get_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setPakage(data.data);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        } else {
          setPakage([]);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const packages = [
    {
      detail: '100 Candidates Cv',
    },
    {
      detail: 'Duis Vestibulum elit vel neque pharetra',
    },
    {
      detail: 'Excepteur sint occaecat cupidatat non',
    },
    {
      detail: '1sunt in culpa qui officia deserunt mollit anim ',
    },
  ];

  const renderItem = ({item, index}) => {
    return (
      <TouchableOpacity activeOpacity={0.9} style={styles.mainContent}>
        <ImageBackground
          source={require('../../../assets/vector-images/Group20260.png')}
          style={styles.backgroundImageBox}
        />
        <View style={styles.bannerContainer}>
          <View style={styles.topContent}>
            <View style={styles.imgContainer}>
              <Image
                style={styles.image}
                source={require('../../../assets/vector-images/twcoe7t5.png')}
              />
            </View>
            <View style={styles.detailBox}>
              <Text style={styles.rightContentText}>{item.plan_name}</Text>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={styles.rightContentText}>₹{item.amount}</Text>
                <Text style={styles.lightContent}>/{item.validity}-day</Text>
              </View>
            </View>
          </View>
          {packages.map((item, index) => (
            <View key={index} style={styles.locationBox}>
              <Icon name={'check'} color="#2261a6" size={18} />
              <Text style={[styles.locationText]}>{item.detail}</Text>
            </View>
          ))}
          <TouchableOpacity activeOpacity={0.8} style={styles.button}>
            <Text style={styles.buttonText}> Upgrade</Text>
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{height: box_height + box_height * 0.4}}>
      <View style={styles.HeaderContainer}>
        <Text style={styles.header}>Package</Text>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            navigation.navigate('PakagesListPage');
          }}
          style={styles.buttonBox1}>
          <Text style={styles.buttonText1}>Show all</Text>
        </TouchableOpacity>
      </View>
      <View style={{alignItems: 'center'}}>
        <FlatList
          horizontal
          showsHorizontalScrollIndicator={false}
          data={pakage}
          renderItem={renderItem}
          keyExtractor={(item) => item.pk}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  HeaderContainer: {
    paddingHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    width: width,
    marginBottom: 15,
    // marginBottom: 10,
  },
  buttonBox: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: width * 0.2,
    paddingVertical: 5,
    borderWidth: 0.5,
    borderColor: '#eee',
  },
  header: {
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  topContent: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  imgContainer: {
    width: box_width * 0.16,
    height: box_width * 0.16,
    borderRadius: 10,
  },
  backgroundImageBox: {
    resizeMode: 'contain',
    justifyContent: 'flex-end',
    width: width * 0.5,
    height: width * 0.3,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  image: {
    width: null,
    height: null,
    borderRadius: 10,
    overflow: 'hidden',
    flex: 1,
  },
  mainContent: {
    width: width - 40,
    height: width * 0.65,
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#aaa',
    marginHorizontal: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  bannerContainer: {
    overflow: 'hidden',
  },
  locationText: {
    fontSize: RFPercentage(1.4),
    fontFamily: 'BalooPaaji2-Regular',
    marginLeft: 10,
    color: '#9491a7',
  },
  locationBox: {
    flexDirection: 'row',
  },
  rightContentText: {
    fontSize: RFValue(8.5, width), // second argument is standardScreenHeight(optional),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  lightContent: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  detailBox: {
    // justifyContent: 'center',
    marginLeft: 10,
  },
  button: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    paddingVertical: 5,
    alignItems: 'center',
    marginTop: 15,
  },
  buttonBox1: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.2,
    paddingVertical: 5,
  },
  buttonText: {
    fontSize: RFPercentage(1.5),
    fontFamily: 'BalooPaaji2-Regular',
    color: '#fff',
  },
  buttonText1: {
    fontSize: RFPercentage(1.6),
    fontFamily: 'BalooPaaji2-Regular',
    color: '#9390a7',
  },
  scrollStyle: {
    flexDirection: 'row',
    width: width,
  },
});
