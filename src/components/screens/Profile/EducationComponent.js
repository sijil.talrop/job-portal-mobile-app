import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import {
  CommonActions,
  useNavigation,
  useIsFocused,
} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Progress from '../Profile/Progress';
import axios from 'axios';
import * as base from '../../../../Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Context} from '../../contexts/Store';
import {Item} from 'native-base';

const {width, height} = Dimensions.get('window');

export default function EducationComponent() {
  const isfocused = useIsFocused();
  const navigation = useNavigation();
  const [profile, setProfile] = useState([]);
  const [education, setEducation] = useState();
  const [experience, setExperience] = useState();
  const [skill, setSkill] = useState();
  const [awarda, setAwarda] = useState();
  const [follower, setFollower] = useState([]);
  const [loading, setLoading] = useState(true);
  const {state, dispatch} = useContext(Context);

  useEffect(() => {
    _get_profile();
  }, []);

  let _get_profile = async () => {
    let get_url = base.BASE_URL + 'users/profile/';
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data, message} = response.data;
        if (StatusCode == 6000) {
          setProfile(data.data);
          setEducation(data.education_details);
          setExperience(data.experience_details);
          setSkill(data.skill_details);
          setAwarda(data.award_details);
          setFollower(data.followed_data);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        } else {
          setProfile([]);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  const workExperience = () => {
    return (
      <View style={{}}>
        <Text style={styles.Header}>Work & Experience</Text>
        {experience &&
          experience.length > 0 &&
          experience.map((item, index) => (
            <View key={index} style={[styles.mainFlexBox]}>
              <View style={{marginRight: 20}}>
                <Text style={{color: '#3681ba', fontSize: 26}}>○</Text>
              </View>
              <View style={{}}>
                <View style={styles.rightContent}>
                  <View style={{flexDirection: 'row'}}>
                    {item.start_date != null && (
                      <Text style={styles.yearText}>
                        {item.start_date.split('-')[0]}-
                      </Text>
                    )}
                    {item.end_date != null && (
                      <Text style={styles.yearText}>
                        {item.end_date.split('-')[0]}
                      </Text>
                    )}
                  </View>

                  <Text style={styles.optionText}>{item.job_title}</Text>
                  <Text style={styles.company}>{item.company}</Text>
                  <View style={{flexDirection: 'row', alignItems: 'center'}}>
                    <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
                    <Text style={styles.place}>{item.place}</Text>
                  </View>

                  <Text style={styles.description}>{item.description}</Text>
                </View>
              </View>
            </View>
          ))}
      </View>
    );
  };

  const Awards = () => {
    return (
      <View style={{}}>
        <Text style={styles.Header}>Awards</Text>
        {awarda &&
          awarda.length > 0 &&
          awarda.map((item, index) => (
            <View
              key={item.pk}
              style={[styles.mainFlexBox, {paddingHorizontal: 10}]}>
              <View style={{marginRight: 20}}>
                <Text style={{color: '#3681ba', fontSize: 26}}>○</Text>
              </View>
              <View style={{}}>
                <View style={styles.rightContent}>
                  <Text style={styles.optionText}>{item.title}</Text>
                  <View style={{flexDirection: 'row'}}>
                    {item.start_date != null && (
                      <Text style={styles.yearText}>
                        {item.start_date.split('-')[0]}-
                      </Text>
                    )}
                    {item.end_date != null && (
                      <Text style={styles.yearText}>
                        {item.end_date.split('-')[0]}
                      </Text>
                    )}
                  </View>

                  <Text style={styles.description}>T{item.description}</Text>
                </View>
              </View>
            </View>
          ))}
      </View>
    );
  };
  const Companies = () => {
    return (
      <ScrollView
        pagingEnabled
        horizontal={true}
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={styles.scrollStyle}
        onScroll={''}>
        {follower &&
          follower.length > 0 &&
          follower.map((item, index) => (
            <TouchableOpacity
              key={index}
              style={{width: width * 0.32, marginHorizontal: width * 0.02}}>
              <View style={styles.companyImage}>
                <Image style={styles.image1} source={{uri: item.logo}} />
              </View>
              <View
                style={{
                  width: width * 0.32,
                  height: width * 0.13,
                }}>
                <Text style={styles.companyText}>
                  {item.business.length > 17
                    ? item.business.substring(0, 17) + '...'
                    : item.business}
                </Text>
              </View>
            </TouchableOpacity>
          ))}
        <View style={{width: width * 0.3}} />
      </ScrollView>
    );
  };

  const eduction_Render = () => {
    return (
      <View style={{}}>
        <Text style={styles.Header}>Education</Text>
        {education &&
          education.length > 0 &&
          education.map((item, index) => (
            <View key={index} style={styles.mainFlexBox}>
              <View style={styles.imageBox}>
                <Image
                  style={styles.image}
                  source={require('../../../assets/vector-images/cap.png')}
                />
              </View>
              <View style={styles.rightContent}>
                <View style={{flexDirection: 'row'}}>
                  {item.start_date != null && (
                    <Text style={styles.yearText}>
                      {item.start_date.split('-')[0]}-
                    </Text>
                  )}
                  {item.end_date != null && (
                    <Text style={styles.yearText}>
                      {item.end_date.split('-')[0]}
                    </Text>
                  )}
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    alignItems: 'center',
                    flexWrap: 'wrap',
                  }}>
                  <Text style={styles.optionText}>{item.school}-</Text>
                  <Text style={styles.yearText}>
                    {item.degree.length > 10
                      ? item.degree.substring(0, 80)
                      : item.degree}
                  </Text>
                </View>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
                  <Text style={styles.place}>{item.place}</Text>
                </View>
                <Text style={styles.description}>{item.description}</Text>
              </View>
            </View>
          ))}
      </View>
    );
  };

  return (
    !loading && (
      <>
        <View style={{paddingLeft: 20}}>
          {isNaN(education) != '' && <View>{eduction_Render()}</View>}
          {isNaN(experience) != '' && <View>{workExperience()}</View>}

          {skill != '' && (
            <View style={{marginVertical: 20}}>
              <Text style={styles.Header}> Personal Skill</Text>
              {skill &&
                skill.length > 0 &&
                skill.map((item, index) => (
                  <Progress
                    key={item.pk}
                    title={item.title}
                    percentage={item.level}
                  />
                ))}
            </View>
          )}
          {awarda != '' && <View>{Awards()}</View>}

          {follower != '' && (
            <View>
              <Text style={styles.Header} s>
                Companies Followed By
              </Text>
              {Companies()}
            </View>
          )}
          <View style={{height: height * 0.04}}></View>
        </View>
      </>
    )
  );
}

const styles = StyleSheet.create({
  imageBox: {
    width: 40,
    height: 40,
    marginRight: 20,
  },
  scrollStyle: {
    flexDirection: 'row',
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    resizeMode: 'contain',
  },
  image1: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 15,
  },
  mainFlexBox: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    marginBottom: 20,
  },
  rightContent: {
    width: width * 0.75,
  },
  yearText: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
    color: '#9491a7',
  },
  place: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
    color: '#9491a7',
    marginLeft: 10,
  },
  optionText: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#3681ba',
  },
  company: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    color: '#3681ba',
  },
  description: {
    fontFamily: 'BalooPaaji2-Regular',
    color: '#42395f',
    fontSize: 14,
  },
  Header: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    marginBottom: 10,
    color: '#130937',
  },
  imageContent: {
    width: width * 0.4,
    height: width * 0.3,
    backgroundColor: '#fff',
  },
  companyImage: {
    width: width * 0.32,
    height: width * 0.25,
    borderWidth: 0.6,
    borderRadius: 15,
    elevation: 0.6,
    borderColor: '#aaa',
  },
  companyText: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
    marginTop: 5,
  },
});
