import React, {useState, useEffect, useRef} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BasicHeading from '../header/BasicHeading';
import CommonSearchBar from '../home/CommonSearchBar';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.32;
const card_width = width * 0.6;
const box_width = width - 40;

export default function MostRecentSearch() {
  const [like, setLike] = useState(true);
  const [product, setproducts] = useState([]);
  const [selectedRating, setSelectedRating] = useState('Facebook');
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  const earnings = [
    {
      id: 1,
      image: 'https://pbs.twimg.com/media/Eg11rNaU0AIxCnA.jpg',
      product_name: 'Sigma',
      Development: 'Game for Loop',
      price: '30',
      icon: 'false',
    },
    {
      id: 2,
      image:
        'https://cdn3.vectorstock.com/i/1000x1000/15/62/crown-king-sport-esport-gaming-logo-download-vector-24171562.jpg',
      product_name: 'Prince & Jobs',
      Development: 'Full Stack Developer',
      price: '45',
      icon: 'true',
    },
    {
      id: 3,
      image:
        'https://upload.wikimedia.org/wikipedia/commons/f/fd/Ubisoft2017.png',
      product_name: '7 Days',
      Development: 'Receptionist',
      price: '20',
      icon: 'true',
    },
    {
      id: 4,
      image:
        'https://st4.depositphotos.com/27205200/30701/v/600/depositphotos_307017002-stock-illustration-viking-esports-logo-design-vector.jpg',
      product_name: 'Viking',
      price: '30',
      Development: 'Workshop Assistants',
      icon: 'true',
    },
    {
      id: 5,
      image:
        'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTwJ5ZNPsBK6p_Wi3uTrBI4kuLy3GAqL4Y_vQ&usqp=CAU',
      product_name: 'Common True',
      price: '30',
      Development: 'Game Designer',
      icon: 'true',
    },
    {
      id: 6,
      image:
        'https://www.dlf.pt/dfpng/middlepng/247-2477181_spirit-of-gamer-logo-icon-spirit-of-gamer.png',
      product_name: 'Spirit of Games',
      price: '30',
      Development: 'Game Designer',
      icon: 'false',
    },
  ];

  const Needs = () => {
    return earnings.map((item, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          TouchableHandler(item.product_name);
        }}
        activeOpacity={0.9}
        style={[
          styles.mainContainer,
          {
            backgroundColor:
              item.product_name == selectedRating ? '#2261a6' : '#fff',
          },
        ]}>
        <View style={styles.left}>
          <View
            style={[
              styles.lefContent,
              {
                backgroundColor:
                  item.product_name == selectedRating ? '#fff' : '#e2e2e2',
              },
            ]}>
            <View style={styles.imageContainer}>
              <Image source={{uri: item.image}} style={styles.productImage} />
            </View>
          </View>
        </View>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.introName,
              {color: item.product_name == selectedRating ? '#fff' : '#adabbb'},
            ]}>
            {item.product_name}
          </Text>
          <Text
            style={[
              styles.name,
              {color: item.product_name == selectedRating ? '#fff' : '#130937'},
            ]}>
            {item.Development}
          </Text>
          <View style={styles.Location}>
            <Text
              style={[
                styles.amount,
                {
                  color:
                    item.product_name == selectedRating ? '#fff' : '#130937',
                },
              ]}>
              ₹20-25/m
            </Text>
            <View style={styles.locationCharge}>
              <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
              <Text
                style={[
                  styles.place,
                  {
                    color:
                      item.product_name == selectedRating ? '#fff' : '#130937',
                  },
                ]}>
                16 Ave New York
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.right}>
          {item.icon == 'true' ? (
            <View style={styles.rateContainer}>
              <Icon name={'bookmark'} color={'#eee'} size={55} />
            </View>
          ) : (
            <View style={styles.rateContainer}>
              <Icon name={'bookmark'} color={'#ef801f'} size={55} />
            </View>
          )}
          <View style={styles.delivery}>
            <Text
              style={[
                styles.bottomButtonText,
                {
                  color: item.product_name == selectedRating ? '#fff' : '#aaa',
                },
              ]}>
              5Hrs
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <View style={{}}>
      {Needs()}
      <View style={{height: height * 0.5}} />
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 0.1,
  },
  headText: {
    fontSize: 14,
    marginBottom: 20,
  },
  left: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: box_width * 0.21,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.55,
  },
  lefContent: {
    height: Reward_Box_Height * 0.4,
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 10,
    borderRadius: 30,
  },
  Location: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
  name: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 17,
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  amount: {
    fontFamily: 'BalooPaaji2-Bold',
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#393057',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.16,
    height: Reward_Box_Height,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  cartLeft: {
    width: card_width,
    height: Reward_Box_Height,
    borderRadius: 10,
    overflow: 'hidden',
    backgroundColor: '#fff',
    elevation: 4,
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
    // elevation: 0.5,
  },
});
