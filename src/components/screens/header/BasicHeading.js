import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
const {height, width} = Dimensions.get('window');
import {CommonActions, useNavigation} from '@react-navigation/native';
import {Left, Right, Body} from 'native-base';

export default function BasicHeadings(props) {
  const navigation = useNavigation();

  return (
    <View style={styles.iconBox}>
      <Left style={{flex: 1}}>
        <TouchableOpacity
          onPress={() => {
            navigation.goBack();
          }}
          activeOpacity={8}
          style={styles.menu}>
          <Icon name="chevron-left" size={29} color={'#000'} />
        </TouchableOpacity>
      </Left>
      <Body style={{flex: 4, alignItems: 'center'}}>
        <View
          style={{
            alignItems: 'center',
            justifyContent: 'center',
          }}>
          <Text style={styles.headText}>{props.title}</Text>
        </View>
      </Body>
      <Right style={{flex: 1}}></Right>
    </View>
  );
}

const styles = StyleSheet.create({
  iconBox: {
    flexDirection: 'row',
    alignItems: 'center',
    alignContent: 'center',
  },
  headText: {
    fontSize: 20,
    color: '#000',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  menu: {
    width: 25,
    height: 25,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
});
