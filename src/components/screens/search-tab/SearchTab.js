import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';
import BasicHeading from '../header/BasicHeading';
import CommonSearchBar from '../home/CommonSearchBar';
import MostRelevantSearch from './MostRelevantSearch';
import MostRecentSearch from './MostRecentSearch';
const {height, width} = Dimensions.get('window');

export default function SearchTab(params) {
  const Tab = [
    {
      Box: 'Most Relevant',
    },
    {
      Box: 'Most Recent',
    },
  ];

  const [selectedBox, setSelectedBox] = useState(Tab[0].Box);
  const confirmHandler = (value) => {
    setSelectedBox(value);
  };

  const renderTabList = () => {
    if (selectedBox == 'Most Relevant') {
      return <MostRelevantSearch />;
    } else if (selectedBox == 'Most Recent') {
      return <MostRecentSearch />;
    }
  };

  const renderTab = () => {
    return (
      <View style={{flex: 1, minHeight: '100%'}}>
        <Text style={styles.filterHeader}>32 Search result</Text>
        {/* <View> */}
        <View style={styles.tabView}>
          {Tab.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                confirmHandler(item.Box);
              }}
              style={[
                styles.tabBox,
                {
                  backgroundColor:
                    item.Box === selectedBox ? '#2261a6' : '#f3f5f8',
                },
              ]}>
              <Text
                style={[
                  styles.buttonText,
                  {color: item.Box == selectedBox ? '#fff' : '#000'},
                ]}>
                {item.Box}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.contentScroll}>
          {renderTabList()}
        </ScrollView>
      </View>
    );
  };

  return (
    <ImageBackground
      style={styles.backgroundImageBox}
      source={require('../../../assets/vector-images/Group20260.png')}>
      {/* <View style={{}}> */}
      <View style={{flex: 1}}>
        <View style={{marginVertical: 20, paddingHorizontal: 15}}>
          <BasicHeading title="Search" />
        </View>
        <View style={{marginVertical: 20}}>
          <CommonSearchBar />
        </View>
        <View style={{alignItems: 'center'}}>{renderTab()}</View>
      </View>
      {/* </View> */}
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  backgroundImageBox: {
    resizeMode: 'cover',
    width: width,
    minHeight: '100%',
  },
  contentScroll: {},
  filterHeader: {
    fontSize: 18,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  header: {
    color: '#000',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    paddingVertical: 15,
    paddingHorizontal: 10,
    borderRadius: 10,
    elevation: 1,
    marginVertical: 20,
  },
  tabBox: {
    width: width * 0.4,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 15,
  },
  iconBox: {
    paddingTop: 20,
    width: width - 30,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
});
