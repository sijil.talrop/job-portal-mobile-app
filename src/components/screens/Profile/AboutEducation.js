import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import DateTimePicker from '@react-native-community/datetimepicker';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import axios from 'axios';
const {width, height} = Dimensions.get('window');
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';

export default function AboutEduction() {
  const {state, dispatch} = useContext(Context);
  const [school, setSchool] = useState();
  const [degree, setDegree] = useState();
  const [startDate, setStartDate] = useState();
  const [endDate, setEndDate] = useState();
  const [place, setPlace] = useState();
  const [push, setPush] = useState([]);
  const [description, setDescription] = useState();
  const [show, setShow] = useState(false);
  const [add, setAdd] = useState(false);
  const [drag, setDrag] = useState(false);
  const [mode, setMode] = useState('date');
  const [date, setDate] = useState(new Date());
  const [isFocused, setIsFocused] = useState({
    boxOne: false,
    boxTwo: false,
    boxThree: false,
    boxFour: false,
    boxFive: true,
    boxSix: true,
  });

  const ref_input2 = useRef();
  const ref_input3 = useRef();

  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };

  const registration = async () => {
    let access_token = state.user_data.access_token;
    let post_url = base.BASE_URL + 'users/update-education/';
    axios
      .post(post_url, push, {
        headers: {
          Authorization: 'Bearer ' + access_token,
          'Content-Type': 'application/json; ',
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          Toast.show(data.message, Toast.SHORT);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    setStartDate(moment(currentDate).format('YYYY-MM-DD'));
    setShow(false);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setDrag(false);
    setMode(currentMode);
  };

  const onEndDate = (event, selectedValue) => {
    const liveDate = selectedValue;
    setEndDate(moment(liveDate).format('YYYY-MM-DD'));
    setDrag(false);
  };
  const pickeMode = (currentMode) => {
    setShow(false);
    setDrag(true);
    setMode(currentMode);
  };

  const renderYearOptions = () => {
    if (school && degree && startDate && endDate && place && description) {
      setAdd(true);
      push.push({
        school: school,
        degree: degree,
        start_date: startDate,
        end_date: endDate,
        place: place,
        description: description,
      });
      setSchool();
      setDegree();
      setStartDate();
      setEndDate();
      setPlace();
      setDescription();
      Toast.show(' Added Successfully  ', Toast.SHORT);
    } else {
      Toast.show('Please enter all details', Toast.SHORT);
    }
  };
  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>2</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Tell us</Text> about your Education
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>School</Text>
            <TextInput
              value={school}
              style={[
                styles.input,
                {borderColor: isFocused.boxOne ? '#c3c4c7' : '#ddd'},
              ]}
              placeholder="School"
              placeholderTextColor={isFocused.boxOne ? '#000' : '#aaa'}
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setSchool(val)}
              onSubmitEditing={() => ref_input2.current.focus()}
              onFocus={() => handleInputFocus('boxOne')}
              onBlur={() => handleInputBlur('boxOne')}
            />
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Degree</Text>
            <TextInput
              value={degree}
              style={[
                styles.input,
                {borderColor: isFocused.boxTwo ? '#c3c4c7' : '#ddd'},
              ]}
              placeholderTextColor={isFocused.boxTwo ? '#000' : '#aaa'}
              placeholder="Degree"
              ref={ref_input2}
              autoFocus={false}
              returnKeyType={'done'}
              onChangeText={(val) => setDegree(val)}
              onFocus={() => handleInputFocus('boxTwo')}
              onBlur={() => handleInputBlur('boxTwo')}
            />
          </View>

          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Start Date </Text>
            <View style={styles.pickerBox}>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onChange}
                />
              )}
              {startDate ? (
                <Text style={styles.dateStyle}>{startDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYYY-DD-MM</Text>
              )}
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  showMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>End Date </Text>
            <View style={styles.pickerBox}>
              {drag && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onEndDate}
                />
              )}
              {endDate ? (
                <Text style={styles.dateStyle}>{endDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYYY-DD-MM</Text>
              )}
              <TouchableOpacity
                activeOpacity={0.8}
                onPress={() => {
                  pickeMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>City</Text>
            <TextInput
              value={place}
              style={[
                styles.input,
                {borderColor: isFocused.boxThree ? '#c3c4c7' : '#ddd'},
              ]}
              placeholder="Enter Your City"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setPlace(val)}
              placeholderTextColor={isFocused.boxThree ? '#000' : '#aaa'}
              onSubmitEditing={() => ref_input3.current.focus()}
              onFocus={() => handleInputFocus('boxThree')}
              onBlur={() => handleInputBlur('boxThree')}
            />
          </View>
          <View style={[styles.loginUser]}>
            <Text style={styles.subHeader}>Description</Text>
            <View
              style={[
                {
                  height: height * 0.11,
                  backgroundColor: '#f5f6f9',
                  borderRadius: 20,
                  borderWidth: 1,
                },
                {borderColor: isFocused.boxFour ? '#c3c4c7' : '#ddd'},
              ]}>
              <TextInput
                value={description}
                style={styles.inputLast}
                placeholder="Explain of Education"
                placeholderTextColor={isFocused.boxFour ? '#000' : '#aaa'}
                ref={ref_input3}
                returnKeyType={'done'}
                onChangeText={(val) => setDescription(val)}
                multiline={true}
                onFocus={() => handleInputFocus('boxFour')}
                onBlur={() => handleInputBlur('boxFour')}
              />
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            renderYearOptions();
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Education</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            if (add) {
              registration();
            } else {
              Toast.show('Click Add Button', Toast.SHORT);
            }
          }}
          style={styles.button2}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText2}>Save</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 12,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  pickerBox: {
    fontSize: 15,
    width: '100%',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dateStyle: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
  },
  inputLast: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    paddingHorizontal: 20,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 5,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  subHeader: {
    fontSize: RFPercentage(1.6),
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  button2: {
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 15,
    backgroundColor: '#ef803a',
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  uploadText2: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
});
