import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Animated,
} from 'react-native';
import {Context} from '../../contexts/Store';
import * as base from '../../../../Settings';
import BasicHeading from '../header/BasicHeading';
import axios from 'axios';
import {
  CommonActions,
  useNavigation,
  useIsFocused,
} from '@react-navigation/native';

const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.2;
const card_width = width * 0.6;
const box_width = width - 40;

export default function Note() {
  const {state, dispatch} = useContext(Context);
  const [notification, setNotificaton] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const navigation = useNavigation();

  useEffect(() => {
    _get_notifications();
    fadeIn();
  }, []);

  const fadeAnim = useRef(new Animated.Value(0)).current;

  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: true,
    }).start();
  };

  // const notification = [
  //   {
  //     description: 'r visuals are created to replace i',
  //     timestamp: 1200,
  //   },
  //   {
  //     description: 'r visuals are created to replace i',
  //     timestamp: 1200,
  //   },
  //   {
  //     description: 'r visuals are created to replace i',
  //     timestamp: 1200,
  //   },
  // ];

  let _get_notifications = async () => {
    setLoading(true);
    let get_url = base.BASE_URL + 'users/notifications/';
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setNotificaton(data.data);
          console.log(data.data, 'ooooooooooooooooooooooooooooooooooooooooo');
          setTimeout(() => {
            setLoading(false);
          }, 500);
        } else {
          setTimeout(() => {
            setLoading(false);
          }, 500);
        }
      })
      .catch((error) => {
        console.log(error.message, '....................');
      });
  };
  const Needs = () => {
    return notification.map((item, index) => (
      <TouchableOpacity
        key={item.pk}
        //   onPress={() => {
        //     TouchableHandler(item.pk);
        //     navigation.navigate('ApplicationListPage');
        //     console.log(item.pk);
        //   }}
        activeOpacity={0.9}
        style={styles.mainContainer}>
        <View style={styles.contentContairener}>
          <Text style={styles.notification}>{item.description}</Text>
        </View>
        <View style={styles.right}>
          <Text style={styles.bottomButtonText}>{item.timestamp}</Text>
        </View>
      </TouchableOpacity>
    ));
  };
  return (
    <>
      <View style={{paddingVertical: 30, paddingHorizontal: 10}}>
        <BasicHeading title="Notifications" />
        <ScrollView
          contentContainerStyle={{alignItems: 'center', paddingTop: 10}}>
          {notification.length > 0 ? (
            <View>{Needs()}</View>
          ) : (
            <Text style={styles.messageText}>No Notifications</Text>
          )}
          <View style={{height: height * 0.2}} />
        </ScrollView>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 0.1,
  },

  contentContairener: {
    width: box_width * 0.55,
  },

  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  notification: {
    fontFamily: 'BalooPaaji2-Normal',
    fontSize: 14,
    // textTransform: 'capitalize',
    // marginBottom: 5,
    color: '#000',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.4,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  foundImage: {
    width: width * 0.5,
    height: width * 0.5,
    resizeMode: 'contain',
  },
  messageText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'BalooPaaji2-Regular',
    marginTop: 40,
  },
});
