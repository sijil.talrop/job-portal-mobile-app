import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  ToastAndroid,
} from 'react-native';
import {
  CommonActions,
  useNavigation,
  useIsFocused,
} from '@react-navigation/native';
import AboutPage from './AboutPage';
import CompanyBasedJob from './CompanyBasedJob';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';

const {height, width} = Dimensions.get('window');

export default function CompanySinglePage({route}) {
  const isfocused = useIsFocused();
  const navigation = useNavigation();
  const {state, dispatch} = useContext(Context);

  const Tab = [
    {
      Box: 'About',
    },
    {
      Box: 'Jobs',
    },
  ];

  const [selectedBox, setSelectedBox] = useState(Tab[0].Box);
  const [company, setCompany] = useState([]);
  const [jobs, setJobs] = useState([]);
  const [review, setReview] = useState([]);
  const [gallery, setGallery] = useState([]);
  const [team_member, setTeam_member] = useState([]);
  const [totalviews, setTotalviews] = useState([]);
  const [loading, setLoading] = useState(true);
  const [followButton, setFollowButton] = useState('');
  const [follow, setFollow] = useState();
  const confirmHandler = (value) => {
    setSelectedBox(value);
  };

  useEffect(() => {
    if (isfocused) {
      _get_recentCompany();
    }
  }, [isfocused]);

  const renderTabList = () => {
    if (selectedBox == 'About') {
      return (
        <AboutPage
          pk={route.params.pk}
          uri={company.logo}
          about={company.about}
          review={review}
          gallery={gallery}
          team_member={team_member}
          total_review={totalviews}
        />
      );
    } else if (selectedBox == 'Jobs') {
      return <CompanyBasedJob jobs={jobs} />;
    }
  };

  const renderTab = () => {
    return (
      <View
        style={{
          alignItems: 'center',
          borderTopLeftRadius: 100,
          borderTopRightRadius: 100,
          backgroundColor: '#fff',
        }}>
        <View style={styles.tabView}>
          {Tab.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                confirmHandler(item.Box);
              }}
              style={[
                styles.tabBox,
                {
                  backgroundColor:
                    item.Box === selectedBox ? '#ef801f' : '#fff',
                },
              ]}>
              <Text
                style={[
                  styles.buttonText,
                  {color: item.Box == selectedBox ? '#fff' : '#000'},
                ]}>
                {item.Box}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.contentScroll}>
          {renderTabList()}
        </ScrollView>
      </View>
    );
  };

  let _get_recentCompany = async () => {
    let get_url = base.BASE_URL + 'users/employer/' + route.params.pk + '/';
    let user = await AsyncStorage.getItem('user');
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCompany(data.data);
          setJobs(data.jobs);
          setReview(data.review);
          setGallery(data.gallery);
          setTeam_member(data.team);
          setTotalviews(data);
          setFollow(data.is_followed);
          setTimeout(() => {
            setLoading(false);
          }, 200);
        } else {
          setJobs([]);
          setReview([]);
          setRecent_company([]);
          setTimeout(() => {
            setLoading(false);
          }, 200);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  let _get_Followers = async () => {
    let get_url =
      base.BASE_URL + 'users/follow-employer/' + route.params.pk + '/';
    let user = await AsyncStorage.getItem('user');
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data, message} = response.data;
        if (StatusCode == 6000) {
          ToastAndroid.show(data.message, ToastAndroid.SHORT);
        } else {
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  return (
    !loading && (
      <View style={styles.topBox}>
        <ImageBackground
          source={require('../../../assets/vector-images/Group20260.png')}
          style={styles.backgroundImage}
        />
        <ScrollView contentContainerStyle={styles.backgroundImageBox}>
          <View style={styles.iconBox1}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
              activeOpacity={0.8}
              style={{}}>
              <Icon name="chevron-left" size={34} color={'#000'} />
            </TouchableOpacity>
            <View style={[styles.rightBox, {backgroundColor: '#eee'}]}>
              {follow ? (
                <TouchableOpacity
                  onPress={() => {
                    _get_Followers();
                    setTimeout(() => {
                      _get_recentCompany();
                    }, 200);
                  }}
                  activeOpacity={0.8}
                  style={{}}>
                  <Icon name="heart" size={34} color={'red'} />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    _get_Followers();
                    setTimeout(() => {
                      _get_recentCompany();
                    }, 200);
                  }}
                  activeOpacity={0.8}
                  style={{}}>
                  <Icon name="heart-outline" size={34} color={'red'} />
                </TouchableOpacity>
              )}
            </View>
          </View>
          <View style={styles.TopContent}>
            <View style={styles.imageMainBox}>
              <View style={styles.imageBox}>
                <Image style={styles.image} source={{uri: company.logo}} />
              </View>
            </View>
            <Text style={styles.companyName}>{company.business}</Text>
            <View style={styles.detailBox}>
              <Text style={styles.location}>
                {company.first_name}
                {company.last_name}
              </Text>
              <View
                style={{
                  flexDirection: 'row',
                  alignItems: 'center',
                }}>
                <Icon name={'google-maps'} color={'#4eeb12'} size={20} />

                <Text style={styles.location}>
                  {company.location.length > 10
                    ? company.location.substring(0, 22) + '...'
                    : company.location}
                </Text>
              </View>
            </View>
            <View style={styles.ReviewBox}>
              <View style={styles.reviewContent}>
                <Text style={styles.bottomText}>{jobs.length}</Text>
                <Text style={styles.bottomText}>Jobs</Text>
              </View>
              <View style={styles.reviewContent}>
                <Text style={styles.bottomText}>
                  {totalviews.total_rating}/5
                </Text>
                <Text style={styles.bottomText}>Reviews</Text>
              </View>
              <View style={styles.reviewContent}>
                <Text style={styles.bottomText}>
                  {totalviews.total_viewers}
                </Text>
                <Text style={styles.bottomText}>Views</Text>
              </View>
            </View>
          </View>
          {renderTab()}
        </ScrollView>
      </View>
    )
  );
}

const styles = StyleSheet.create({
  backgroundImage: {
    resizeMode: 'contain',
    height: width * 0.5,
    position: 'absolute',
    width: width * 0.5,
    top: 0,
    right: 0,
  },
  iconBox1: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingVertical: '8%',
  },
  menu: {
    width: 50,
    height: 50,
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    borderWidth: 2,
    borderColor: '#eee',
    elevation: 1,
  },
  headText: {
    fontSize: 18,
    color: '#000',
    fontFamily: 'Poppins-SemiBoldItalic',
  },
  backgroundImageBox: {
    backgroundColor: '#e5ecf0',
  },
  topBox: {
    minHeight: '100%',
    width: width,
    backgroundColor: '#fff',
  },
  TopContent: {
    alignItems: 'center',
    marginBottom: height * 0.03,
  },
  imageMainBox: {
    width: width * 0.3,
    height: width * 0.3,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  imageBox: {
    width: width * 0.2,
    height: width * 0.2,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 30,
    overflow: 'hidden',
  },
  companyName: {
    fontSize: RFValue(10, width), // second argument is standardScreenHeight(optional),
    marginTop: 10,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  detailBox: {
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 10,
  },
  location: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.5),
  },
  bottomText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.5),
  },
  ReviewBox: {
    flexDirection: 'row',
  },
  reviewContent: {
    marginHorizontal: 20,
    alignItems: 'center',
  },
  //   tablist style
  topView: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
  },
  header: {
    color: '#000',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    borderRadius: 10,
    top: -10,
    borderWidth: 0.6,
    borderColor: '#aaa',
    elevation: 0.06,
    // marginVertical: 20,
  },
  tabBox: {
    width: width * 0.4,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 15,
  },
  iconBox: {
    paddingTop: 20,
    width: width - 30,
  },
  buttonText: {
    fontFamily: 'Roboto-Bold',
    fontSize: 13,
  },
  contentScroll: {
    width: width - 30,
  },
  followButtonBox1: {
    borderRadius: 15,
    paddingHorizontal: 15,
    width: width * 0.3,
    paddingVertical: 3,
    marginVertical: 5,
    borderColor: '#0080ff',
    borderWidth: 1,
  },
  followButtonBox: {
    backgroundColor: '#0080ff',
    borderRadius: 15,
    paddingHorizontal: 15,
    paddingVertical: 3,
    marginVertical: 5,
    width: width * 0.3,
  },
  followButtonText: {
    fontSize: 12,
    color: '#fff',
    fontFamily: 'BalooPaaji2-SemiBold',
    textAlign: 'center',
  },
  followButtonText1: {
    fontSize: 12,
    color: '#0080ff',
    fontFamily: 'BalooPaaji2-SemiBold',
    textAlign: 'center',
  },
});
