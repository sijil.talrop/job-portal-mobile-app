import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {CommonActions, useNavigation} from '@react-navigation/native';
import axios from 'axios';
import * as base from '../../../../Settings';
import {InstagramLoader} from 'react-native-easy-content-loader';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.28;
const card_width = width * 0.6;
const box_width = width - 40;

export default function RelevantTab() {
  const navigation = useNavigation();
  const [selectedRating, setSelectedRating] = useState('');
  const [relevant, setRelevant] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };
  useEffect(() => {
    if (isNaN(relevant)) {
    }
  }, []);

  let _get_recentCompany = () => {
    let get_url = base.BASE_URL + 'users/employers/?popular=true';
    axios
      .get(get_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setRelevant(data.data);
          setSelectedRating(data.data[0].pk);
          setTimeout(() => {
            setLoading(false);
          }, 200);
        } else {
          setRelevant([]);
          setTimeout(() => {
            setLoading(false);
          }, 200);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  useEffect(() => {
    _get_recentCompany();
  }, []);

  const Needs = ({item, index}) => {
    return (
      <InstagramLoader
        active
        tHeight={5}
        tWidth="95%"
        sTWidth="95%"
        sTHeight={5}
        loading={isLoading}
        primaryColor="#d2d2d2"
        secondaryColor="#d2d2d2"
        imageStyles={{width: box_width - 40, height: 100, borderRadius: 20}}>
        <TouchableOpacity
          key={index}
          onPress={() => {
            TouchableHandler(item.business);
            navigation.navigate('CompanySinglePage', {
              pk: item.pk,
            });
          }}
          activeOpacity={0.9}
          style={[
            styles.mainContainer,
            {
              backgroundColor: item.pk == selectedRating ? '#2261a6' : '#fff',
            },
          ]}>
          <View style={styles.left}>
            <View
              style={[
                styles.lefContent,
                {
                  backgroundColor:
                    item.pk == selectedRating ? '#fff' : '#e2e2e2',
                },
              ]}>
              <View style={styles.imageContainer}>
                <Image source={{uri: item.logo}} style={styles.productImage} />
              </View>
            </View>
          </View>
          <View style={styles.contentContairener}>
            <Text
              style={[
                styles.name,
                {color: item.pk == selectedRating ? '#fff' : '#130937'},
              ]}>
              {item.business.length > 17
                ? item.business.substring(0, 18) + '...'
                : item.business}
            </Text>
            <Text
              style={[
                styles.introName,
                {color: item.pk == selectedRating ? '#fff' : '#adabbb'},
              ]}>
              {item.first_name}
              {item.last_name}
            </Text>
          </View>
          <View style={styles.right}>
            {item.icon == 'true' ? (
              <View style={styles.rateContainer}>
                <Icon name={'bookmark'} color={'#eee'} size={45} />
              </View>
            ) : (
              <View style={styles.rateContainer}>
                <Icon name={'bookmark'} color={'#ef801f'} size={45} />
              </View>
            )}
          </View>
        </TouchableOpacity>
      </InstagramLoader>
    );
  };

  return (
    <View style={{}}>
      <View style={{alignItems: 'center'}}>
        <FlatList
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{minHeight: '100%'}}
          data={relevant}
          renderItem={Needs}
          keyExtractor={(item) => item.pk}
        />
        <View style={{height: '22%'}} />
      </View>
      {!isNaN(relevant) != '' && (
        <Text style={{textAlign: 'center'}}>Nothing Found</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  HeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  buttonBox: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: width * 0.2,
    paddingVertical: 5,
    borderWidth: 0.5,
    borderColor: '#eee',
  },
  buttonText: {
    fontSize: 17,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#9390a7',
  },
  header: {
    fontSize: 22,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 20,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#aaa',
    elevation: 0.08,
  },
  left: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: box_width * 0.17,
    justifyContent: 'center',
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.55,
  },
  lefContent: {
    height: Reward_Box_Height * 0.4,
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 10,
    borderRadius: 30,
  },

  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.7),
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFValue(8, box_width), // second argument is standardScreenHeight(optional),
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.22,
    height: Reward_Box_Height,
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
    right: 7,
  },
});
