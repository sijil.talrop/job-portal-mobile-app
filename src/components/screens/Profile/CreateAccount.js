import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Picker} from '@react-native-picker/picker';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';
const {width, height} = Dimensions.get('window');
import Modal from 'react-native-modal';
import SetBar from './SetBar';

export default function CreateAccount(props) {
  const {state, dispatch} = useContext(Context);
  const [first_name, setFirst_name] = useState('');
  const [last_name, setLast_name] = useState('');
  const [phone, setPhone] = useState(props.phone);
  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const [location, setLocation] = useState();
  const [position, setPosition] = useState();
  const [location_list, setLocation_list] = useState([]);
  const [jobpositionlist, setJobPositionList] = useState([]);
  const [label, setLabel] = useState();

  const [isModalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  useEffect(() => {
    get_location_list();
    get_Job_position();
  }, [500]);

  const get_location_list = () => {
    let post_url = base.BASE_URL + 'cities/locations/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setLocation_list(data.data);
        } else {
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_Job_position = () => {
    let post_url = base.BASE_URL + 'jobs/job-positions/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setJobPositionList(data.data);
        } else {
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const registration = async () => {
    let post_url = base.BASE_URL + 'users/register/';
    let access_token = state.user_data.access_token;
    axios
      .post(
        post_url,
        {
          phone: phone,
          first_name: first_name,
          last_name: last_name,
          location: location,
          user_type: 'qSi2NQRUWKWmmIwxgkRMuOUgcEQ5gfkr',
          job_position: position,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          dispatch({
            type: 'UPDATE_USER_DATA',
            user_data: {
              ...state.user_data,
              is_verified: true,
            },
          });
          Toast.show(data.message, Toast.SHORT);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  const showAlert = () => {
    Alert.alert('You need to...');
  };

  return (
    <View style={styles.middleContent}>
      <View style={styles.loginUser}>
        <TextInput
          value={String(first_name)}
          style={styles.input}
          placeholder="First Name"
          autoFocus={true}
          autoCompleteType="off"
          returnKeyType={'next'}
          onChangeText={(val) => setFirst_name(val)}
          onSubmitEditing={() => ref_input2.current.focus()}
        />
        <Icon name={'account-heart'} color={'#aaa'} size={25} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={String(last_name)}
          style={styles.input}
          placeholder="Last Name"
          ref={ref_input2}
          autoCompleteType="off"
          returnKeyType={'next'}
          onChangeText={(val) => setLast_name(val)}
          onSubmitEditing={() => ref_input3.current.focus()}
        />
        <Icon name={'account-heart'} color={'#aaa'} size={25} />
      </View>
      <View style={[styles.loginUser, {paddingVertical: 15}]}>
        <Text style={styles.inputText}>{props.phone}</Text>
        <Icon name={'phone-classic'} color={'#aaa'} size={25} />
      </View>
      <View style={styles.loginUser1}>
        <TouchableOpacity
          onPress={() => {
            toggleModal();
          }}
          style={styles.loginUserLocation}>
          {location ? (
            <Text style={styles.locationText}>
              {label.length > 20 ? label.substring(0, 26) + '...' : label}
            </Text>
          ) : (
            <Text style={styles.locationText}>Select Location</Text>
          )}
          <Icon name="google-maps" size={30} color="#aaa" />
        </TouchableOpacity>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={position}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setPosition(itemValue)}>
          <Picker.Item key={'none'} label={'Job Position'} value={'Position'} />
          {jobpositionlist &&
            jobpositionlist.length > 0 &&
            jobpositionlist.map((item, index) => (
              <Picker.Item
                key={item.value}
                label={item.label}
                value={item.value}
              />
            ))}
        </Picker>
      </View>
      <TouchableOpacity
        onPress={() => {
          if (position != 'Position') {
            registration();
          } else {
            Alert.alert('Select Job Position');
            showAlert();
          }
        }}
        style={styles.login}>
        <Text style={styles.buttonText}>Continue</Text>
      </TouchableOpacity>
      <Modal onBackButtonPress={toggleModal} isVisible={isModalVisible}>
        <View style={{flex: 1}}>
          <SetBar
            setLocation={setLocation}
            setLabel={setLabel}
            toggleModal={toggleModal}
          />
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  Scroll: {
    backgroundColor: 'red',
  },
  middleContent: {
    marginTop: width * 0.04,
    width: width * 0.9,
  },
  inputContent: {
    paddingVertical: height * 0.01,
  },
  input: {
    fontSize: 15,
    width: '80%',
    fontFamily: 'BalooPaaji2-SemiBold',
    backgroundColor: '#f5f6f9',
  },
  loginUser: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    marginVertical: 10,
    paddingVertical: 5,
    borderRadius: 20,
    paddingHorizontal: 20,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 15,
    marginVertical: 10,
    borderRadius: 20,
    paddingVertical: 5,
    paddingLeft: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  loginUserLocation: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
    paddingHorizontal: 10,
  },
  picker: {
    fontSize: 10,
    color: '#aaa',
  },
  login: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    paddingVertical: 15,
    marginTop: height * 0.05,
  },
  buttonText: {
    fontSize: 15,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  inputText: {
    fontSize: 15,
    color: '#aaa',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  locationText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#aaa',
    fontSize: 15,
  },
});
