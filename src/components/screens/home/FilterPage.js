import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Animated,
  Dimensions,
  Button,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Picker} from '@react-native-picker/picker';
import Toast from 'react-native-simple-toast';
import {CommonActions, useNavigation} from '@react-navigation/native';
import * as base from '../../../../Settings';
import axios from 'axios';
import Modal from 'react-native-modal';
import SetBar from '../../screens/Profile/SetBar';
const {width, height} = Dimensions.get('window');

export default function FilterPage(props) {
  const navigation = useNavigation();
  const [selectedvalue, setSelectedvalue] = useState();
  const bottomValue = useRef(new Animated.Value(100)).current;
  const [category_list, setCategory_list] = useState([]);
  const [subcategory_list, setSubcategory_list] = useState([]);
  const [jobtype, setJobtype] = useState([]);
  const [category, setCategory] = useState();
  const [subcategory, setSubcategory] = useState();
  const [location, setLocation] = useState();
  const [label, setLabel] = useState();

  const [popular_job, setPopular_job] = useState();
  const [isModalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  let _get_categories = () => {
    // &subcategory=${subcategory}&location=${location}
    // category=${category}&jobtype=${selectedvalue}&
    let get_url =
      base.BASE_URL +
      `jobs/jobs/?location=${location}&category=${category}&jobtype=${selectedvalue}`;
    console.log(get_url);
    axios
      .get(get_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setPopular_job(data.data);
          setTimeout(() => {
            navigation.navigate('FilterListPage', {popular_job: popular_job});
          }, 500);
        } else {
          Toast.show(data.message, Toast.SHORT);
          console.warn(response.error, 'gghghghghhfghg');
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_category_list = () => {
    let post_url = base.BASE_URL + 'jobs/job-categories/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCategory_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  const get_subcategory_list = () => {
    let post_url = base.BASE_URL + 'jobs/job-positions/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setSubcategory_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };
  const get_job_type = () => {
    let post_url = base.BASE_URL + 'jobs/job-types/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setJobtype(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const closeButton = () => {
    setLocation('');
  };

  const confirmHandler = (value) => {
    setSelectedvalue(value);
  };
  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 80000,
    }).start();
  };

  useEffect(() => {
    jumpUp();
    get_category_list();
    get_subcategory_list();
    get_job_type();
  }, []);

  const jobType = () => {
    return (
      <View style={styles.flexBox}>
        {jobtype.map((item, index) => (
          <TouchableOpacity
            key={item.value}
            onPress={() => {
              confirmHandler(item.value);
            }}
            style={[
              styles.mainContainer,
              {
                backgroundColor:
                  item.value === selectedvalue ? '#2261a6' : '#fafafa',
              },
            ]}>
            <Text
              style={[
                styles.itemFont,
                {
                  color: item.value === selectedvalue ? '#fff' : '#aaa',
                },
              ]}>
              {item.label}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
    );
  };

  return (
    <View style={styles.middleContent}>
      <Animated.View
        style={[
          styles.contentBox,
          {
            transform: [{translateY: bottomValue}],
          },
        ]}>
        <Text style={styles.topHeader}>Set Filters</Text>
        <View>
          <Text style={styles.headText}>Category</Text>
          <View style={styles.loginUser1}>
            <Icon name="bag-checked" size={30} color="#000" />
            <Picker
              selectedValue={category}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
              {category_list &&
                category_list.length > 0 &&
                category_list.map((item, index) => (
                  <Picker.Item
                    key={index}
                    label={item.label}
                    value={item.value}
                  />
                ))}
            </Picker>
          </View>
        </View>
        <View>
          <Text style={styles.headText}>Sub Category</Text>
          <View style={styles.loginUser1}>
            <Icon name="bag-checked" size={30} color="#000" />
            <Picker
              selectedValue={subcategory}
              style={styles.picker}
              onValueChange={(itemValue, itemIndex) =>
                setSubcategory(itemValue)
              }>
              {subcategory_list &&
                subcategory_list.length > 0 &&
                subcategory_list.map((item, index) => (
                  <Picker.Item
                    label={item.label}
                    key={index}
                    value={item.value}
                  />
                ))}
            </Picker>
          </View>
        </View>
        <View>
          <Text style={styles.headText}>Location</Text>
          <TouchableOpacity
            onPress={() => {
              toggleModal();
            }}
            style={styles.loginUserLocation}>
            <Icon name="google-maps" size={30} color="#000" />
            {/* <Text>{location}</Text> */}
            {location ? (
              <Text style={styles.locationText}>
                {label.length > 20 ? label.substring(0, 26) + '...' : label}
              </Text>
            ) : (
              <Text style={styles.locationText}>Select Location</Text>
            )}
            <TouchableOpacity
              onPress={() => {
                closeButton();
              }}>
              <Icon name="close" size={20} color="#000" />
            </TouchableOpacity>

            {/* <Text>{location}</Text> */}
            {/* <TextInput
              value={location}
              style={styles.locationBox}
              placeholder="Location"
              returnKeyType={'next'}
              onChangeText={(val) => setLocation(val)}
            /> */}
          </TouchableOpacity>
        </View>
        <View style={{width: width, paddingHorizontal: 20}}>
          <Text style={styles.headText}>Job Type</Text>
          {jobType()}
        </View>
        <TouchableOpacity
          onPress={() => {
            _get_categories();
          }}
          style={styles.login}>
          <Text style={styles.buttonText}>Apply Filters</Text>
        </TouchableOpacity>
      </Animated.View>
      <Modal onBackButtonPress={toggleModal} isVisible={isModalVisible}>
        <View style={{flex: 1}}>
          <SetBar
            setLocation={setLocation}
            setLabel={setLabel}
            toggleModal={toggleModal}
          />
        </View>
      </Modal>
    </View>
  );
}

const styles = StyleSheet.create({
  middleContent: {
    width: width,
    alignItems: 'center',
    justifyContent: 'flex-end',
    minHeight: '100%',
    backgroundColor: 'transparent',
  },
  contentBox: {
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingVertical: 30,
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  topHeader: {
    fontSize: 20,
    marginBottom: 20,
    fontFamily: 'BalooPaaji2-SemiBold',
  },

  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 15,
    marginVertical: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    width: width * 0.9,
    // paddingVertical: 10,
    borderWidth: 1,
    borderColor: '#eee',
  },
  loginUserLocation: {
    backgroundColor: '#f5f6f9',
    borderRadius: 15,
    marginVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    alignItems: 'center',
    width: width * 0.9,
    paddingVertical: 10,
    borderWidth: 1,
    borderColor: '#eee',
    justifyContent: 'space-between',
  },
  locationText: {
    fontFamily: 'BalooPaaji2-Regular',
    marginLeft: 30,
  },
  picker: {
    fontSize: 10,
    color: '#aaa',
    width: width * 0.7,
  },
  locationBox: {
    fontSize: 16,
    color: '#aaa',
    width: width * 0.7,
  },
  login: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    paddingVertical: 15,
    marginTop: height * 0.05,
    width: width - 40,
  },
  buttonText: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
  },
  container: {
    flexDirection: 'column',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  flexBox: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    flexWrap: 'wrap',
  },
  itemFont: {
    fontFamily: 'BalooPaaji2-Regular',
    textAlign: 'center',
    fontSize: 14,
  },
  mainContainer: {
    paddingHorizontal: 12,
    paddingVertical: 8,
    borderRadius: 15,
    margin: 10,
    backgroundColor: '#f5f6f9',
  },
  headText: {
    fontSize: 15,
    marginBottom: 10,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
});
