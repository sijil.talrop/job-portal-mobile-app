import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Animated,
  Image,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.3;
const card_width = width * 0.6;
const box_width = width - 40;

export default function CompanyBasedJob(props) {
  const navigation = useNavigation();
  const [selectedRating, setSelectedRating] = useState('');
  const fadeAnim = useRef(new Animated.Value(0)).current;

  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };
  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: true,
    }).start();
  };

  useEffect(() => {
    if (isNaN(props.jobs)) {
      setTimeout(() => {
        setSelectedRating(props.jobs[0].name);
      }, 500);
    }
    fadeIn();
  }, []);

  const Needs = () => {
    return props.jobs.map((item, index) => (
      <TouchableOpacity
        key={index}
        onPress={() => {
          TouchableHandler(item.name);
          navigation.navigate('CompanyJobSinglePage', {
            item: item,
          });
        }}
        activeOpacity={0.9}
        style={[
          styles.mainContainer,
          {
            backgroundColor: item.name == selectedRating ? '#2261a6' : '#fff',
          },
        ]}>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.name,
              {color: item.name == selectedRating ? '#fff' : '#130937'},
            ]}>
            {item.name.length > 17
              ? item.name.substring(0, 17) + '...'
              : item.name}
          </Text>
          <View style={styles.Location}>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}>
              <Text
                style={[
                  styles.amount,
                  {
                    color: item.name == selectedRating ? '#fff' : '#130937',
                  },
                ]}>
                ₹{item.salary_minimum} - {item.salary_maximum}/
              </Text>
              <Text
                style={[
                  {fontFamily: 'BalooPaaji2-Regular', fontSize: 15},
                  {color: item.name == selectedRating ? '#fff' : '#130937'},
                ]}>
                m
              </Text>
            </View>
            <View style={styles.locationCharge}>
              <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
              <Text
                style={[
                  styles.place,
                  {
                    color: item.name == selectedRating ? '#fff' : '#130937',
                  },
                ]}>
                {item.location.length > 17
                  ? item.location.substring(0, 25) + '...'
                  : item.location}
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.right}>
          <View style={styles.rateContainer}>
            <Icon
              name={'bookmark'}
              color={item.kind == 'Featured' ? '#ef801f' : '#eee'}
              size={50}
            />
          </View>

          <View style={styles.delivery}>
            <Text
              style={[
                styles.bottomButtonText,
                {
                  color: item.name == selectedRating ? '#fff' : '#aaa',
                },
              ]}>
              {item.job_type}
            </Text>
            <Text
              style={[
                styles.bottomButtonText,
                {
                  color: item.name == selectedRating ? '#fff' : '#aaa',
                },
              ]}>
              {item.date_added}
            </Text>
          </View>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <View style={{minHeight: '100%'}}>
      <View style={{flex: 1}}>
        <ScrollView
          contentContainerStyle={{
            alignItems: 'center',
            paddingVertical: 20,
          }}>
          {!isNaN(props.jobs) != '' && (
            <Animated.View
              style={[
                {
                  opacity: fadeAnim,
                },
              ]}>
              <Image
                style={styles.foundImage}
                source={require('../../../assets/vector-images/noresultfound.png')}
              />
              <Text style={styles.messageText}>Nothing Found</Text>
            </Animated.View>
          )}
          {Needs()}
        </ScrollView>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 1,
    paddingHorizontal: 20,
  },
  contentContairener: {
    width: box_width * 0.59,
    height: Reward_Box_Height,
    justifyContent: 'center',
  },
  Location: {},
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFValue(8, box_width), // second argument is standardScreenHeight(optional),
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  amount: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFPercentage(1.9),
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.47),
    textTransform: 'capitalize',
    marginLeft: 3,
  },
  right: {
    alignItems: 'flex-end',
    justifyContent: 'space-evenly',
    width: box_width * 0.3,
    height: Reward_Box_Height,
  },
  delivery: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: RFPercentage(1.4),
    fontFamily: 'BalooPaaji2-Regular',
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
    right: '5%',
  },
  emptyText: {
    color: '#000',
    fontSize: 20,
    fontFamily: 'BalooPaaji2-Regular',
  },
  foundImage: {
    width: width * 0.5,
    height: width * 0.5,
    resizeMode: 'contain',
  },
  messageText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'BalooPaaji2-Regular',
  },
});
