import React, {useState, useEffect, useRef} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Dimensions,
  TextInput,
  TouchableOpacity,
  ScrollViewComponent,
  ImageBackground,
} from 'react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

//svg
import Success from './../../../assets/Icons/success.svg';

const {height, width} = Dimensions.get('window');

const Storys = () => {
  return (
    <View style={styles.mainContainer}>
      <View style={styles.innerContainer}>
        <View style={styles.imageContainer}>
          <ImageBackground
            source={require('../../../assets/Icons/elipse.png')}
            style={styles.backImage}>
            <View style={styles.imageView}>
              <Success height={90} width={90} />
              <Text style={styles.txt}>Success!</Text>
            </View>
          </ImageBackground>
        </View>
        <Text style={styles.para}>You Logined Successfully</Text>
        <TouchableOpacity style={styles.button}>
          <Text style={styles.btnTxt}>CONTINUE</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  mainContainer: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
    backgroundColor: '#000',
  },
  innerContainer: {
    backgroundColor: '#fff',
    borderRadius: 12,
    width: '100%',
    height: '33%',
    alignItems: 'center',
    // backgroundColor: '#000',
    overflow: 'hidden',
  },
  imageContainer: {
    width: '100%',
    height: '55%',
    marginBottom: 20,
    position: 'relative',
    overflow: 'hidden',
  },
  backImage: {
    width: '100%',
    height: '100%',
    resizeMode: 'cover',
    overflow: 'hidden',
  },
  imageView: {
    alignItems: 'center',
    position: 'absolute',
    top: 20,
    bottom: 0,
    right: 0,
    left: 0,
    // backgroundColor: '#000',
  },
  txt: {
    fontFamily: 'Poppins-Medium',
    fontSize: RFValue(26, height),
    color: '#4eabff',
  },
  para: {
    fontFamily: 'Poppins-Regular',
    fontSize: RFValue(16, height),
    color: '#202020',
    marginBottom: 15,
  },
  button: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    height: 50,
    width: '85%',
    backgroundColor: '#46adff',
  },
  btnTxt: {
    textAlign: 'center',
    color: '#ffff',
    fontFamily: 'Poppins-Medium',
    fontSize: RFValue(16, height),
  },
});
export default Storys;
