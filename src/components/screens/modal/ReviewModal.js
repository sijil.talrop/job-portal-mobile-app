import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  Image,
  TextInput,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Rating, AirbnbRating} from 'react-native-ratings';
import axios from 'axios';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';

const {width, height} = Dimensions.get('window');

export default function AmountHandOver(props) {
  const {state, dispatch} = useContext(Context);
  const [commentbox, setCommentBox] = useState(false);
  const [comments, SetComments] = useState();
  const [rating, setRating] = useState('');
  const [review, setReview] = useState();

  const [isFocused, setIsFocused] = useState({
    name: false,
    email: false,
    comment: false,
  });
  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };

  const ratingCompleted = (rating) => {
    if (rating == 1) {
      setRating('1.5');
      console.warn(rating);
    } else if (rating == 2) {
      setRating('2.5');
      console.warn(rating);
    } else if (rating == 3) {
      setRating('3.5');
      console.warn(rating);
    } else if (rating == 4) {
      setRating('4.5');
      console.warn(rating);
    } else if (rating == 5) {
      setRating('5.0');
      console.warn(rating);
    } else {
      setRating('3.0');
    }
  };

  const rate_my_company = async () => {
    let access_token = state.user_data.access_token;
    let post_url = base.BASE_URL + 'general/create-rating/' + props.pk + '/';
    axios
      .post(
        post_url,
        {
          rating: rating,
          comment: comments,
        },
        {
          headers: {
            Authorization: 'Bearer ' + access_token,
          },
        },
      )
      .then((response) => {
        console.warn(rating);
        if (response.data.StatusCode == 6000) {
          ToastAndroid.show('Successfully Submitted', ToastAndroid.LONG);
          setReview(response.data);
        } else {
          console.warn(response.data);
        }
      })
      .catch((error) => {});
  };

  const reviewCommet = () => {
    return (
      <View>
        <Text style={styles.inputHead}>Comment</Text>
        <View
          style={[
            styles.inputBox,
            {borderColor: isFocused.comment ? '#ef801f' : '#aaa'},
          ]}>
          <TextInput
            style={styles.input}
            // underlineColorAndroid="transparent"
            placeholder="Comment"
            placeholderTextColor={isFocused.comment ? '#ef801f' : '#aaa'}
            autoCapitalize="sentences"
            onFocus={() => {
              handleInputFocus('comment');
            }}
            onBlur={() => {
              handleInputBlur('comment');
            }}
            onChangeText={(val) => {
              SetComments(val);
            }}
            autoCompleteType="off"
            multiline={true}
          />
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <View style={[styles.contentContainer]}>
        <View style={{alignItems: 'center'}}>
          <Text style={styles.headerText}>Rate The Company</Text>
          <Text style={styles.subText}>
            How did you find this company based on{'\n'} your usage ?
          </Text>
        </View>
        <View style={styles.TopContent}>
          <View style={styles.imageMainBox}>
            <View style={styles.imageBox}>
              <Image style={styles.image} source={{uri: props.logo}} />
            </View>
          </View>
          <Text style={styles.companyName}>Dream Find</Text>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Text style={styles.amount}>Software Solutions</Text>
            <View style={styles.detailBox}>
              <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
              <Text style={styles.locationText}>
                {/* {route.params.item.location} */}
                kollam
              </Text>
            </View>
          </View>
        </View>
        {!commentbox && (
          <View style={styles.handoverBox}>
            <AirbnbRating
              reviews={['Horrible', 'Bad', 'Average', 'Good', 'Excellent']}
              count={5}
              selectedColor={'#EE813A'}
              unSelectedColor="#eee"
              reviewColor={'#4cb050'}
              defaultRating={'3.0'}
              size={45}
              reviewSize={14}
              tintColor="red"
              starContainerStyle={{backgroundColor: '#fff'}}
              onFinishRating={ratingCompleted}
            />
          </View>
        )}
        {commentbox && <View>{reviewCommet()}</View>}

        {commentbox ? (
          <TouchableOpacity
            onPress={() => {
              rate_my_company();
              setTimeout(() => {
                props.toggleModal();
              }, 500);
            }}
            style={styles.loginButton}>
            <Text style={[styles.loginText, {fontSize: 20}]}>Submit</Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            onPress={() => {
              setCommentBox(!commentbox);
            }}
            style={styles.loginButton}>
            <Text style={[styles.loginText, {fontSize: 20}]}>Next</Text>
          </TouchableOpacity>
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    minHeight: '100%',
    justifyContent: 'flex-end',
    flex: 1,
    alignItems: 'center',
  },
  backgroundImage: {},
  contentContainer: {
    width: width,
    height: height * 0.8,
    paddingHorizontal: 30,
    justifyContent: 'center',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    borderWidth: 1,
    elevation: 1,
    borderColor: '#eee',
  },
  loginButton: {
    backgroundColor: '#EE813A',
    borderRadius: 10,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 20,
    paddingVertical: 10,
    paddingHorizontal: 15,
    flexDirection: 'row',
  },
  headerText: {
    fontSize: 20,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#000',
  },
  subText: {
    fontSize: 14,
    color: '#726E86',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-Regular',
  },
  handoverBox: {
    justifyContent: 'center',
    alignItems: 'center',
    marginVertical: 20,
  },
  loginText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#fff',
  },
  companyName: {
    fontSize: 22,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  TopContent: {
    alignItems: 'center',
    marginBottom: 20,
  },
  detailBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  locationText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
  },
  imageMainBox: {
    width: width * 0.3,
    height: width * 0.3,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  imageBox: {
    width: width * 0.2,
    height: width * 0.2,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 30,
    overflow: 'hidden',
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    // width: width * 0.5,
    borderRadius: 15,
    marginVertical: 10,
  },
  input: {
    width: '100%',
    // backgroundColor: 'red',
    paddingHorizontal: 10,
  },
});
