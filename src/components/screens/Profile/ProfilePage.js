import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Alert,
  Linking,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import {
  CommonActions,
  useNavigation,
  useIsFocused,
} from '@react-navigation/native';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {ImageBackground} from 'react-native';
import EducationComponent from './EducationComponent';
import {Context} from '../../contexts/Store';
import * as base from '../../../../Settings';
const {width, height} = Dimensions.get('window');
const card_width = width * 0.28;
const card_height = width * 0.28;

export default function MatchedJobs() {
  const isfocused = useIsFocused();
  const navigation = useNavigation();
  const [isOpen, setIsOpen] = useState(false);
  const [selectedBox, setSelectedBox] = useState('Saved Jobs');
  const [profile, setProfile] = useState([]);
  const [jobposition, setJobPosition] = useState();
  const [followedCompany, setFollwedCompany] = useState([]);
  const [singleFile, setSingleFile] = useState(null);
  const [appliedJobCount, setAppliedJobCount] = useState();
  const [savedJobCount, setSavedJobCount] = useState();
  const [matchedJobCount, setMatchedJobCount] = useState();
  const {state, dispatch} = useContext(Context);

  const changeValue = (value) => {
    setSelectedBox(value);
  };

  const renderCategory = [
    {
      id: 1,
      number: savedJobCount,
      category: 'Saved Jobs',
    },
    {
      id: 2,
      number: appliedJobCount,
      category: 'Applied Jobs',
    },
    {
      id: 3,
      number: matchedJobCount,
      category: 'Matched Jobs',
    },
  ];

  const navigationRender = (selectedBox, category) => {
    if (selectedBox == 'Saved Jobs') {
      return navigation.push('SavedJobs');
    } else if (selectedBox == 'Applied Jobs') {
      return navigation.push('AppliedJobs');
    } else if (selectedBox == 'Matched Jobs') {
      return navigation.push('MatchedJobs');
    }
  };

  useEffect(() => {
    if (isfocused) {
      _get_profile();
    }
  }, [isfocused]);

  //  #*****profile data fetch***
  let _get_profile = async () => {
    let get_url = base.BASE_URL + 'users/profile/';
    let user = await AsyncStorage.getItem('user');
    let access_token = state.user_data.access_token;

    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data, message} = response.data;
        if (StatusCode == 6000) {
          setProfile(data.data);
          setJobPosition(data.data.job_position);
          let profile_data = data.data;
          dispatch({
            type: 'UPDATE_USER_DATA',
            user_data: {
              ...state.user_data,
              profile_data: profile_data,
            },
          });
          setAppliedJobCount(data.applied_jobs_count);
          setSavedJobCount(data.saved_jobs_count);
          setMatchedJobCount(data.matched_jobs_count);
          setFollwedCompany(data.followed_data);
        } else {
          setProfile([]);
        }
      })
      .catch((error) => {
        console.warn(error.response);
        if (error.response.status == 401) {
          dispatch({
            type: 'UPDATE_USER_DATA',
            user_data: {
              is_verified: false,
            },
          });
        }
      });
  };

  return (
    <ScrollView
      showsVerticalScrollIndicator={false}
      contentContainerStyle={styles.mainContainer}>
      <ImageBackground
        style={styles.backgroundImageBox}
        source={require('../../../assets/vector-images/Group20260.png')}
      />
      <View
        style={{
          alignItems: 'center',
          paddingTop: height * 0.07,
          paddingBottom: height * 0.035,
        }}>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            navigation.navigate('CreateProfile', {
              image: profile.photo,
              about: profile.about,
              first_name: profile.first_name,
              follower: followedCompany,
            });
          }}>
          <View
            style={[
              styles.buttonBox,
              {
                backgroundColor: '#fff',
                position: 'absolute',
                right: 20,
                bottom: 0,
              },
            ]}>
            <Icon name={'lead-pencil'} color={'#2261a6'} size={18} />
          </View>
          <View activeOpacity={0.8} style={styles.imageContainer}>
            <Image style={styles.image} source={{uri: profile.photo}} />
          </View>
        </TouchableOpacity>

        <Text style={styles.name}>
          {profile.first_name} {profile.last_name}
        </Text>
        <View style={{flexDirection: 'row', flexWrap: 'wrap'}}>
          {jobposition &&
            jobposition.length > 0 &&
            jobposition.map((item) => (
              <View
                style={{
                  backgroundColor: '#2261a6',
                  borderRadius: 10,
                  paddingVertical: 5,
                  paddingHorizontal: 10,
                  margin: 10,
                }}>
                <Text style={styles.position_Of_job}>{item.name}</Text>
              </View>
            ))}
        </View>
        <Text style={styles.mail}>{profile.email}</Text>
      </View>
      <View style={styles.contentBox}>
        {renderCategory.map((item, index) => (
          <TouchableOpacity
            key={index}
            onPress={() => {
              changeValue(item.category);
              navigationRender(item.category, selectedBox);
            }}
            style={[
              styles.itemBox,
              {
                backgroundColor:
                  item.category == selectedBox ? '#2261a6' : '#fff',
              },
            ]}>
            <Text
              style={[
                styles.boxHeader,
                {color: item.category == selectedBox ? '#fff' : '#130937'},
              ]}>
              {item.number}
            </Text>
            <Text
              style={[
                styles.BoxSubHeader,
                {color: item.category == selectedBox ? '#fff' : '#130937'},
              ]}>
              {item.category}
            </Text>
          </TouchableOpacity>
        ))}
      </View>
      <View style={{width: width - 40, marginTop: 10}}>
        <TouchableOpacity
          onPress={() => {
            Linking.openURL(
              'http://jobportal.talrop.works/download-cv/' + profile.pk + '/',
            );
          }}
          activeOpacity={0.8}
          style={styles.inputBox}>
          <Icon name={'update'} color={'#000'} size={30} />
          <View style={styles.contentRightBox}>
            <Text style={styles.BottomContentHead}>Download CV</Text>
            <Text style={styles.BottomText}>
              Last updated on {profile.date_added}
            </Text>
          </View>
        </TouchableOpacity>
        <View style={styles.aboutContainer}>
          <Text style={styles.aboutHead}>About</Text>
          <Text style={styles.Description}>{profile.about}</Text>
          <View style={{borderRadius: 15, padding: 20}}>
            <View style={[styles.detailBox, {borderBottomWidth: 1}]}>
              <Text style={styles.detailText}>Phone</Text>
              <Text style={styles.detailText2}>{profile.phone}</Text>
            </View>
            {isNaN(profile.dob) != '' && (
              <View style={styles.detailBox}>
                <Text style={styles.detailText}>Dob</Text>
                <Text style={styles.detailText2}>{profile.dob}</Text>
              </View>
            )}
          </View>
          {isNaN(profile.address) != '' && (
            <View style={styles.addressComponent}>
              <Text style={styles.head}>Address</Text>
              <Text style={styles.AddressFont}>{profile.address}</Text>
            </View>
          )}
          <View style={styles.addressComponent}>
            <Text style={styles.head}>Location</Text>
            <Text style={styles.AddressFont}>{profile.location}</Text>
          </View>
          <View style={styles.drawebleBox}>
            <TouchableOpacity
              activeOpacity={0.8}
              onPress={() => setIsOpen(!isOpen)}
              style={styles.readMore}>
              <Text style={styles.read}>Read More</Text>
              <Icon
                name={isOpen ? 'chevron-up' : 'chevron-down'}
                color={'#2261a6'}
                size={40}
              />
            </TouchableOpacity>
          </View>
        </View>
        {isOpen && (
          <React.Fragment>
            <View style={styles.component}>
              <EducationComponent />
            </View>
          </React.Fragment>
        )}
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    backgroundColor: '#fff',
    minHeight: '100%',
    flexDirection: 'column',
    justifyContent: 'space-around',
  },
  imageContainer: {
    width: width * 0.3,
    height: width * 0.3,
    borderRadius: 100,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: '#ddd',
  },
  backgroundImageBox: {
    resizeMode: 'contain',
    width: width * 0.5,
    height: width * 0.4,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
    textTransform: 'capitalize',
    color: '#130937',
    marginTop: 15,
  },
  mail: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#959595',
  },
  position_Of_job: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#fff',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    borderRadius: 100,
    overflow: 'hidden',
  },
  itemBox: {
    width: card_width,
    height: card_height,
    borderWidth: 0.4,
    borderColor: '#aaa',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'space-evenly',
    elevation: 3,
  },
  contentBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: width - 40,
  },
  textInput: {
    justifyContent: 'center',
    height: 40,
    borderRadius: 30,
    width: width * 0.7,
    textAlign: 'center',
    backgroundColor: '#eee',
  },
  inputBox: {
    flexDirection: 'row',
    alignItems: 'center',
    height: width * 0.24,
  },
  contentRightBox: {
    marginLeft: 30,
  },
  BottomContentHead: {
    fontSize: 17,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#130937',
    marginBottom: 10,
  },
  BottomText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    color: '#959595',
  },
  boxHeader: {
    fontSize: 25,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  BoxSubHeader: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.5),
  },
  button: {
    width: width * 0.2,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: 10,
    backgroundColor: '#2261a6',
    borderRadius: 10,
    marginLeft: '5%',
  },
  buttonText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  aboutContainer: {
    marginTop: 20,
    marginBottom: '10%',
    paddingHorizontal: 20,
    paddingVertical: 20,
    borderWidth: 1,
    borderColor: '#eee',
    borderRadius: 20,
    backgroundColor: '#fff',
    elevation: 1,
  },
  addressComponent: {
    padding: 10,
  },
  detailBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    borderStyle: 'dotted',
    borderColor: '#aaa',
    borderRadius: 10,
    paddingVertical: 5,
  },
  head: {
    fontSize: RFPercentage(1.6),
    color: '#2261a6',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  detailText: {
    color: '#2261a6',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.6),
  },
  detailText2: {
    color: '#000',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.6),
  },
  aboutHead: {
    fontSize: 17,
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#000',
  },
  Description: {
    color: '#3d345b',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.5),
    textAlign: 'justify',
  },
  AddressFont: {
    color: '#000',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.5),
  },
  drawebleBox: {
    alignItems: 'center',
    justifyContent: 'center',
    borderTopWidth: 1,
    marginTop: 10,
    borderColor: '#aaa',
  },
  readMore: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 8,
    paddingTop: 10,
  },
  read: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
    color: '#afadbd',
  },
  buttonBox: {
    padding: 5,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    elevation: 0.1,
    borderWidth: 0.5,
    borderColor: '#2261a6',
  },
});
