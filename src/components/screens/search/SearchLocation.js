import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  Platform,
  Dimensions,
} from 'react-native';
import * as base from '../../../../Settings';
import axios from 'axios';
import Toast from 'react-native-simple-toast';
import SearchBar from 'react-native-dynamic-search-bar';
import SearchResultPopup from '../popup-pages/SearchResultPopup';

const {width, height} = Dimensions.get('window');
const box_width = width * 0.8;
const Reward_Box_Height = width * 0.1;
const card_width = width * 0.9;

function SearchPage(props) {
  const [selectedRating, setSelectedRating] = useState('Facebook');
  const TouchableHandler = (value) => {
    props.setLocation(value);
  };

  const [search, setSearch] = useState([]);
  const [country_list, setCountry_list] = useState([]);
  const [selectedvalue, setSelectedvalue] = useState();
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => {
      _get_categories();
    }, 2000);
    get_country_list();
  }, []);

  let _get_categories = () => {
    let get_url = base.BASE_URL + `cities/locations/?q=${selectedvalue}`;
    axios
      .get(get_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setSearch(data.data);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        } else {
          Toast.show('Please Type', Toast.SHORT);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        }
      })
      .catch((error) => {
        console.warn(error);
        setTimeout(() => {
          setLoading(false);
        }, 500);
      });
  };

  const get_country_list = () => {
    let post_url = base.BASE_URL + 'cities/locations/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCountry_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const render_option = () => {
    return search.map((item, index) => (
      <TouchableOpacity
        key={item.label}
        onPress={() => {
          TouchableHandler(item.label);
          props.toggleModal();
        }}
        activeOpacity={0.9}
        style={[styles.mainContainer]}>
        <View style={styles.contentContairener}>
          <Text
            style={[
              styles.amount,
              {
                // color: item.business == selectedRating ? '#fff' : '#130937',
              },
            ]}>
            {item.label}
          </Text>
        </View>
      </TouchableOpacity>
    ));
  };

  return (
    <View
      style={{
        // width: 400,
        height: width,
        backgroundColor: 'transparent',
        alignItems: 'center',
      }}>
      <View
        style={{
          //   width: width,
          alignItems: 'center',
          justifyContent: 'center',
          flexDirection: 'row',
        }}>
        <View style={styles.searchContainer}>
          <SearchBar
            autoFocus={true}
            onSearchPress={(text) => {
              setSelectedvalue(text);
              _get_categories();
              // navigation.navigate('FilterListPage', {popular_job: popular_job});
            }}
            clearIconComponent={!search ? true : false}
            openTheKeyboard={true}
            // searchIcon={{ size: 24
            clearIcon={true}
            onChangeText={(text) => {
              setSelectedvalue(text);
            }}
            textInputStyle={{
              backgroundColor: '#f3f5f8',
              width: width * 0.6,
            }}
            onClearPress={() => setSelectedvalue('')}
            // spinnerVisibility={!search ? true : false}
            // showLoading={true}
            placeholder="Search..."
            searchIcon={true}
            value={country_list}
            returnKeyType="search"
            onSubmitEditing={(text) => {
              _get_categories();
              setSelectedvalue(text);
            }}
            onSubmitEditing={(value) => {
              // setSelectedvalue(value);
            }}
            style={{
              backgroundColor: '#f3f5f8',
              height: 55,
              width: width * 0.8,
              backgroundColor: '#eee',
              borderRadius: 10,
              borderWidth: 1,
              borderColor: '#eee',
            }}
          />
        </View>
        {/* <TouchableOpacity
          onPress={() => {
            _get_categories();
          }}>
          <Text>jjjj</Text>
        </TouchableOpacity> */}
      </View>
      {!isNaN(country_list) && selectedvalue != '' && (
        <View>
          <SearchResultPopup />
        </View>
      )}
      <View
        style={{
          paddingTop: 20,
          //   width: width,
        }}>
        <ScrollView
          showsVerticalScrollIndicator={false}
          style={{paddingHorizontal: 20, height: '100%'}}>
          {render_option()}
          <View style={{height: height * 0.7}} />
        </ScrollView>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 20,
  },
  title: {
    color: '#000',
    fontFamily:
      Platform.OS === 'ios' ? 'ProductSans-Regular' : 'ProductSans-Bold',

    marginBottom: 5,
  },
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 10,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 1,
  },
  headText: {
    fontSize: 14,
    marginBottom: 20,
  },
  left: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    width: box_width * 0.17,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.53,
  },
  lefContent: {
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
    // backgroundColor: 'yellow',
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 10,
    borderRadius: 30,
  },
  Location: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-between',
  },
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    textTransform: 'capitalize',
    marginBottom: 5,
    color: '#000',
  },
  amount: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 12,
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
    textTransform: 'capitalize',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.24,
    height: Reward_Box_Height,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.5,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
    // elevation: 0.5,
  },
});
export default SearchPage;
