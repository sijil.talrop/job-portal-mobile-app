import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  View,
  Animated,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  Image,
  ImageBackground,
  ToastAndroid,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useNavigation} from '@react-navigation/native';
const {height, width} = Dimensions.get('window');
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import axios from 'axios';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';

export default function CompanyJobSinglePage({route}) {
  const navigation = useNavigation();
  const [saved_job, setSaved_job] = useState([]);
  const {state, dispatch} = useContext(Context);
  const Job_Position = route.params.item.job_position;
  // #Animated.Views Props
  const bottomValue = useRef(new Animated.Value(100)).current;
  const jumpUp = () => {
    Animated.spring(bottomValue, {
      useNativeDriver: true,
      toValue: 0,
      duration: 3000,
    }).start();
  };
  useEffect(() => {
    jumpUp();
    // console.warn(route.params.item);
  }, []);

  let _get_saved_jobs = async () => {
    let get_url =
      base.BASE_URL + 'jobs/save-remove-job/' + route.params.item.pk + '/';
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data, message} = response.data;
        if (StatusCode == 6000) {
          ToastAndroid.show(data.message, ToastAndroid.SHORT);
        } else {
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const About = () => {
    return (
      <Animated.View
        style={[
          styles.About,
          {
            transform: [{translateY: bottomValue}],
          },
        ]}>
        <View>
          <View style={{marginBottom: 15}}>
            <Text style={styles.header}>About </Text>
            <Text style={styles.description}>
              {route.params.item.description}
            </Text>
          </View>
          <ImageBackground
            source={require('../../../assets/images/contact-banner.png')}
            style={styles.bannerContainer}>
            {/* <View> */}
            <View style={styles.letterBox}>
              <Text style={styles.jobHead}>kind of job</Text>
              <Text style={styles.jobHead}>{route.params.item.kind}</Text>
            </View>
            <View style={styles.letterBox}>
              <Text style={styles.jobHead}>Date Added</Text>
              <Text style={styles.jobHead}>{route.params.item.date_added}</Text>
            </View>
            <View style={styles.letterBox}>
              <Text style={styles.jobHead}>Vacancies</Text>
              <Text style={styles.jobHead}>{route.params.item.vacancy}</Text>
            </View>
            <View style={styles.letterBox}>
              <Text style={styles.jobHead}>Number of Applicants</Text>
              <Text style={styles.jobHead}>
                {route.params.item.applicants_count}
              </Text>
            </View>
            <View style={styles.letterBox}>
              <Text style={styles.jobHead}>Job Type</Text>
              <Text style={styles.jobHead}>{route.params.item.job_type}</Text>
            </View>
            {/* </View> */}
          </ImageBackground>
          <View style={styles.contentBoxMiddle}>
            <Text style={styles.header}>Job Positions</Text>
            {Job_Position &&
              Job_Position.length > 0 &&
              Job_Position.map((item, index) => (
                <View key={index} style={styles.basicContent}>
                  <Icon name={'star-outline'} color={'#2261a6'} size={20} />

                  <Text style={[styles.description, {marginLeft: 20}]}>
                    {item.name}
                  </Text>
                </View>
              ))}
          </View>
          {route.params.item.qualifications.length > 0 && (
            <View style={styles.contentBoxMiddle}>
              <Text style={styles.header}>Qualification</Text>
              <View style={styles.basicContent}>
                <Icon name={'star-outline'} color={'#2261a6'} size={20} />
                <Text style={[styles.description, {marginLeft: 20}]}>
                  {route.params.item.qualifications}
                </Text>
              </View>
            </View>
          )}
        </View>
      </Animated.View>
    );
  };
  return (
    <View style={styles.mainContentBox}>
      <ImageBackground
        source={require('../../../assets/vector-images/Group20260.png')}
        style={styles.backgroundImage}
      />
      <ScrollView contentContainerStyle={styles.ContentBox}>
        <View style={styles.TopContent}>
          <View style={styles.imageMainBox}>
            <View style={styles.imageBox}>
              <Image
                style={styles.image}
                source={{uri: route.params.item.logo}}
              />
            </View>
          </View>
          <Text style={styles.companyName}>{route.params.item.name}</Text>
          <View style={styles.detailBox}>
            <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
            <Text style={styles.locationText}>
              {route.params.item.location.length > 17
                ? route.params.item.location.substring(0, 40) + '...'
                : route.params.item.location}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-around',
              width: '80%',
            }}>
            <View style={styles.typeView}>
              <Text>{route.params.item.job_type}</Text>
            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text style={styles.amount}>
                ₹{route.params.item.salary_minimum}-
                {route.params.item.salary_maximum}/
              </Text>
              <Text style={styles.amount2}>m</Text>
            </View>
          </View>
        </View>
        {About()}
      </ScrollView>
      <View style={styles.fixedContent}>
        <TouchableOpacity
          onPress={() => {
            _get_saved_jobs();
          }}
          style={styles.imageContent}>
          <Icon name={'bookmark-outline'} color={'#ef801f'} size={40} />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            navigation.navigate('ApplyNowModal', {
              item: route.params.item,
            });
          }}
          activeOpacity={0.8}
          style={styles.button}>
          <Text style={styles.buttonText}>Apply Job</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  About: {
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderTopWidth: 0.8,
    borderColor: '#d2d2d2',
  },
  fixedContent: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    position: 'absolute',
    bottom: '2%',
    left: 0,
    right: 0,
  },
  bannerContainer: {
    borderRadius: 30,
    width: width - 40,
    padding: 20,
    overflow: 'hidden',
    borderWidth: 0.5,
    elevation: 0.08,
    borderColor: '#aaa',
    justifyContent: 'center',
  },
  mainContentBox: {
    resizeMode: 'cover',
    width: width,
    minHeight: '100%',
    backgroundColor: '#fff',
  },
  backgroundImage: {
    width: width * 0.5,
    height: width * 0.5,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  ContentBox: {
    paddingVertical: '15%',
  },
  TopContent: {
    alignItems: 'center',
    marginBottom: 20,
  },
  imageMainBox: {
    width: width * 0.3,
    height: width * 0.3,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  imageContent: {
    borderWidth: 2,
    borderColor: '#ef801f',
    borderRadius: 15,
    width: 50,
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  contentBoxMiddle: {
    marginTop: 20,
    borderRadius: 20,
    borderWidth: 0.5,
    borderColor: '#aaa',
    padding: 10,
  },
  imageMainBox: {
    width: width * 0.3,
    height: width * 0.3,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
  },
  imageBox: {
    width: width * 0.2,
    height: width * 0.2,
  },
  image: {
    width: null,
    height: null,
    flex: 1,
    borderRadius: 30,
    overflow: 'hidden',
  },
  button: {
    width: width * 0.7,
    backgroundColor: '#ef801f',
    borderRadius: 15,
    alignItems: 'center',
    justifyContent: 'center',
  },
  amount: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
  },
  typeView: {
    backgroundColor: '#eee',
    paddingHorizontal: 10,
    borderRadius: 10,
  },
  amount2: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  companyName: {
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  detailBox: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  description: {
    color: '#8e8e8e',
    fontSize: RFPercentage(1.5),
    fontFamily: 'BalooPaaji2-Regular',
    textAlign: 'justify',
  },
  header: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFPercentage(1.6),
  },
  locationText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
  },
  jobHead: {
    color: '#fff',
    fontSize: RFPercentage(1.5),
    fontFamily: 'BalooPaaji2-Regular',
    textAlign: 'justify',
  },
  letterBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginVertical: 5,
    borderRadius: 10,
    borderStyle: 'dashed',
  },

  basicContent: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    width: width * 0.85,
  },
  //   tablist style
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#fff',
    borderRadius: 10,
    marginVertical: 20,
  },
  tabBox: {
    width: width * 0.4,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 15,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 16,
    color: '#fff',
  },
});
