import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import AsyncStorage from '@react-native-async-storage/async-storage';
import * as base from '../../../../Settings';
import Toast from 'react-native-simple-toast';
import moment from 'moment';
import axios from 'axios';

const {width, height} = Dimensions.get('window');
const card_width = width * 0.27;
const card_height = width * 0.3;

export default function ExperienceCv({
  jobTitle,
  setJobTitle,
  employer,
  setEmployer,
  fromDate,
  setFromDate,
  toDate,
  setToDate,
  city,
  setCity,
  coverLetter,
  setCoverLetter,
  experience,
  setExperience,
}) {
  const [show, setShow] = useState(false);
  const [drag, setDrag] = useState(false);
  const [mode, setMode] = useState('date');
  const [date, setDate] = useState(new Date());

  const ref_input2 = useRef();
  const ref_input3 = useRef();

  const [selectedBox, setSelectedBox] = useState();
  const confirmHandler = (value) => {
    setSelectedBox(value);
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate;
    // console.log(moment(currentDate).format('YYYY-MM-DD'));
    setFromDate(moment(currentDate).format('YYYY-MM-DD'));
    setShow(false);
    // console.warn(currentDate);
  };

  const showMode = (currentMode) => {
    setShow(true);
    setDrag(false);
    setMode(currentMode);
  };

  const onEndDate = (event, selectedValue) => {
    const liveDate = selectedValue;
    setToDate(moment(liveDate).format('YYYY-MM-DD'));
    setDrag(false);
  };
  const pickeMode = (currentMode) => {
    setShow(false);
    setDrag(true);
    setMode(currentMode);
  };

  const renderYearOptions = () => {
    if (jobTitle && employer && fromDate && toDate && city && coverLetter) {
      experience.push({
        job_title: jobTitle,
        company: employer,
        start_date: fromDate,
        end_date: toDate,
        place: city,
        description: coverLetter,
      });
      setJobTitle('');
      setEmployer('');
      setFromDate();
      setToDate();
      setCity('');
      setCoverLetter('');
      Toast.show(' Added Successfully  ', Toast.SHORT);
    } else {
      Toast.show('Please enter all details', Toast.SHORT);
    }
  };

  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>3</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Tell us</Text> about your
            Experience
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Job Title</Text>
            <TextInput
              value={jobTitle}
              style={styles.input}
              placeholder="Job title"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setJobTitle(val)}
              onSubmitEditing={() => ref_input2.current.focus()}
            />
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Employer </Text>
            <TextInput
              value={employer}
              style={styles.input}
              placeholder="Employer"
              autoFocus={false}
              returnKeyType={'next'}
              ref={ref_input2}
              onChangeText={(val) => setEmployer(val)}
              dataDetectorTypes="phoneNumber"
              autoCompleteType="off"
            />
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Start Date </Text>
            <View style={styles.pickerBox}>
              {show && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onChange}
                />
              )}
              {fromDate ? (
                <Text style={styles.dateStyle}>{fromDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYY-MM-DD</Text>
              )}
              <TouchableOpacity
                onPress={() => {
                  showMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>End Date </Text>
            <View style={styles.pickerBox}>
              {drag && (
                <DateTimePicker
                  testID="dateTimePicker"
                  value={date}
                  mode={'date'}
                  is24Hour={true}
                  display="default"
                  onChange={onEndDate}
                />
              )}
              {toDate ? (
                <Text style={styles.dateStyle}>{toDate}</Text>
              ) : (
                <Text style={styles.dateStyle}>YYY-MM-DD</Text>
              )}
              <TouchableOpacity
                onPress={() => {
                  pickeMode();
                }}>
                <Icon name={'calendar-edit'} color={'#2261a6'} size={25} />
              </TouchableOpacity>
            </View>
          </View>

          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>City</Text>
            <TextInput
              value={city}
              style={styles.input}
              placeholder="Enter your city"
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setCity(val)}
              onSubmitEditing={() => ref_input3.current.focus()}
            />
          </View>
          <View style={[styles.loginUser]}>
            <Text style={styles.subHeader}>Description</Text>
            <View
              style={{
                height: height * 0.11,
                backgroundColor: '#f5f6f9',
                borderRadius: 20,
                borderWidth: 1,
                borderColor: '#d8d6e2',
              }}>
              <TextInput
                value={coverLetter}
                style={styles.inputLast}
                placeholder="Explain of expereince"
                ref={ref_input3}
                returnKeyType={'next'}
                onChangeText={(val) => setCoverLetter(val)}
                multiline={true}
              />
            </View>
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            renderYearOptions();
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Experience</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 10,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  pickerBox: {
    fontSize: 15,
    width: '100%',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  inputLast: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    paddingHorizontal: 20,
  },
  loginUser1: {
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 5,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: 17,
  },
  subHeader: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
});
