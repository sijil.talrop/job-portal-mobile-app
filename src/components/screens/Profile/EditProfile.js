import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  TextInput,
  Dimensions,
  Image,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
// import {Picker} from '@react-native-picker/picker';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import Toast from 'react-native-simple-toast';
import * as base from '../../../../Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import {CommonActions, useNavigation} from '@react-navigation/native';
const {width, height} = Dimensions.get('window');

export default function EditProfile(props) {
  const navigation = useNavigation();
  const [first_name, setFirst_name] = useState('');
  const [last_name, setLast_name] = useState('');
  const [phone, setPhone] = useState('');
  const [re_enter_password, setRe_enter_password] = useState('password');
  const ref_input2 = useRef();
  const ref_input3 = useRef();
  const [country, setCountry] = useState('');
  const [state, setState] = useState('');
  const [city, setCity] = useState('');
  const [location, setLocation] = useState('');
  const [category, setCategory] = useState('');
  const [country_list, setCountry_list] = useState();
  const [state_list, setState_list] = useState();
  const [city_list, setCity_list] = useState();
  const [location_list, setLocation_list] = useState();
  const [category_list, setCategory_list] = useState();
  const [image_data, setImageData] = useState();
  const [image_response, setImageResponse] = useState();
  const [profile, setProfile] = useState([]);
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    // get_country_list();
    // get_state_list();
    // get_city_list();
    // get_location_list();
    // get_category_list();
    _get_profile();
  }, [500]);

  const get_country_list = () => {
    let post_url = base.BASE_URL + 'cities/counties/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCountry_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_state_list = () => {
    let post_url = base.BASE_URL + 'cities/states/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setState_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_city_list = () => {
    let post_url = base.BASE_URL + 'cities/cities/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCity_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const get_location_list = () => {
    let post_url = base.BASE_URL + 'cities/locations/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setLocation_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  const get_category_list = () => {
    let post_url = base.BASE_URL + 'jobs/job-categories/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setCategory_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const registration = () => {
    let post_url = base.BASE_URL + 'users/register/';
    console.log(post_url);
    axios
      .post(post_url, {
        phone: phone,
        first_name: first_name,
        last_name: last_name,
        country: country,
        state: state,
        city: city,
        location: location,
        category: category,
        user_type: 'qSi2NQRUWKWmmIwxgkRMuOUgcEQ5gfkr',
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setTimeout(() => {
            navigation.navigate('SuccessPopup');
          }, 500);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  let _get_profile = async () => {
    let get_url = base.BASE_URL + 'users/profile/';
    console.log(get_url);
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data, message} = response.data;
        if (StatusCode == 6000) {
          setProfile(data.data);
        } else {
          setProfile([]);
        }
      })
      .catch((error) => {});
  };

  const ImageUpload = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
      },
      (response) => {
        console.log();
        if (!response['didCancel']) {
          console.log(response);
          setImageResponse(response.uri);
          setImageData({
            uri: response.uri,
            name: response.fileName,
            filename: response.fileName,
            type: 'image/png',
          });
          console.log(response.uri);
        }
      },
    );
  };

  const uploadImage = async () => {
    let post_url = base.BASE_URL + 'users/update-profile-image/';
    console.log(post_url);
    let user = await AsyncStorage.getItem('user');
    let user_instance = JSON.parse(user);
    let access_token = user_instance['access_token'];
    let body = new FormData();

    body.append('image', image_data);

    let res = await fetch(post_url, {
      method: 'post',
      body: body,
      headers: {
        'Content-Type': 'multipart/form-data; ',
        Authorization: 'Bearer ' + access_token,
      },
    });
    let responseJson = await res.json();

    if (responseJson.StatusCode == 6000) {
      setTimeout(() => {
        // setLoading(false);
        Toast.show('Your profile edited successfully', Toast.SHORT);
        // navigation.navigate('My Account');
      }, 500);
    } else {
      Toast.show(responseJson.message, Toast.SHORT);
      setTimeout(() => {
        // setLoading(false);
      }, 500);
    }
  };
  // #***post datas using fetch***#

  return (
    <View style={styles.middleContent}>
      <View
        style={{
          backgroundColor: 'transparent',
          alignItems: 'center',
          paddingTop: height * 0.09,
        }}>
        <TouchableOpacity
          onPress={() => {
            // selectFile();
            // uploadImage();
          }}
          onPress={ImageUpload}
          activeOpacity={0.8}
          style={styles.imageContainer}>
          {image_response != '' ? (
            <Image
              source={{
                uri: image_response,
              }}
              style={styles.image}
            />
          ) : (
            <Image style={styles.image} source={{uri: profile.photo}} />
          )}
        </TouchableOpacity>
      </View>
      <TouchableOpacity
        onPress={() => {
          // editProfile();
          uploadImage();
        }}
        style={styles.login}>
        <Text style={styles.buttonText}>Continue</Text>
      </TouchableOpacity>
      {/* <View style={styles.loginUser}>
        <TextInput
          value={first_name}
          style={styles.input}
          placeholder="First Name"
          autoFocus={true}
          returnKeyType={'next'}
          onChangeText={(val) => setFirst_name(val)}
          onSubmitEditing={() => ref_input2.current.focus()}
          // secureTextEntry={true}
        />
        <Icon name={'account'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={last_name}
          style={styles.input}
          placeholder="Last Name"
          ref={ref_input2}
          returnKeyType={'next'}
          onChangeText={(val) => setLast_name(val)}
          onSubmitEditing={() => ref_input3.current.focus()}
        />
        <Icon name={'account'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser}>
        <TextInput
          value={phone}
          style={styles.input}
          placeholder="Phone Number"
          ref={ref_input3}
          returnKeyType={'next'}
          onChangeText={(val) => setPhone(val)}
        />
        <Icon name={'gmail'} color={'#000'} size={20} />
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={country}`
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setCountry(itemValue)}>
          {country_list &&
            country_list.length > 0 &&
            country_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={state}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setState(itemValue)}>
          {state_list &&
            state_list.length > 0 &&
            state_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={city}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setCity(itemValue)}>
          {city_list &&
            city_list.length > 0 &&
            city_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={location}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setLocation(itemValue)}>
          {location_list &&
            location_list.length > 0 &&
            location_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <View style={styles.loginUser1}>
        <Picker
          selectedValue={category}
          style={styles.picker}
          onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
          {category_list &&
            category_list.length > 0 &&
            category_list.map((item, index) => (
              <Picker.Item key={index} label={item.label} value={item.value} />
            ))}
        </Picker>
      </View>
      <TouchableOpacity
        onPress={() => {
          registration();
        }}
        style={styles.login}>
        <Text style={styles.buttonText}>Continue</Text>
      </TouchableOpacity> */}
    </View>
  );
}

const styles = StyleSheet.create({
  Scroll: {
    backgroundColor: 'red',
  },
  middleContent: {
    marginTop: width * 0.04,
    width: width * 0.9,
  },
  inputContent: {
    paddingVertical: height * 0.01,
  },
  input: {
    fontSize: 15,
    width: width * 0.7,
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  loginUser: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingHorizontal: 10,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 15,
    marginVertical: 10,
  },
  picker: {
    fontSize: 10,
    color: '#aaa',
  },
  login: {
    backgroundColor: '#ef801f',
    borderRadius: 15,
    paddingVertical: 15,
    marginTop: height * 0.05,
  },
  buttonText: {
    fontSize: 15,
    color: '#fff',
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  imageContainer: {
    width: width * 0.3,
    height: width * 0.3,
    borderRadius: 100,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: '#fad4b2',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    borderRadius: 100,
    overflow: 'hidden',
  },
});
