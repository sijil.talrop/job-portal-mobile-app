import React, {useState, useEffect, useRef, useContext} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Dimensions,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import * as base from '../../../../Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import Toast from 'react-native-simple-toast';
import axios from 'axios';
import {Picker} from '@react-native-picker/picker';
const {width, height} = Dimensions.get('window');
import {Context} from '../../contexts/Store';

export default function SocialMedia() {
  const {state, dispatch} = useContext(Context);

  const [link, setLink] = useState();
  const [socialmedia, setSocialMedia] = useState();
  const [titleList, setTitleList] = useState();
  const [push, setPush] = useState([]);
  const [required, setRequired] = useState(false);

  const [isFocused, setIsFocused] = useState({
    link_box: false,
  });

  useEffect(() => {
    get_title_list();
  }, []);

  const handleInputFocus = (textinput) => {
    setIsFocused({
      [textinput]: true,
    });
  };
  const handleInputBlur = (textinput) => {
    setIsFocused({
      [textinput]: false,
    });
  };

  const get_title_list = () => {
    let post_url = base.BASE_URL + 'users/social-media-title/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setTitleList(data.data);
        } else {
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  const registration = async () => {
    let user = await AsyncStorage.getItem('user');
    let access_token = state.user_data.access_token;
    let post_url = base.BASE_URL + 'users/update-social-media/';
    axios
      .post(post_url, push, {
        headers: {
          Authorization: 'Bearer ' + access_token,
          'Content-Type': 'application/json; ',
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          Toast.show(data.message, Toast.SHORT);
          setRequired(true);
        } else {
          Toast.show(data.message, Toast.SHORT);
          setPush([]);
        }
      })
      .catch((error) => {
        console.warn(error.response);
      });
  };

  return (
    <View>
      <View>
        <View style={styles.basicDetailBox}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>6</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Social</Text> Media
          </Text>
        </View>
        <View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Social Media</Text>
            <View style={styles.loginUser1}>
              <Picker
                selectedValue={socialmedia}
                style={{color: '#aaa'}}
                onValueChange={(itemValue, itemIndex) =>
                  setSocialMedia(itemValue)
                }>
                {titleList &&
                  titleList.length > 0 &&
                  titleList.map((item, index) => (
                    <Picker.Item
                      key={index}
                      label={item.label}
                      value={item.value}
                    />
                  ))}
              </Picker>
            </View>
          </View>
          <View style={styles.loginUser}>
            <Text style={styles.subHeader}>Link</Text>
            <TextInput
              value={link}
              style={[
                styles.input,
                {borderColor: isFocused.link_box ? '#c3c4c7' : '#ddd'},
              ]}
              placeholder="Link"
              placeholderTextColor={isFocused.link_box ? '#000' : '#aaa'}
              autoFocus={false}
              returnKeyType={'next'}
              onChangeText={(val) => setLink(val)}
              onFocus={() => handleInputFocus('link_box')}
              onBlur={() => handleInputBlur('link_box')}
            />
          </View>
        </View>
        <TouchableOpacity
          onPress={() => {
            if (socialmedia && link) {
              push.push({
                title: socialmedia,
                url: link,
              });
              setSocialMedia();
              setLink();
              Toast.show(' Added Successfully  ', Toast.SHORT);
            } else {
              Toast.show('Please enter all details', Toast.SHORT);
            }
          }}
          style={styles.button}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText}>Add Social Media</Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() => {
            registration();
          }}
          style={styles.button2}>
          <Icon name={'plus'} color={'#ef803a'} size={20} />
          <Text style={styles.uploadText2}>Save</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 14,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 14,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  input2: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    backgroundColor: '#f5f6f9',
    width: '70%',
  },
  loginUser1: {
    borderRadius: 20,
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: '#f5f6f9',
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  subHeader: {
    fontSize: RFPercentage(1.6),
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  button2: {
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 15,
    backgroundColor: '#ef803a',
  },
  uploadText2: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  button: {
    borderStyle: 'dashed',
    borderWidth: 1,
    borderColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#ef803a',
    marginLeft: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
});
