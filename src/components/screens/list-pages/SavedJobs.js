import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ImageBackground,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {
  CommonActions,
  useNavigation,
  useIsFocused,
} from '@react-navigation/native';
import {
  FacebookLoader,
  InstagramLoader,
} from 'react-native-easy-content-loader';
import BasicHeading from '../header/BasicHeading';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.31;
const card_width = width * 0.6;
const box_width = width - 40;
import axios from 'axios';
import * as base from '../../../../Settings';
import {Context} from '../../contexts/Store';

export default function SavedJobs() {
  const navigation = useNavigation();
  const isfocused = useIsFocused();
  const [saved_job, setSaved_job] = useState([]);
  const [isLoading, setLoading] = useState(true);
  const {state, dispatch} = useContext(Context);
  const [selectedRating, setSelectedRating] = useState('Facebook');
  const fadeAnim = useRef(new Animated.Value(0)).current;
  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      useNativeDriver: true,
      toValue: 1,
      duration: 5000,
    }).start();
  };
  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  let _get_saved_jobs = async () => {
    let get_url = base.BASE_URL + 'jobs/saved-jobs/';
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setSaved_job(data.data);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        } else {
          setSaved_job([]);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  useEffect(() => {
    if (isfocused) {
      _get_saved_jobs();
      fadeIn();
    }
  }, [isfocused]);

  const Needs = () => {
    return (
      saved_job &&
      saved_job.length > 0 &&
      saved_job.map((item, pk) => (
        <InstagramLoader
          key={item.pk}
          active
          tHeight={5}
          tWidth="95%"
          sTWidth="95%"
          sTHeight={5}
          loading={isLoading}
          primaryColor="#d2d2d2"
          secondaryColor="#d2d2d2"
          imageStyles={{width: box_width, height: 100}}>
          <TouchableOpacity
            onPress={() => {
              TouchableHandler(item.pk);
              navigation.navigate('CompanyJobSinglePage', {
                item: item,
              });
            }}
            activeOpacity={0.9}
            style={[
              styles.mainContainer,
              {
                backgroundColor: item.pk == selectedRating ? '#2261a6' : '#fff',
              },
            ]}>
            <View style={styles.left}>
              <View
                style={[
                  styles.lefContent,
                  {
                    backgroundColor:
                      item.pk == selectedRating ? '#fff' : '#e2e2e2',
                  },
                ]}>
                <View style={styles.imageContainer}>
                  <Image
                    source={{uri: item.logo}}
                    style={styles.productImage}
                  />
                </View>
              </View>
            </View>
            <View style={styles.contentContairener}>
              <Text
                style={[
                  styles.introName,
                  {color: item.pk == selectedRating ? '#fff' : '#adabbb'},
                ]}>
                {item.business.length > 17
                  ? item.business.substring(0, 20) + '...'
                  : item.business}
              </Text>
              <Text
                style={[
                  styles.name,
                  {color: item.pk == selectedRating ? '#fff' : '#130937'},
                ]}>
                {item.name.length > 17
                  ? item.name.substring(0, 20) + '...'
                  : item.name}
              </Text>
              <View style={styles.Location}>
                <Text
                  style={[
                    styles.amount,
                    {
                      color: item.pk == selectedRating ? '#fff' : '#130937',
                    },
                  ]}>
                  {item.salary_minimum}-{item.salary_maximum}k/m
                </Text>
                <View style={styles.locationCharge}>
                  <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
                  <Text
                    style={[
                      styles.place,
                      {
                        color: item.pk == selectedRating ? '#fff' : '#130937',
                      },
                    ]}>
                    {item.location.length > 17
                      ? item.location.substring(0, 20) + '...'
                      : item.location}
                  </Text>
                </View>
              </View>
            </View>
            <View style={styles.right}>
              <View style={styles.rateContainer}>
                <Icon
                  name={'bookmark'}
                  color={item.kind == 'Featured' ? '#eee' : '#EF803A'}
                  size={50}
                />
              </View>
              <View style={styles.delivery}>
                <Text
                  style={[
                    styles.bottomButtonText,
                    {
                      color: item.pk == selectedRating ? '#fff' : '#aaa',
                    },
                  ]}>
                  {item.date_added}
                </Text>
              </View>
            </View>
          </TouchableOpacity>
        </InstagramLoader>
      ))
    );
  };

  return (
    <View style={{backgroundColor: '#fff'}}>
      <ImageBackground
        source={require('../../../assets/vector-images/Group20260.png')}
        style={styles.backgroundImage}
      />
      <View
        style={{
          paddingHorizontal: 15,
          paddingVertical: 30,
        }}>
        <BasicHeading title="Saved Jobs" />
      </View>
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          alignItems: 'center',
          paddingVertical: height * 0.01,
          minHeight: '100%',
        }}>
        {!isNaN(saved_job) != '' && (
          <Animated.View
            style={[
              {
                opacity: fadeAnim,
              },
            ]}>
            <Image
              style={styles.foundImage}
              source={require('../../../assets/vector-images/noresultfound.png')}
            />
            <Text style={styles.messageText}>No Jobs Found</Text>
          </Animated.View>
        )}

        {Needs()}
        <View style={{height: height * 0.4}}></View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 12,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#eee',
    elevation: 1,
  },
  left: {
    flexDirection: 'row',
    alignItems: 'center',
    width: box_width * 0.16,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.55,
  },
  lefContent: {
    height: Reward_Box_Height * 0.4,
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 30,
  },
  Location: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    textTransform: 'capitalize',
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    textTransform: 'capitalize',
    color: '#000',
  },
  amount: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    textTransform: 'capitalize',
    color: '#393057',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.22,
    height: Reward_Box_Height,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.7,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 13,
    fontFamily: 'BalooPaaji2-Regular',
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
  },
  backgroundImage: {
    width: width * 0.4,
    height: width * 0.35,
    resizeMode: 'contain',
    position: 'absolute',
    top: 0,
    right: 0,
  },
  foundImage: {
    width: width * 0.5,
    height: width * 0.5,
    resizeMode: 'contain',
  },
  messageText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'BalooPaaji2-Regular',
  },
});
