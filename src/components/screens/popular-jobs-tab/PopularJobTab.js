import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import BasicHeading from '../header/BasicHeading';
import CommonSearchBar from '../home/CommonSearchBar';
import MostRelevant from './MostRelevant';
import MostRecent from './MostRecent';
import {useNavigation} from '@react-navigation/native';
const {height, width} = Dimensions.get('window');

export default function PopularJobTab({route}) {
  const navigation = useNavigation();
  const Tab = [
    {
      Box: 'Most Relevant',
    },
    {
      Box: 'Most Recent',
    },
  ];

  const [selectedBox, setSelectedBox] = useState('Most Relevant');
  const confirmHandler = (value) => {
    setSelectedBox(value);
  };

  const renderTabList = () => {
    if (selectedBox == 'Most Relevant') {
      return <MostRelevant />;
    } else if (selectedBox == 'Most Recent') {
      return <MostRecent />;
    }
  };

  const renderTab = () => {
    return (
      <View style={{flex: 1, minHeight: '100%'}}>
        <Text style={styles.filterHeader}>
          {route.params.total} Job Opportunity
        </Text>
        <View style={styles.tabView}>
          {Tab.map((item, index) => (
            <TouchableOpacity
              key={index}
              onPress={() => {
                confirmHandler(item.Box);
              }}
              style={[
                styles.tabBox,
                {
                  backgroundColor:
                    item.Box === selectedBox ? '#2261a6' : '#ecf1f9',
                },
                {borderColor: item.Box === selectedBox ? '#ddd' : '#b0c4e4'},
              ]}>
              <Text
                style={[
                  styles.buttonText,
                  {color: item.Box == selectedBox ? '#fff' : '#000'},
                ]}>
                {item.Box}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.contentScroll}>
          {renderTabList()}
          <View style={{height: 300}} />
        </ScrollView>
      </View>
    );
  };

  return (
    <View style={styles.backgroundImageBox}>
      <ImageBackground
        style={styles.background}
        source={require('../../../assets/vector-images/Group20260.png')}
      />
      <View style={{flex: 1}}>
        <View style={{marginVertical: 20, paddingHorizontal: 15}}>
          <BasicHeading title="Popular Jobs" />
        </View>
        <View style={{marginVertical: 20}}>
          <CommonSearchBar />
        </View>
        <View style={{}}>{renderTab()}</View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  backgroundImageBox: {
    width: width,
    minHeight: '100%',
  },
  background: {
    resizeMode: 'contain',
    width: width * 0.5,
    height: width * 0.5,
    position: 'absolute',
    top: 0,
    right: 0,
  },
  contentScroll: {},
  filterHeader: {
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
    fontFamily: 'BalooPaaji2-SemiBold',
    paddingHorizontal: 20,
  },
  header: {
    color: '#000',
    textAlign: 'center',
    fontFamily: 'Roboto-Bold',
    fontSize: 20,
  },
  tabView: {
    flexDirection: 'row',
    backgroundColor: 'transparent',
    paddingVertical: 5,
    paddingHorizontal: 20,
    borderRadius: 10,
    marginVertical: 20,
  },
  tabBox: {
    width: width * 0.3,
    alignItems: 'center',
    borderRadius: 10,
    paddingVertical: 5,
    borderWidth: 1,
    marginRight: 20,
    elevation: 0.7,
  },
  iconBox: {
    paddingTop: 20,
    width: width - 30,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: RFPercentage(1.6),
  },
});
