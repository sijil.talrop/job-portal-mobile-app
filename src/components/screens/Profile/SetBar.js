import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  ScrollView,
  View,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import axios from 'axios';

import * as base from '../../../../Settings';

export default function App(props) {
  const [location, setLocation] = useState();
  const [location_list, setLocation_list] = useState([]);

  useEffect(() => {
    get_location_list();
  }, []);
  const get_location_list = () => {
    let post_url = base.BASE_URL + 'cities/locations/';
    axios
      .get(post_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setLocation_list(data.data);
        } else {
          Toast.show(data.message, Toast.SHORT);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };
  const [colors] = useState(['#84DCC6', '#FEC8C8', '#F7E4CF', '#E8DEF3']);
  const [filtered, setFiltered] = useState(location_list);
  const [searching, setSearching] = useState(false);
  const [select, setSelect] = useState();
  const onSearch = (text) => {
    if (text) {
      setSearching(true);
      const temp = text.toLowerCase();

      const tempList = location_list.filter((item) => {
        if (item.label.match(temp)) return item;
      });
      setFiltered(tempList);
    } else {
      setSearching(false);
      setFiltered(location_list);
    }
  };

  const confirmLocation = (value) => {
    props.setLocation(value);
  };
  const randomColor = () => {
    return colors[Math.floor(Math.random() * colors.length)];
  };
  return (
    <ScrollView contentContainerStyle={styles.container1}>
      <TextInput
        style={styles.textInput}
        placeholder="Search"
        placeholderTextColor="#000"
        onChangeText={onSearch}
      />
      {!searching && (
        <View
          style={{
            justifyContent: 'center',
            alignContent: 'center',
            alignItems: 'center',
          }}>
          <View style={styles.renderContentBox}>
            {location_list &&
              location_list.length > 0 &&
              location_list.map((item, index) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      confirmLocation(item.value, item.label);
                      props.toggleModal();
                      props.setLabel(item.label);
                    }}
                    style={styles.itemView}>
                    <Text style={styles.itemText}>{item.label}</Text>
                  </TouchableOpacity>
                );
              })}
          </View>
        </View>
      )}
      {searching && (
        <TouchableOpacity
          onPress={() => setSearching(false)}
          style={styles.container}>
          <View style={styles.subContainer}>
            {filtered.length ? (
              filtered.map((item) => {
                return (
                  <TouchableOpacity
                    onPress={() => {
                      confirmLocation(item.value);
                      props.toggleModal();
                      props.setLabel(item.label);
                    }}
                    style={styles.itemView}>
                    <Text style={styles.itemText}>{item.label}</Text>
                  </TouchableOpacity>
                );
              })
            ) : (
              <View style={styles.noResultView}>
                <Text style={styles.noResultText}>No search items matched</Text>
              </View>
            )}
          </View>
        </TouchableOpacity>
      )}
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container1: {
    alignItems: 'center',
    marginTop: '10%',
    minHeight: '100%',
  },
  textInput: {
    backgroundColor: '#fff',
    width: '100%',
    borderRadius: 50,
    fontSize: 17,
    paddingHorizontal: 20,
    fontFamily: 'BalooPaaji2-Regular',
  },
  container: {
    width: '100%',
  },
  subContainer: {
    // backgroundColor: '#aaa',
    paddingTop: 10,
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    flexWrap: 'wrap',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  itemView: {
    width: '100%',
    marginBottom: 10,
    paddingVertical: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    borderWidth: 1,
    borderColor: '#aaa',
  },
  renderContentBox: {
    flexWrap: 'wrap',
    flexDirection: 'row',
    justifyContent: 'center',
    marginTop: '5%',
  },
  itemText: {
    color: '#fff',
    paddingHorizontal: 10,
    fontFamily: 'BalooPaaji2-Regular',
  },
  noResultView: {
    alignSelf: 'center',
    height: 100,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    alignContent: 'center',
  },
  noResultText: {
    fontSize: 18,
    fontWeight: 'bold',
    color: 'white',
  },
});
