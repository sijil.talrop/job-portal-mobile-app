import React, {useState, useEffect, useRef, useContext} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {launchCamera, launchImageLibrary} from 'react-native-image-picker';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';

import AboutEducation from './AboutEducation';
import AboutExperience from './AboutExperience';
import AboutSkill from './AboutSkill';
import AboutAwards from './AboutAwards';
import SocialMedia from './SocialMedia';
import BasicInformation from './BasicInformation';
import * as base from '../../../../Settings';
import axios from 'axios';

import Toast from 'react-native-simple-toast';
import {Context} from '../../contexts/Store';
const {width, height} = Dimensions.get('window');
export default function CreateProfile({route}) {
  const {state, dispatch} = useContext(Context);
  const navigation = useNavigation();
  const [scroll, setScroll] = useState(false);

  const handleScroll = () => {
    setTimeout(() => {
      setScroll(!scroll);
    }, 1000);
  };

  const [image_data, setImageData] = useState('');
  const [image_response, setImageResponse] = useState('');
  const [name, setName] = useState('');
  const [button, setButton] = useState(false);

  const uploadImage = () => {
    launchImageLibrary(
      {
        mediaType: 'photo',
        includeBase64: false,
      },
      (response) => {
        console.log(response);
        if (!response['didCancel']) {
          console.log(response);
          setImageResponse(response.uri);
          setImageData({
            uri: response.uri,
            name: response.fileName,
            filename: response.fileName,
            type: 'image/png',
          });
          setButton(true);
          console.log(response.uri);
        }
      },
    );
  };
  const EditProfile = () => {
    let post_url = base.BASE_URL + 'users/update-profile-image/';
    let formData = new FormData();
    formData.append('name', name);
    let access_token = state.user_data.access_token;

    if (image_response != '') {
      formData.append('image', image_data);
    }
    axios
      .post(post_url, formData, {
        headers: {
          Authorization: `Bearer ${access_token}`,
          'Content-Type': 'multipart/form-data; ',
        },
      })
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode === 6000) {
          setTimeout(() => {
            Toast.show(response.data.data.message, Toast.SHORT);
          }, 500);
        } else {
          setTimeout(() => {
            Toast.show(response.data.message, Toast.SHORT);
          }, 500);
        }
      })
      .catch((error) => {
        setTimeout(() => {
          console.log(error);
        }, 500);
      });
  };

  // const updateProfile = async () => {
  //   let post_url = base.BASE_URL + 'users/update-profile-image/';
  //   let access_token = state.user_data.access_token;
  //   let body = new FormData();
  //   body.append('image', image_data);
  //   body.append('name', name);

  //   let res = await fetch(post_url, {
  //     method: 'post',
  //     body: body,
  //     headers: {
  //       'Content-Type': 'multipart/form-data; ',
  //       Authorization: 'Bearer ' + access_token,
  //     },
  //   });
  //   let responseJson = await res.json();

  //   if (responseJson.StatusCode == 6000) {
  //     Toast.show('Your profile Updated successfully', Toast.SHORT);
  //     console.warn('create');
  //     let data = responseJson.data;
  //     setButton(false);
  //   } else {
  //     console.log(responseJson.message);
  //   }
  // };

  const Companies = () => {
    return (
      <>
        <View style={[styles.basicDetailBox, {marginBottom: 20}]}>
          <View style={styles.dots}>
            <Text style={styles.dotText}>7</Text>
          </View>
          <Text style={styles.countText}>
            <Text style={{color: '#0462bf'}}>Companies</Text> Followed By
          </Text>
        </View>
        <ScrollView
          pagingEnabled
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.scrollStyle}
          onScroll={''}>
          {route.params.follower &&
            route.params.follower.length > 0 &&
            route.params.follower.map((item, index) => (
              <TouchableOpacity
                key={index}
                style={{marginHorizontal: width * 0.02}}>
                <View style={styles.companyImage}>
                  <Image style={styles.image1} source={{uri: item.logo}} />
                </View>
                <View
                  style={{
                    width: width * 0.32,
                    height: width * 0.13,
                  }}>
                  <Text style={styles.companyText}>
                    {item.business.length > 17
                      ? item.business.substring(0, 17) + '...'
                      : item.business}
                  </Text>
                </View>
              </TouchableOpacity>
            ))}
          <TouchableOpacity style={{width: width * 0.3}}></TouchableOpacity>
        </ScrollView>
      </>
    );
  };

  return (
    <ScrollView
      onScrollBeginDrag={() => handleScroll()}
      onScrollEndDrag={() => handleScroll()}
      style={{}}
      contentContainerStyle={styles.mainContainer}>
      <View>
        <View style={styles.headerBox}>
          <TouchableOpacity
            onPress={() => {
              navigation.goBack();
            }}
            style={styles.leftIcon}>
            <Icon name={'chevron-left'} color={'#000'} size={25} />
          </TouchableOpacity>
          <Text style={styles.headerText}>Generate CV</Text>
          <Text style={styles.headerText}>10/7</Text>
        </View>
        <View
          style={{
            alignItems: 'center',
            marginVertical: height * 0.05,
          }}>
          <TouchableOpacity style={styles.imageContainer}>
            {image_response == '' ? (
              <Image
                style={styles.image}
                source={{uri: route.params.image}}
                style={styles.image}
              />
            ) : (
              <Image source={{uri: image_response}} style={styles.image} />
            )}
          </TouchableOpacity>
          {button == true ? (
            <TouchableOpacity
              style={styles.buttonBox2}
              onPress={() => {
                EditProfile();
                setButton(false);
              }}>
              <Text style={styles.uploadButtonText}>Save</Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={[
                styles.buttonBox,
                {
                  backgroundColor: '#fff',
                  position: 'absolute',
                  right: '35%',
                  top: '67%',
                },
              ]}
              onPress={uploadImage}>
              <Icon name={'upload'} color={'#2261a6'} size={20} />
            </TouchableOpacity>
          )}
        </View>

        <View style={{marginBottom: height * 0.06}}>
          <BasicInformation />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <AboutEducation />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <AboutExperience />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <AboutSkill />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <AboutAwards />
        </View>
        <View style={{marginBottom: height * 0.06}}>
          <SocialMedia />
        </View>
        {isNaN(route.params.follower) != '' && <View>{Companies()}</View>}

        {!scroll && (
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              position: 'absolute',
              bottom: 10,
              right: 0,
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.navigate('Home');
              }}
              activeOpacity={0.8}
              style={[styles.bottomButton]}>
              <Icon name={'home-heart'} color={'#2261a6'} size={40} />
            </TouchableOpacity>
          </View>
        )}
        {scroll && (
          <View
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              position: 'absolute',
              bottom: 10,
              left: 0,
            }}>
            <TouchableOpacity
              onPress={() => {
                navigation.goBack();
              }}
              activeOpacity={0.8}
              style={[styles.bottomButton]}>
              <Icon name={'step-backward'} color={'#2261a6'} size={40} />
            </TouchableOpacity>
          </View>
        )}
        <View style={{height: height * 0.05}} />
      </View>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    minHeight: '100%',
    paddingHorizontal: 20,
    paddingVertical: '10%',
    backgroundColor: '#fff',
  },
  headerBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  leftIcon: {
    width: 30,
    height: 30,
    borderRadius: 10,
    backgroundColor: '#dbdae4',
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 18,
  },
  imageContainer: {
    width: width * 0.3,
    height: width * 0.3,
    borderRadius: 100,
    overflow: 'hidden',
    borderWidth: 3,
    borderColor: '#ddd',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
    borderRadius: 100,
    overflow: 'hidden',
  },
  basicDetailBox: {
    flexDirection: 'row',
    marginBottom: 15,
  },
  dots: {
    width: 30,
    height: 30,
    backgroundColor: '#0462bf',
    borderRadius: 30,
    marginRight: 20,
    justifyContent: 'center',
    elevation: 2,
  },
  dotText: {
    textAlign: 'center',
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#fff',
    fontSize: 14,
  },
  loginUser: {
    justifyContent: 'space-between',
    marginVertical: 10,
  },
  input: {
    fontSize: 15,
    width: '100%',
    fontFamily: 'BalooPaaji2-Regular',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderWidth: 1,
  },
  pickerBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 15,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  inputLast: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    paddingHorizontal: 20,
  },
  tabView: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    backgroundColor: '#f5f6f9',
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 20,
    marginVertical: 20,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  tabBox: {
    width: width * 0.25,
    alignItems: 'center',
    borderRadius: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingVertical: 5,
  },
  buttonText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
  loginUser1: {
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    marginVertical: 10,
    paddingVertical: 5,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#d8d6e2',
  },
  countText: {
    fontFamily: 'BalooPaaji2-SemiBold',
    color: '#262753',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  subHeader: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-SemiBold',
    marginBottom: 10,
    color: '#262753',
  },
  scrollStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageContent: {
    width: width * 0.4,
    height: width * 0.3,
    backgroundColor: '#fff',
  },
  companyImage: {
    width: width * 0.3,
    height: width * 0.25,
    borderWidth: 1,
    borderColor: '#aaa',
    borderRadius: 15,
  },
  image1: {
    width: null,
    height: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 10,
  },
  bottomButton: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 100,
    marginTop: 20,
    width: width * 0.2,
    height: width * 0.1,
    borderWidth: 1,
    elevation: 0.1,
    borderColor: '#0462bf',
  },
  bottomText: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    color: '#fff',
  },
  datePickerStyle: {
    width: '100%',
    marginTop: 20,
    backgroundColor: '#f5f6f9',
    borderRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 12,
    borderWidth: 1,
    borderColor: '#d8d6e2',
    alignContent: 'flex-start',
    justifyContent: 'flex-start',
  },
  button: {
    backgroundColor: '#ef803a',
    borderRadius: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginTop: 10,
  },
  uploadText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-Regular',
  },
  buttonBox: {
    padding: 5,
    backgroundColor: '#ef803a',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 30,
    borderWidth: 0.5,
    borderColor: '#2261a6',
    // marginTop: 10,
  },
  buttonBox2: {
    backgroundColor: '#ef803a',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    width: 100,
    marginTop: 10,
    paddingVertical: 5,
  },
  uploadButtonText: {
    color: '#fff',
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 12,
  },
  companyText: {
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
    marginTop: 5,
  },
});
