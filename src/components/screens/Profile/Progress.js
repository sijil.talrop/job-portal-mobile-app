import React, {useState, useEffect} from 'react';
import {View, StyleSheet, Text, TouchableOpacity} from 'react-native';

export default function Progress(props) {
  const step = 1;
  const interval = 70;
  const [progressPercentage, setProgressPercentage] = useState(0);

  const updateProgress = () => setProgressPercentage(progressPercentage + step);
  if (progressPercentage < props.percentage) {
    setTimeout(updateProgress, interval);
  }

  return (
    <View style={{paddingHorizontal: 20, paddingVertical: 10}}>
      <TouchableOpacity style={styles.progressBox}>
        <View style={styles.percentage}>
          <Text style={styles.content}>{props.title}</Text>
          <Text style={styles.content}>{props.percentage}%</Text>
        </View>
        <View style={styles.banner}>
          <View
            style={{
              width: progressPercentage + '%',
              backgroundColor: '#ef801f',
              height: 10,
              borderRadius: 30,
            }}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  progressBox: {},
  percentage: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  banner: {
    backgroundColor: '#dedede',
    height: 10,
    borderRadius: 30,
    marginTop: 5,
  },
  content: {
    color: '#797979',
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 15,
  },
});
