import React, {useState, useEffect, useRef, useContext} from 'react';
import {StyleSheet, Text, View, Image, Dimensions, Alert} from 'react-native';
import {
  DrawerContentScrollView,
  DrawerItem,
  DrawerItemList,
} from '@react-navigation/drawer';

import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/AntDesign';
import axios from 'axios';
import * as base from '../../../Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Context} from '../contexts/Store';
import {TouchableOpacity} from 'react-native';

Icon.loadFont();
const {width, height} = Dimensions.get('window');

export function DrawerContent(props) {
  const [photo, setPhoto] = useState('');
  const [firstname, setFirstname] = useState('');
  const [lastname, setLastName] = useState('');
  const [selectedRating, setSelectedRating] = useState();
  const {state, dispatch} = useContext(Context);

  const DrawerContent = [
    {
      navigation: 'ProfileStackNavigator',
      label: 'Profile',
    },
    {
      navigation: 'HomeStackNavigator',
      label: 'Home',
    },
    {
      navigation: 'AppliedJobs',
      label: 'Applied Jobs',
    },
    {
      navigation: 'MatchedJobs',
      label: 'Matched Jobs',
    },
    {
      navigation: 'SavedJobs',
      label: 'Saved Jobs',
    },
    {
      navigation: 'NotificationStackNavigator',
      label: 'Notifications',
    },
    {
      navigation: 'CvStackNavigator',
      label: 'Cv',
    },
  ];

  const confirmHandler = (value) => {
    setSelectedRating(value);
  };

  useEffect(() => {
    if (isNaN(DrawerContent)) {
      setSelectedRating(DrawerContent[1].navigation);
    }
    _get_profile();
  }, []);

  useEffect(() => {
    if (state.user_data.profile_data) {
      setPhoto(state.user_data.profile_data.photo);
      setFirstname(state.user_data.profile_data.first_name);
      setLastName(state.user_data.profile_data.last_name);
    }
  }, [state.user_data]);

  let _get_profile = async () => {
    let get_url = base.BASE_URL + 'users/profile/';
    let access_token = state.user_data.access_token;
    axios
      .get(get_url, {
        headers: {
          Authorization: 'Bearer ' + access_token,
        },
      })
      .then((response) => {
        let {StatusCode, data, message} = response.data;
        if (StatusCode == 6000) {
          let profile_data = data.data;
          dispatch({
            type: 'UPDATE_USER_DATA',
            user_data: {
              ...state.user_data.profile_data,
              profile_data: profile_data,
            },
          });
        } else {
        }
      })
      .catch((error) => {
        console.warn(error.response);
        if (error.response.status == 401) {
          dispatch({
            type: 'UPDATE_USER_DATA',
            user_data: {
              is_verified: false,
            },
          });
        }
      });
  };

  const logOut = () => {
    Alert.alert('Log Out', 'Are you sure want to log out', [
      {
        text: 'No',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Yes',
        onPress: () => {
          _signOutAsync();
        },
      },
    ]);
  };

  const _signOutAsync = async () => {
    setTimeout(() => {
      dispatch({
        type: 'UPDATE_USER_DATA',
        user_data: {
          ...state.user_data,
          is_verified: false,
        },
      });
    }, 500);
    await AsyncStorage.clear();
  };

  return (
    <View style={styles.mainContainer}>
      <DrawerContentScrollView
        {...props}
        contentContainerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
          height: height,
        }}
        style={{
          backgroundColor: '#fff',
          borderColor: '#eee',
        }}>
        <View style={styles.top_container}>
          <View style={[styles.imageContainer, {backgroundColor: '#fff'}]}>
            <Image
              source={{
                uri: photo,
              }}
              style={styles.productImage}
            />
          </View>
          <View style={styles.text_container}>
            <Text style={styles.name}>{firstname}</Text>
            <Text style={[styles.name, {marginLeft: 5}]}>{lastname}</Text>
          </View>
        </View>
        <View style={styles.drawerItems}>
          {DrawerContent.map((item, index) => (
            <LinearGradient
              key={item.navigation}
              style={[styles.drawer]}
              start={{x: 0, y: 0}}
              end={{x: 1, y: 0}}
              colors={
                item.navigation === selectedRating
                  ? ['#fad6b8', '#fadfc2', '#fbe8d7', '#fcf2e8', '#fcf7f2']
                  : ['#fff', '#fff']
              }>
              <DrawerItem
                onPress={() => {
                  confirmHandler(item.navigation);
                  setTimeout(() => {
                    props.navigation.navigate(item.navigation);
                  }, 500);
                }}
                label={item.label}
                labelStyle={[
                  styles.label,
                  {
                    color:
                      item.navigation === selectedRating
                        ? '#e6934d'
                        : '#150a39',
                  },
                ]}
              />
            </LinearGradient>
          ))}
        </View>
        <TouchableOpacity
          onPress={() => {
            logOut();
          }}
          style={styles.logOut}>
          <Text style={styles.logTesxt}>Log Out</Text>
          <Icon name="logout" size={22} color="#e6934d" />
        </TouchableOpacity>
        <View style={{width: '60%'}}>
          <Text style={styles.versionText}>Version 12.23</Text>
        </View>
      </DrawerContentScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    overflow: 'hidden',
    justifyContent: 'center',
    minHeight: '100%',
  },
  drawer: {
    height: 50,
    width: width * 0.5,
    justifyContent: 'center',
    borderRadius: 10,
    marginVertical: 10,
    borderColor: '#fcf7f2',
    marginLeft: 50,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    borderRadius: 10,
    borderRadius: 30,
  },
  imageContainer: {
    borderWidth: 1,
    borderColor: '#aaa',
    borderStyle: 'dashed',
    borderRadius: 30,
    height: 100,
    width: 100,
  },
  label: {
    color: '#000',
    fontSize: 16,
    fontFamily: 'BalooPaaji2-Regular',
  },
  image: {
    width: null,
    height: null,
    flex: 1,
  },
  imgBox: {
    width: 20,
    height: 20,
    marginRight: '10%',
  },
  icons: {
    color: '#121b3c',
  },
  top_container: {
    width: width * 0.5,
    paddingLeft: 25,
  },
  image_container: {
    width: 75,
    height: 75,
    marginRight: 17,
    borderRadius: 100,
    overflow: 'hidden',
    borderColor: '#fff',
    elevation: 16,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  text_container: {
    paddingLeft: 10,
    flexDirection: 'row',
  },
  name: {
    fontSize: 15,
    color: '#000',
    fontFamily: 'BalooPaaji2-Regular',
    marginTop: 10,
  },
  ProfileText: {
    fontFamily: 'BalooPaaji2-Regular',
    color: '#7B7B7B',
    fontSize: 13,
  },
  drawerItems: {
    paddingBottom: 15,
    borderTopLeftRadius: 100,
    borderBottomRightRadius: 100,
    backgroundColor: '#fff',
    alignItems: 'center',
    marginVertical: 30,
  },
  version: {
    paddingBottom: 20,
  },
  versionText: {
    fontSize: 16,
    fontFamily: 'BalooPaaji2-Regular',
    color: '#aaa',
  },
  logTesxt: {
    fontSize: 15,
    fontFamily: 'BalooPaaji2-Regular',
    color: '#e6934d',
  },
  logOut: {
    width: '60%',
    flexDirection: 'row',
    justifyContent: 'space-around',
    borderRadius: 10,
    alignItems: 'center',
    paddingVertical: 3,
    marginBottom: 20,
    borderWidth: 0.5,
    elevation: 0.1,
    borderColor: '#e6934d',
    borderStyle: 'dashed',
  },
});
