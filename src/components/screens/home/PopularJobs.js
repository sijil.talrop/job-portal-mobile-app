import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  TouchableOpacity,
  ImageBackground,
  Image,
  StyleSheet,
  Text,
  Dimensions,
  FlatList,
  Pressable,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {CommonActions, useNavigation} from '@react-navigation/native';
import {RFPercentage, RFValue} from 'react-native-responsive-fontsize';
import axios from 'axios';
import * as base from '../../../../Settings';
import {InstagramLoader} from 'react-native-easy-content-loader';

const {width} = Dimensions.get('window');
const box_width = width * 0.8;
const box_height = box_width * 0.62;
const height = width * 0.6; //60%

export default function PopularJobs({refresh}) {
  const [selectedRating, setSelectedRating] = useState('AirBn');
  const [popular_job, setPopular_job] = useState([]);
  const [isLoading, setLoading] = useState(true);

  const navigation = useNavigation();

  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  let _get_categories = () => {
    let get_url = base.BASE_URL + 'jobs/jobs/?popular=true';
    axios
      .get(get_url)
      .then((response) => {
        let {StatusCode, data} = response.data;
        if (StatusCode == 6000) {
          setPopular_job(data.data);
          setSelectedRating(data.data[0].name);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        } else {
          setPopular_job([]);
          setTimeout(() => {
            setLoading(false);
          }, 500);
        }
      })
      .catch((error) => {
        console.warn(error);
      });
  };

  useEffect(() => {
    _get_categories();
  }, [refresh]);

  const onPressFunction = () => {
    flatlistRef.current.scrollToIndex({index: 0});
  };
  const flatlistRef = useRef();

  const renderItem = ({item, index}) => {
    return (
      <InstagramLoader
        key={item.pk}
        active
        tHeight="20%"
        tWidth="42%"
        sTWidth="42%"
        loading={isLoading}
        primaryColor="#d2d2d2"
        secondaryColor="#d2d2d2"
        imageStyles={{
          width: box_width + box_width * 0.07,
          height: box_height * 0.7,
        }}>
        <TouchableOpacity
          activeOpacity={0.9}
          onPress={() => {
            TouchableHandler(item.name);
            setTimeout(() => {
              navigation.navigate('CompanyJobSinglePage', {
                item: item,
              });
            }, 500);
          }}
          style={styles.mainContent}>
          <ImageBackground
            source={
              item.name === selectedRating
                ? require('../../../assets/images/contact-banner.png')
                : require('../../../assets/images/job-portal–2.jpg')
            }
            style={styles.bannerContainer}>
            <View style={styles.topContent}>
              <View style={styles.imgContainer}>
                <Image style={styles.image} source={{uri: item.logo}} />
              </View>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text
                  style={[
                    styles.rightContentText,
                    {color: item.name === selectedRating ? '#fff' : '#000'},
                  ]}>
                  ₹{item.salary_minimum}-{item.salary_maximum}/
                </Text>
                <Text
                  style={[
                    styles.cashdots,
                    {color: item.name === selectedRating ? '#fff' : '#000'},
                  ]}>
                  m
                </Text>
              </View>
            </View>
            <View>
              <Text
                style={[
                  styles.titleText,
                  {color: item.name === selectedRating ? '#fff' : '#000'},
                ]}>
                {item.business.length > 17
                  ? item.business.substring(0, 20) + '...'
                  : item.business}
              </Text>
              <Text
                style={[
                  styles.jobText,
                  {color: item.name === selectedRating ? '#fff' : '#000'},
                ]}>
                {item.name.length > 17
                  ? item.name.substring(0, 20) + '...'
                  : item.name}
              </Text>
              <View style={styles.locationBox}>
                <Icon
                  name={'google-maps'}
                  color={item.name === selectedRating ? '#fff' : '#90c43c'}
                  size={20}
                />
                <Text
                  style={[
                    styles.locationText,
                    {color: item.name === selectedRating ? '#fff' : '#000'},
                  ]}>
                  {item.location.length > 17
                    ? item.location.substring(0, 20) + '...'
                    : item.location}
                </Text>
              </View>
            </View>
            <View style={styles.rateContainer}>
              <Icon
                name={'bookmark'}
                color={item.kind == 'Featured' ? '#ef801f' : '#eee'}
                size={45}
              />
            </View>
          </ImageBackground>
        </TouchableOpacity>
      </InstagramLoader>
    );
  };

  return (
    <View style={{height: box_height + box_height * 0.42}}>
      <View style={styles.HeaderContainer}>
        <Text style={styles.header}>Popular Jobs</Text>
        <TouchableOpacity
          activeOpacity={0.8}
          onPress={() => {
            navigation.navigate('PopularJobTab', {total: popular_job.length});
          }}
          style={styles.buttonBox}>
          <Text style={styles.buttonText}>Show all</Text>
        </TouchableOpacity>
      </View>
      <View style={{alignItems: 'center'}}>
        <FlatList
          horizontal
          ref={flatlistRef}
          showsHorizontalScrollIndicator={false}
          data={popular_job}
          renderItem={renderItem}
          keyExtractor={(item) => item.pk}
        />
        <Pressable style={styles.button} onPress={onPressFunction}>
          <Icon name={'chevron-left'} color={'#000'} size={30} />
        </Pressable>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  HeaderContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    width: width,
  },
  buttonBox: {
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    width: width * 0.2,
    paddingVertical: 5,
  },
  buttonText: {
    fontSize: RFPercentage(1.6),
    fontFamily: 'BalooPaaji2-Regular',
    color: '#9390a7',
  },
  header: {
    fontSize: 19,
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: RFValue(9, width), // second argument is standardScreenHeight(optional),
  },
  scrollStyle: {
    flexDirection: 'row',
  },
  topContent: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  imgContainer: {
    width: box_width * 0.16,
    height: box_width * 0.16,
    borderRadius: 20,
    backgroundColor: '#fff',
    overflow: 'hidden',
    borderWidth: 0.5,
    borderColor: '#aaa',
  },
  image: {
    width: null,
    height: null,
    borderRadius: 20,
    overflow: 'hidden',
    flex: 1,
  },
  mainContent: {
    margin: 15,
    width: box_width - 40,
    height: box_height,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  bannerContainer: {
    borderRadius: 30,
    width: box_width - 40,
    height: box_height,
    padding: 20,
    overflow: 'hidden',
    borderWidth: 0.5,
    elevation: 0.08,
    borderColor: '#aaa',
    justifyContent: 'center',
  },
  titleText: {
    fontSize: RFPercentage(1.6),
    color: '#fff',
    marginTop: 10,
    fontFamily: 'BalooPaaji2-Regular',
    textTransform: 'capitalize',
  },
  jobText: {
    fontSize: RFPercentage(1.8),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  locationText: {
    fontSize: RFPercentage(1.4),
    fontFamily: 'BalooPaaji2-Regular',
    marginLeft: 10,
  },
  locationBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  rightContentText: {
    fontSize: RFPercentage(1.9),
    fontFamily: 'BalooPaaji2-SemiBold',
  },
  rateContainer: {
    position: 'absolute',
    right: 15,
    top: -10,
  },
  button: {
    position: 'absolute',
    width: 50,
    height: 50,
    borderRadius: 50 / 1,
    backgroundColor: '#eee',
    alignItems: 'center',
    justifyContent: 'center',
    right: 30,
    bottom: 30,
  },
  arrow: {
    fontSize: 20,
  },
});
