import React, {useState, useEffect, useRef} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  ImageBackground,
  Animated,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BasicHeading from '../header/BasicHeading';
import {CommonActions, useNavigation} from '@react-navigation/native';
import axios from 'axios';
import * as base from '../../../../Settings';
import AsyncStorage from '@react-native-async-storage/async-storage';
const {width, height} = Dimensions.get('window');
const Reward_Box_Height = width * 0.31;
const card_width = width * 0.6;
const box_width = width - 40;

export default function FilterListPage({route}) {
  const navigation = useNavigation();
  const [selectedRating, setSelectedRating] = useState('Facebook');

  const TouchableHandler = (value) => {
    setSelectedRating(value);
  };

  useEffect(() => {
    fadeIn();
  }, []);

  const fadeAnim = useRef(new Animated.Value(0)).current;
  const fadeIn = () => {
    Animated.timing(fadeAnim, {
      toValue: 1,
      duration: 5000,
      useNativeDriver: true,
    }).start();
  };
  //   {
  //     subcategory_list &&
  //       subcategory_list.length > 0 &&
  //       subcategory_list.map((item, index) => (
  //         <Picker.Item label={item.label} key={index} value={item.value} />
  //       ));
  //   }

  const Needs = () => {
    return (
      route.params.popular_job &&
      route.params.popular_job.length > 0 &&
      route.params.popular_job.map((item, index) => (
        <TouchableOpacity
          key={item.pk}
          onPress={() => {
            TouchableHandler(item.pk);
            navigation.navigate('CompanyJobSinglePage', {
              item: item,
            });
          }}
          activeOpacity={0.9}
          style={[
            styles.mainContainer,
            {
              backgroundColor: item.pk == selectedRating ? '#2261a6' : '#fff',
            },
          ]}>
          <View style={styles.left}>
            <View
              style={[
                styles.lefContent,
                {
                  backgroundColor:
                    item.pk == selectedRating ? '#fff' : '#e2e2e2',
                },
              ]}>
              <View style={styles.imageContainer}>
                <Image source={{uri: item.logo}} style={styles.productImage} />
              </View>
            </View>
          </View>
          <View style={styles.contentContairener}>
            <Text
              style={[
                styles.introName,
                {color: item.pk == selectedRating ? '#fff' : '#adabbb'},
              ]}>
              {item.business.length > 17
                ? item.business.substring(0, 20) + '...'
                : item.business}
            </Text>
            <Text
              style={[
                styles.name,
                {color: item.pk == selectedRating ? '#fff' : '#130937'},
              ]}>
              {item.name}
            </Text>
            <View style={styles.Location}>
              <Text
                style={[
                  styles.amount,
                  {
                    color: item.pk == selectedRating ? '#fff' : '#130937',
                  },
                ]}>
                ₹{item.salary_minimum}-{item.salary_maximum}/m
              </Text>
              <View style={styles.locationCharge}>
                <Icon name={'google-maps'} color={'#4eeb12'} size={20} />
                <Text
                  style={[
                    styles.place,
                    {
                      color: item.pk == selectedRating ? '#fff' : '#130937',
                    },
                  ]}>
                  {item.location.length > 17
                    ? item.location.substring(0, 20) + '...'
                    : item.location}
                </Text>
              </View>
            </View>
          </View>
          <View style={styles.right}>
            <View style={styles.rateContainer}>
              <Icon
                name={'bookmark'}
                color={item.kind == 'Featured' ? '#EF803A' : '#eee'}
                size={45}
              />
            </View>

            <View style={styles.delivery}>
              <Text
                style={[
                  styles.bottomButtonText,
                  {
                    color: item.pk == selectedRating ? '#fff' : '#aaa',
                  },
                ]}>
                {item.date_added}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      ))
    );
  };

  return (
    <View style={{minHeight: '100%', backgroundColor: '#fff'}}>
      <ImageBackground
        style={styles.background}
        source={require('../../../assets/vector-images/Group20260.png')}
      />
      <View
        style={{
          paddingHorizontal: 15,
          paddingVertical: 30,
        }}>
        <BasicHeading title="filter result" />
      </View>
      <ScrollView
        contentContainerStyle={{
          alignItems: 'center',
          // paddingVertical: 20,
          minHeight: '100%',
        }}>
        {Needs()}
        {!isNaN(route.params.popular_job) != '' && (
          <Animated.View
            style={[
              {
                opacity: fadeAnim,
              },
            ]}>
            <Image
              style={styles.foundImage}
              source={require('../../../assets/vector-images/noresultfound.png')}
            />
            <Text style={styles.messageText}>Nothing Found</Text>
          </Animated.View>
        )}
        <View style={{height: height * 0.4}}></View>
      </ScrollView>
    </View>
  );
}

const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    flexDirection: 'row',
    width: box_width,
    height: Reward_Box_Height,
    marginBottom: 15,
    backgroundColor: '#fff',
    padding: 12,
    borderRadius: 15,
    borderWidth: 0.5,
    borderColor: '#aaa',
    elevation: 0.08,
    justifyContent: 'space-around',
  },
  left: {
    alignItems: 'center',
    width: box_width * 0.17,
  },
  imageContainer: {
    borderWidth: 0.5,
    borderColor: '#aaa',
    borderRadius: 30,
    height: Reward_Box_Height * 0.3,
    width: Reward_Box_Height * 0.3,
  },
  contentContairener: {
    width: box_width * 0.53,
  },
  lefContent: {
    height: Reward_Box_Height * 0.4,
    width: Reward_Box_Height * 0.4,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 15,
  },
  productImage: {
    height: null,
    width: null,
    flex: 1,
    overflow: 'hidden',
    borderRadius: 10,
    borderRadius: 30,
  },
  Location: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  locationCharge: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  contentContainer: {
    justifyContent: 'center',
  },
  introName: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    textTransform: 'capitalize',
  },
  name: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 17,
    textTransform: 'capitalize',
    color: '#000',
  },
  amount: {
    fontFamily: 'BalooPaaji2-SemiBold',
    fontSize: 15,
    textTransform: 'capitalize',
    color: '#000',
  },
  place: {
    fontFamily: 'BalooPaaji2-Regular',
    fontSize: 14,
    textTransform: 'capitalize',
    color: '#393057',
  },
  right: {
    flexDirection: 'column',
    alignItems: 'flex-end',
    justifyContent: 'center',
    width: box_width * 0.22,
    height: Reward_Box_Height,
  },
  delivery: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    height: Reward_Box_Height * 0.7,
  },
  bottomButtonText: {
    color: '#aaa',
    fontSize: 14,
    fontFamily: 'BalooPaaji2-Regular',
  },
  rateContainer: {
    position: 'absolute',
    top: -7,
  },
  background: {
    resizeMode: 'contain',
    width: width * 0.4,
    height: width * 0.3,
    position: 'absolute',
    top: 0,
    right: 0,
  },
});
