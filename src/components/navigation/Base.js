import 'react-native-gesture-handler';
import React, {useState, useEffect, useRef, useContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';

import {StyleSheet, View, Dimensions, ImageBackground} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {DrawerContent} from './DrawerContent';
import Animated from 'react-native-reanimated';
import Home from '../screens/home/Home';
import FilterPage from '../screens/home/FilterPage';
import PopularJobTab from '../screens/popular-jobs-tab/PopularJobTab';
import CompanyTab from '../screens/company-tab/CompanyTab';
import CompanySinglePage from '../screens/company-single-page/CompanySinglePage';
import CompanyJobSinglePage from '../screens/company-single-page/CompanyJobSinglePage';
import ApplyNowModal from '../screens/company-single-page/ApplyNowModal';
import SuccessPopup from '../screens/popup-pages/SuccessPopup';
import ProfilePage from '../screens/Profile/ProfilePage';
import AppliedJobs from '../screens/list-pages/AppliedJobs';
import MatchedJobs from '../screens/list-pages/MatchedJobs';
import SavedJobs from '../screens/list-pages/SavedJobs';
import SignInPage from '../screens/verification/SignInPage';
import Otp from '../screens/verification/Otp';
import IntroPage from '../screens/verification/IntroPage';
import AccountTab from '../screens/Profile/AccountTab';
import SearchPage from '../screens/search/SearchPage';
import FilterListPage from '../screens/list-pages/FilterListPage';
import PakagesListPage from '../screens/list-pages/PakagesListPage';
import CreateProfile from '../screens/Profile/CreateProfile';
import HomeHeader from '../screens/header/HomeHeader';
import Note from '../screens/notifications/Note';
import Cv from '../screens/generate-cv/Cv';
import {Context} from '../contexts/Store';

const {height, width} = Dimensions.get('window');
const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const AppDrawerNavigator = () => {
  const [profile, setProfile] = useState([]);

  const [progress, setProgress] = React.useState(new Animated.Value(0));
  const scale = Animated.interpolate(progress, {
    inputRange: [0, 0.9],
    outputRange: [1, 0.8],
  });
  const borderRadius = Animated.interpolate(progress, {
    inputRange: [0, 0],
    outputRange: [0, 50],
  });
  const borderWidth = Animated.interpolate(progress, {
    inputRange: [0, 0],
    outputRange: [0, 1],
  });
  const elevation = Animated.interpolate(progress, {
    inputRange: [0, 0],
    outputRange: [0, 5],
  });
  const screenStyle = {
    borderRadius,
    elevation,
    borderWidth,
    transform: [{scale}],
  };

  return (
    <View
      style={{
        backgroundColor: '#fff',
        width: '100%',
        height: '100%',
      }}>
      <ImageBackground
        source={require('../../assets/vector-images/Group20260.png')}
        style={styles.backgroundImage}
      />
      <Drawer.Navigator
        drawerContent={(props) => {
          setProgress(props.progress);
          return <DrawerContent {...props} />;
        }}
        hideStatusBar={false}
        drawerType="slide"
        overlayColor="transparent"
        drawerStyle={styles.drawerStyles}
        contentContainerStyle={{flex: 1}}
        drawerContentOptions={{
          activeTintColor: 'green',
          inactiveTintColor: 'green',
          activeBackgroundColor: 'transparent',
        }}
        sceneContainerStyle={{backgroundColor: 'transparent'}}
        initialRouteName="HomeStackNavigator">
        <Drawer.Screen name="HomeStackNavigator">
          {(props) => <HomeStackNavigator {...props} style={screenStyle} />}
        </Drawer.Screen>
        <Drawer.Screen name="ProfileStackNavigator">
          {(props) => <ProfileStackNavigator {...props} style={screenStyle} />}
        </Drawer.Screen>
        <Drawer.Screen name="BaseStackNavigator">
          {(props) => <BaseStackNavigator {...props} style={screenStyle} />}
        </Drawer.Screen>
        <Drawer.Screen name="NotificationStackNavigator">
          {(props) => (
            <NotificationStackNavigator {...props} style={screenStyle} />
          )}
        </Drawer.Screen>
        <Drawer.Screen name="CvStackNavigator">
          {(props) => <CvStackNavigator {...props} style={screenStyle} />}
        </Drawer.Screen>
      </Drawer.Navigator>
    </View>
  );
};

const BaseStackNavigator = () => {
  return (
    <>
      <Stack.Navigator
        initialRouteName="AppliedJobs"
        options={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="AppliedJobs"
          component={AppliedJobs}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="MatchedJobs"
          component={MatchedJobs}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="SavedJobs"
          component={SavedJobs}
          options={{
            headerShown: false,
          }}
        />

        <Stack.Screen
          name="Home"
          component={Home}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </>
  );
};

const NotificationStackNavigator = ({navigation, style}) => {
  return (
    <Animated.View
      style={[
        styles.stack,
        {
          flex: 1,
          overflow: 'hidden',
          borderWidth: 1,
          borderColor: '#aaa',
        },
        style,
      ]}>
      <Stack.Navigator
        screenOptions={{headerTransparent: true, headerTitle: null}}
        initialRouteName="Note"
        options={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="Note"
          component={Note}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </Animated.View>
  );
};

const CvStackNavigator = ({navigation, style}) => {
  return (
    <Animated.View
      style={[
        styles.stack,
        {
          flex: 1,
          overflow: 'hidden',
          borderWidth: 1,
          borderColor: '#aaa',
        },
        style,
      ]}>
      <Stack.Navigator
        screenOptions={{headerTransparent: true, headerTitle: null}}
        initialRouteName="Cv"
        options={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="Cv"
          component={Cv}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </Animated.View>
  );
};

const HomeStackNavigator = ({navigation, style}) => {
  return (
    <>
      <Animated.View
        style={[
          styles.stack,
          {
            overflow: 'hidden',
            borderWidth: 1,
            borderColor: '#aaa',
          },
          style,
        ]}>
        <Stack.Navigator
          screenOptions={{headerTransparent: true, headerTitle: null}}
          initialRouteName="Home"
          options={{
            headerShown: false,
          }}>
          <Stack.Screen
            name="Home"
            component={Home}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="PopularJobTab"
            component={PopularJobTab}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="CompanyTab"
            component={CompanyTab}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="CompanySinglePage"
            component={CompanySinglePage}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="CompanyJobSinglePage"
            component={CompanyJobSinglePage}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="ApplyNowModal"
            component={ApplyNowModal}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="SuccessPopup"
            component={SuccessPopup}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="FilterPage"
            component={FilterPage}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="SearchPage"
            component={SearchPage}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="FilterListPage"
            component={FilterListPage}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="AppliedJobs"
            component={AppliedJobs}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="MatchedJobs"
            component={MatchedJobs}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="SavedJobs"
            component={SavedJobs}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="PakagesListPage"
            component={PakagesListPage}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="HomeHeader"
            component={HomeHeader}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="DrawerContent"
            component={DrawerContent}
            options={{
              headerShown: false,
            }}
          />
          <Stack.Screen
            name="Cv"
            component={Cv}
            options={{
              headerShown: false,
            }}
          />
        </Stack.Navigator>
      </Animated.View>
    </>
  );
};

const ProfileStackNavigator = ({navigation, style}) => {
  return (
    <Animated.View
      style={[
        styles.stack,
        {
          flex: 1,
          overflow: 'hidden',
          borderWidth: 1,
          borderColor: '#aaa',
        },
        style,
      ]}>
      <Stack.Navigator
        initialRouteName="ProfilePage"
        options={{
          headerShown: false,
        }}>
        <Stack.Screen
          name="ProfilePage"
          component={ProfilePage}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="AppliedJobs"
          component={AppliedJobs}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="MatchedJobs"
          component={MatchedJobs}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="SavedJobs"
          component={SavedJobs}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="CompanyJobSinglePage"
          component={CompanyJobSinglePage}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="CompanySinglePage"
          component={CompanySinglePage}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="ApplyNowModal"
          component={ApplyNowModal}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="SuccessPopup"
          component={SuccessPopup}
          options={{
            headerShown: false,
          }}
        />
        {/* <Stack.Screen
          name="Note"
          component={Note}
          options={{
            headerShown: false,
          }}
        /> */}
        <Stack.Screen
          name="CreateProfile"
          component={CreateProfile}
          options={{
            headerShown: false,
          }}
        />
        <Stack.Screen
          name="DrawerContent"
          component={DrawerContent}
          options={{
            headerShown: false,
          }}
        />
      </Stack.Navigator>
    </Animated.View>
  );
};
const AuthStackNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="IntroPage"
      options={{
        headerShown: false,
      }}>
      <Stack.Screen
        name="IntroPage"
        component={IntroPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SignInPage"
        component={SignInPage}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="Otp"
        component={Otp}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="AccountTab"
        component={AccountTab}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="SuccessPopup"
        component={SuccessPopup}
        options={{
          headerShown: false,
        }}
      />
      <Stack.Screen
        name="HomeStackNavigator"
        component={HomeStackNavigator}
        options={{
          headerShown: false,
        }}
      />
      {/* <Stack.Screen
        name="Note"
        component={Note}
        options={{
          headerShown: false,
        }}
      /> */}
      <Stack.Screen
        name="HomeHeader"
        component={HomeHeader}
        options={{
          headerShown: false,
        }}
      />
    </Stack.Navigator>
  );
};

export default function Base() {
  const {state, dispatch} = useContext(Context);

  const fetchUserData = async () => {
    let user_data_stored = await AsyncStorage.getItem('user_data');
    let user_data_value = JSON.parse(user_data_stored);
    dispatch({
      type: 'UPDATE_USER_DATA',
      user_data: user_data_value,
    });
  };
  useEffect(() => {
    fetchUserData();
  }, []);

  return (
    <NavigationContainer>
      {state.user_data.is_verified ? (
        <AppDrawerNavigator />
      ) : (
        <AuthStackNavigator />
      )}
    </NavigationContainer>

    // <NavigationContainer>
    //   {state.user_data.is_verified ? (
    //     <AppDrawerNavigator />
    //   ) : (
    //     <AuthStackNavigator />
    //   )}
    // </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  stack: {
    flex: 1,
    shadowColor: '#FFF',
    shadowOffset: {
      width: 0,
      height: 8,
    },
    shadowOpacity: 0.44,
    shadowRadius: 10.32,
    elevation: 2,
  },
  drawerStyles: {
    flex: 1,
    width: '60%',
    borderColor: '#000',
    backgroundColor: 'transparent',
  },
  // drawerItem: {alignItems: 'flex-start', marginVertical: 0},
  // drawerLabel: {color: 'white', marginLeft: -16},
  avatar: {
    borderRadius: 60,
    marginBottom: 16,
    borderColor: 'white',
    borderWidth: StyleSheet.hairlineWidth,
  },
  backgroundImage: {
    resizeMode: 'contain',
    height: width * 0.3,
    position: 'absolute',
    width: width * 0.3,
    top: 0,
    right: 0,
  },
});
